#!/usr/bin/env python3
# Copyright 2016 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Runs gn format on all of Cobalt's gn files."""

import os
import sys
# Ensure that this file can import from cobalt root even if run as a script.
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import subprocess

from tools import get_files

THIS_DIR = os.path.dirname(__file__)
SRC_ROOT_DIR = os.path.abspath(os.path.join(THIS_DIR, os.pardir))


def main(only_directories=[], all_files=False):
  status = 0
  files_to_lint = get_files.files_to_lint(
      ('.gn', '.gni'), only_directories=only_directories, all_files=all_files
  )

  for f in files_to_lint:
    try:
      subprocess.check_call(['gn', 'format', '--dry-run', f])
    except:
      print(
          '%(file)s: Does not match expected format. '
          'Please run `sysroot/bin/gn format %(file)s`'
          % {'file': os.path.relpath(f, SRC_ROOT_DIR)}
      )
      status += 1

  return status


if __name__ == '__main__':
  exit(main())
