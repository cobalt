#!/usr/bin/env python3
# Copyright 2016 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Runs gofmt on all of Cobalt's go files."""

import os
import sys

# Ensure that this file can import from cobalt root even if run as a script.
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import subprocess

from tools import get_files


def main(only_directories=[], all_files=False):
  status = 0
  files_to_lint = get_files.files_to_lint(
      ('.go',), only_directories=only_directories, all_files=all_files
  )

  print('Linting %d go files' % len(files_to_lint))
  if files_to_lint:
    p = subprocess.Popen(
        ['gofmt', '-d'] + files_to_lint, text=True, stdout=subprocess.PIPE
    )
    out, err = p.communicate()

    if p.returncode != 0:
      print('Received non-zero return code (%s)' % p.returncode)
      status += 1

    if len(out) > 0:
      print('The following differences were found with gofmt:\n%s' % out)
      status += 1

  return status


if __name__ == '__main__':
  exit(main())
