#!/usr/bin/env python3
# Copyright 2016 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Runs all executables (tests) in a directory"""

import logging
import os
import subprocess

THIS_DIR = os.path.dirname(__file__)
SRC_ROOT_DIR = os.path.abspath(os.path.join(THIS_DIR, os.pardir))
SYS_ROOT_DIR = os.path.join(SRC_ROOT_DIR, "sysroot")

_logger = logging.getLogger()


def run_all_tests(test_dir, verbose_count=0, test_args=None):
  """Runs the tests in the given directory.

  Optionally also starts various processes that may be needed by the tests.

  Args:
    test_dir: String. Name of the directory under the "out" directory containing
      test executables to be run.
    test_args: List of strings. These will be passed to each test executable.

  Returns:
    A list of strings indicating which tests failed. Returns None or to
    indicate success.
  """
  tdir = os.path.abspath(os.path.join(SRC_ROOT_DIR, "out", test_dir))

  if not os.path.exists(tdir):
    print("\n*************** ERROR ****************")
    print("Directory %s does not exist." % tdir)
    print("Run 'cobaltb.py build' first.")
    return ["Directory %s does not exist." % tdir]

  if test_args is None:
    test_args = []
  if "rust" not in tdir:
    test_args.append("-logtostderr")
  if verbose_count > 0:
    test_args.append("-v=%d" % verbose_count)

  print("Running all tests in %s " % tdir)
  print("Test arguments: '%s'" % test_args)
  failure_list = []
  for test_executable in os.listdir(tdir):
    path = os.path.abspath(os.path.join(tdir, test_executable))
    if os.path.isdir(path):
      continue
    if not os.access(path, os.X_OK):
      continue
    print("Running %s..." % test_executable)
    command = [path] + test_args
    return_code = subprocess.call(command)
    if return_code != 0:
      failure_list.append(test_executable)
    if return_code < 0:
      print("")
      print(
          "****** WARNING Process [%s] terminated by signal %d"
          % (command[0], -return_code)
      )
  if failure_list:
    return failure_list
  else:
    return None
