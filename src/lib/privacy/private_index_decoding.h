// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LIB_PRIVACY_PRIVATE_INDEX_DECODING_H_
#define COBALT_SRC_LIB_PRIVACY_PRIVATE_INDEX_DECODING_H_

#include <google/protobuf/repeated_field.h>

#include "src/public/lib/status.h"
#include "src/registry/metric_definition.pb.h"

namespace cobalt {

// A value which represents either an approximate sum or an approximate count, as a double. Used
// when decoding observations for FleetwideMeans reports.
struct SumOrCount {
  enum SumOrCountType : uint8_t { SUM, COUNT };
  SumOrCountType type;

  double sum = 0;
  double count = 0;
};

// Populates |event_vector| with the event vector which corresponds to |index| according to the
// contents of |metric_dimensions| and returns an OK status, or returns an error status if |index|
// does not represent a valid event vector according to |metric_dimensions|.
Status DecodePrivateIndexAsEventVector(
    uint64_t index,
    const google::protobuf::RepeatedPtrField<MetricDefinition::MetricDimension>& metric_dimensions,
    std::vector<uint32_t>* event_vector);

// Populates |event_vector| and |double_value| with the event vector and numeric value which
// correspond to |index| according to |metric_dimensions|, |min_value|, |max_value|, and
// |num_index_points|, or returns an error status if |index| does not represent a valid
// (event_vector, double_value) pair.
Status DecodePrivateIndexAsDouble(
    uint64_t index,
    const google::protobuf::RepeatedPtrField<MetricDefinition::MetricDimension>& metric_dimensions,
    int64_t min_value, int64_t max_value, uint64_t num_index_points,
    std::vector<uint32_t>* event_vector, double* double_value);

// Populates |event_vector| and |sum_or_count| with the event vector and SumAndCount which
// correspond to |index| according to |metric_dimensions|, |min_value|, |max_value|, |max_count| and
// |num_index_points|, or returns an error status if |index| does not represent a valid
// (event_vector, sum)  or (event_vector, count) pair.
//
// After a successful call, |sum_or_count| will be a SumOrCount struct whose |type| field is set to
// either SUM or COUNT. If the |type| is SUM, then the |sum| field is populated with a double
// representing a sum; if the |type| is COUNT, then the |count| field is populated with an double
// representing a count.
Status DecodePrivateIndexAsSumOrCount(
    uint64_t index,
    const google::protobuf::RepeatedPtrField<MetricDefinition::MetricDimension>& metric_dimensions,
    int64_t min_value, int64_t max_value, uint64_t max_count, uint64_t num_index_points,
    std::vector<uint32_t>* event_vector, SumOrCount* sum_or_count);

// Populates |event_vector| and |bucket_index| with the event vector and histogram bucket index
// which correspond to |index| according to |metric_dimensions| and |max_bucket_index|, or returns
// an error status if |index| does not represent a valid (event_vector, bucket_index) pair.
Status DecodePrivateIndexAsHistogramBucketIndex(
    uint64_t index,
    const google::protobuf::RepeatedPtrField<MetricDefinition::MetricDimension>& metric_dimensions,
    uint32_t max_bucket_index, std::vector<uint32_t>* event_vector, uint32_t* bucket_index);

// Populates |event_vector|, |bucket_index|, and |bucket_count| with the event vector, histogram
// bucket index, and bucket count which correspond to |index| according to |metric_dimensions|,
// |max_bucket_index|, |max_count|, and |num_index_points|, or else returns an error status if
// |index| does not represent a valid (event_vector, bucket_index, bucket_count) tuple.
// The bucket count is decoded as a double, without rounding.
Status DecodePrivateIndexAsHistogramBucketIndexAndCount(
    uint64_t index,
    const google::protobuf::RepeatedPtrField<MetricDefinition::MetricDimension>& metric_dimensions,
    uint32_t max_bucket_index, uint64_t max_count, uint64_t num_index_points,
    std::vector<uint32_t>* event_vector, uint32_t* bucket_index, double* bucket_count);

}  // namespace cobalt

#endif  // COBALT_SRC_LIB_PRIVACY_PRIVATE_INDEX_DECODING_H_
