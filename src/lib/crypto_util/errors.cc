// Copyright 2016 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/lib/crypto_util/errors.h"

#include <string>

#include <openssl/err.h>

namespace cobalt::crypto {

namespace {
constexpr size_t kOpenSSLErrorLen = 256;
}

std::string GetLastErrorMessage() {
  char buf[kOpenSSLErrorLen];
  ERR_error_string_n(ERR_peek_last_error(), &buf[0], kOpenSSLErrorLen);
  return std::string(buf);
}

}  // namespace cobalt::crypto
