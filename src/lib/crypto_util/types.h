// Copyright 2016 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LIB_CRYPTO_UTIL_TYPES_H_
#define COBALT_SRC_LIB_CRYPTO_UTIL_TYPES_H_

namespace cobalt::crypto {

using byte = unsigned char;

}  // namespace cobalt::crypto

#endif  // COBALT_SRC_LIB_CRYPTO_UTIL_TYPES_H_
