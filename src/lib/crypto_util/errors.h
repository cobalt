// Copyright 2016 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LIB_CRYPTO_UTIL_ERRORS_H_
#define COBALT_SRC_LIB_CRYPTO_UTIL_ERRORS_H_

#include <string>

namespace cobalt::crypto {

// Returns the error message corresponding to the last error from the
// this crypto library that occurred on this thread. After
// each call to a method from this library the result code should be
// checked. A result of one indicates success and zero indicates failure.
// When a zero is returned this function may be invoked to obtain a
// human-readable description of the error.
std::string GetLastErrorMessage();

}  // namespace cobalt::crypto

#endif  // COBALT_SRC_LIB_CRYPTO_UTIL_ERRORS_H_
