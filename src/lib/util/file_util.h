// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
#ifndef COBALT_SRC_LIB_UTIL_FILE_UTIL_H_
#define COBALT_SRC_LIB_UTIL_FILE_UTIL_H_

#include <string>

#include "src/lib/util/named_type.h"
#include "src/lib/util/status_builder.h"
#include "src/public/lib/statusor/statusor.h"

namespace cobalt::util {

using Path = util::NamedType<std::string, struct PathTag>;

lib::statusor::StatusOr<std::string> ReadTextFile(const Path& file_path);

lib::statusor::StatusOr<std::string> ReadNonEmptyTextFile(const Path& file_path);

lib::statusor::StatusOr<std::string> ReadHexFile(const Path& file_path);

std::string ReadHexFileOrDefault(const Path& file_path, const std::string& default_string);
// DEPRECATED: TODO(https://fxbug.dev/278930401): Remove once not used in Fuchsia
std::string ReadHexFileOrDefault(const std::string& file_path, const std::string& default_string);

template <>
class ContextFormatter<Path> {
 public:
  explicit ContextFormatter(const Path& v) : v_(v) {}
  static std::string default_key() { return "Path"; }
  void write_value(std::ostream& stream) { stream << v_; }

 private:
  const Path& v_;
};

}  // namespace cobalt::util
#endif  // COBALT_SRC_LIB_UTIL_FILE_UTIL_H_
