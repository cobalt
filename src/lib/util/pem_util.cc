// Copyright 2017 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/lib/util/pem_util.h"

#include "src/lib/util/file_util.h"
#include "src/logging.h"

namespace cobalt::util {

bool PemUtil::ReadTextFile(const std::string& file_path, std::string* file_contents) {
  if (!file_contents) {
    return false;
  }

  auto result = ::cobalt::util::ReadNonEmptyTextFile(Path(file_path));
  if (!result.ok()) {
    // NOTE(rudominer) We use VLOG instead of LOG(ERROR) for any error messages
    // in Cobalt that may occur on the client side.
    VLOG(1) << result.status().error_message();
    return false;
  }

  *file_contents = result.value();
  return true;
}

}  // namespace cobalt::util
