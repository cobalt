// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/lib/util/file_system.h"

#include "absl/strings/str_cat.h"

namespace cobalt::util {

// NOLINTNEXTLINE(misc-no-recursion)
bool FileSystem::DeleteRecursive(const std::string &directory) {
  auto children = ListFiles(directory).ConsumeValueOr({});
  for (const auto &child : children) {
    if (!DeleteRecursive(absl::StrCat(directory, "/", child))) {
      return false;
    }
  }
  return Delete(directory);
}

}  // namespace cobalt::util
