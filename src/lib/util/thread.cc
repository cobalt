// Copyright 2023 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/lib/util/thread.h"

#include <string>
#include <thread>

#ifdef __Fuchsia__
#include <zircon/syscalls.h>
#include <zircon/syscalls/object.h>
#include <zircon/threads.h>

namespace cobalt::util {

void NameThread(const std::string& name, std::thread& thread) {
  zx_handle_t thread_handle = native_thread_get_zx_handle(thread.native_handle());
  zx_object_set_property(thread_handle, ZX_PROP_NAME, name.c_str(), name.size());
}

}  // namespace cobalt::util

#else

namespace cobalt::util {

void NameThread(const std::string& name, std::thread& thread) {}

}  // namespace cobalt::util

#endif  // __Fuchsia__
