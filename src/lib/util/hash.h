// Copyright 2017 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LIB_UTIL_HASH_H_
#define COBALT_SRC_LIB_UTIL_HASH_H_

#include <cstdint>
#include <string>

namespace cobalt::util {

// Computes the farmhash::Fingerprint128 of the input data.
std::string FarmhashFingerprint(const std::string& data);

// Computes the farmhash::Fingerprint64 of the input data.
std::string FarmhashFingerprint64(const std::string& data);

uint64_t Farmhash64(const std::string& data);

}  // namespace cobalt::util

#endif  // COBALT_SRC_LIB_UTIL_HASH_H_
