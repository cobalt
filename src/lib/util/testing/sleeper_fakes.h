// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LIB_UTIL_TESTING_SLEEPER_FAKES_H_
#define COBALT_SRC_LIB_UTIL_TESTING_SLEEPER_FAKES_H_

#include <chrono>
#include <functional>

#include "src/lib/util/sleeper.h"
#include "src/lib/util/testing/clock_fakes.h"

namespace cobalt::util::testing {

// A fake implementation of SleeperInterface that does not actually sleep. It
// records the length of the most recent invocation of sleep_for. Optionally
// it can invoke a user-supplied callback and it can update a fake clock.
class FakeSleeper : public SleeperInterface {
 public:
  FakeSleeper() = default;

  // If a callback is set, it will be invoked each time sleep_for is invoked.
  void set_callback(std::function<void(const std::chrono::milliseconds&)> c) {
    callback_ = std::move(c);
  }

  // If a clock is set, its increment_by() method will be invoked each time
  // sleep_for() is invoked. In this way, in a test where both sleeping and the
  // clock are fakes, the fake clock will be advanced by the fake sleep time.
  void set_incrementing_steady_clock(testing::IncrementingSteadyClock* clock) {
    incrementing_steady_clock_ = clock;
  }

  void sleep_for(const std::chrono::milliseconds& sleep_duration) override {
    last_sleep_duration_ = sleep_duration;
    if (callback_) {
      callback_(sleep_duration);
    }
    if (incrementing_steady_clock_) {
      incrementing_steady_clock_->increment_by(sleep_duration);
    }
  }

  std::chrono::milliseconds last_sleep_duration() { return last_sleep_duration_; }

  void Reset() { last_sleep_duration_ = std::chrono::milliseconds(0); }

 private:
  std::chrono::milliseconds last_sleep_duration_;
  std::function<void(const std::chrono::milliseconds&)> callback_ = nullptr;
  testing::IncrementingSteadyClock* incrementing_steady_clock_ = nullptr;
};

}  // namespace cobalt::util::testing

#endif  // COBALT_SRC_LIB_UTIL_TESTING_SLEEPER_FAKES_H_
