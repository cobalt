
// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file

#include "src/lib/util/status_builder.h"

#include "src/logging.h"
#include "src/public/lib/status.h"
#include "src/public/lib/status_codes.h"

namespace cobalt::util {

const size_t kMaxErrorMessage = 256;

StatusBuilder::StatusBuilder(const Status& original_status, util::SourceLocation location)
    : location_(location) {
  SetCode(original_status.error_code());
  error_message_ << original_status.error_message();
  error_details_ << original_status.error_details();
}

StatusBuilder::StatusBuilder(StatusCode status_code, util::SourceLocation location)
    : location_(location) {
  SetCode(status_code);
}

StatusBuilder::StatusBuilder(StatusCode status_code, const std::string& initial_message,
                             util::SourceLocation location)
    : location_(location) {
  SetCode(status_code);
  error_message_ << initial_message;
}

#ifdef HAVE_GLOG

#define LOG_AT_LOC(location, severity) \
  ::google::LogMessage((location).file_name(), (location).line(), google::GLOG_##severity).stream()

#elif defined(__Fuchsia__)

#define LOG_AT_LOC(location, severity)                                                         \
  FX_LAZY_STREAM(                                                                              \
      ::fuchsia_logging::LogMessage(__FX_LOG_SEVERITY_##severity, (location).file_name(), \
                                    (location).line(), nullptr, "core")                        \
          .stream(),                                                                           \
      FX_LOG_IS_ON(severity))

#else

#define LOG_AT_LOC(location, severity) LOG(severity).AtLocation(location)

#endif
Status StatusBuilder::Build() {
  if (code_ == StatusCode::OK) {
    return Status::OkStatus();
  }

  std::string message = error_message_.str();
  if (message.size() > kMaxErrorMessage) {
    message.resize(kMaxErrorMessage);
  }

  Status result = Status(code_, message, error_details_.str());

  switch (log_level_) {
    case LogLevel::ERROR:
      LOG_AT_LOC(location_, ERROR) << result;
      break;
    case LogLevel::WARNING:
      LOG_AT_LOC(location_, WARNING) << result;
      break;
    case LogLevel::INFO:
      LOG_AT_LOC(location_, INFO) << result;
      break;
    default:
      // No logging
      break;
  }

  return result;
}
#undef LOG_AT_LOC

}  // namespace cobalt::util
