// Copyright 2017 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LIB_UTIL_PEM_UTIL_H_
#define COBALT_SRC_LIB_UTIL_PEM_UTIL_H_

#include <string>

namespace cobalt::util {

// PemUtil provides utilities for reading and writing PEM files.
class PemUtil {
 public:
  static const int kMaxFileSize = 100000;

  // Reads the text file at the specified path and writes the contents into
  // |*file_contents|. The file must contain at most |kMaxFileSize| bytes.
  // Returns true for success or false for failure.
  static bool ReadTextFile(const std::string& file_path, std::string* file_contents);
};

}  // namespace cobalt::util

#endif  // COBALT_SRC_LIB_UTIL_PEM_UTIL_H_
