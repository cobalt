// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LIB_UTIL_HYBRID_TINK_ENCRYPTED_MESSAGE_MAKER_H_
#define COBALT_SRC_LIB_UTIL_HYBRID_TINK_ENCRYPTED_MESSAGE_MAKER_H_

#include "google/protobuf/message_lite.h"
#include "src/lib/util/encrypted_message_util.h"
#include "third_party/tink/cc/hybrid_encrypt.h"

namespace cobalt::util {

lib::statusor::StatusOr<util::NotNullUniquePtr<EncryptedMessageMaker>>
MakeHybridTinkEncryptedMessageMaker(const std::string& public_keyset_bytes,
                                    const std::string& context_info, uint32_t key_index);

// HybridTinkEncryptedMessageMaker is an implementation of
// EncryptedMessageMaker. See EncryptedMessage::HYBRID_TINK for details.
class HybridTinkEncryptedMessageMaker : public EncryptedMessageMaker {
 public:
  HybridTinkEncryptedMessageMaker(std::unique_ptr<::crypto::tink::HybridEncrypt> encrypter,
                                  std::string context_info, uint32_t key_index);

  bool Encrypt(const google::protobuf::MessageLite& message,
               EncryptedMessage* encrypted_message) const override;

 private:
  std::unique_ptr<::crypto::tink::HybridEncrypt> encrypter_;
  std::string context_info_;
  uint32_t key_index_;
};

}  // namespace cobalt::util

#endif  // COBALT_SRC_LIB_UTIL_HYBRID_TINK_ENCRYPTED_MESSAGE_MAKER_H_
