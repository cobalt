// Copyright 2023 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LIB_UTIL_THREAD_H_
#define COBALT_SRC_LIB_UTIL_THREAD_H_

#include <string>
#include <thread>

namespace cobalt::util {

void NameThread(const std::string& name, std::thread& thread);

}  // namespace cobalt::util

#endif  // COBALT_SRC_LIB_UTIL_THREAD_H_
