
// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file

#ifndef COBALT_SRC_LIB_UTIL_SOURCE_LOCATION_H_
#define COBALT_SRC_LIB_UTIL_SOURCE_LOCATION_H_

#include <cstdint>

namespace cobalt::util {

// SourceLocation is used to track where the method is called to support logging status from
// util::StatusBuilder
class SourceLocation {
  // Used to ban passing in explicit arguments to SourceLocation::current()
  struct PrivateTag {
   private:
    explicit PrivateTag() = default;
    friend class SourceLocation;
  };

 public:
  SourceLocation() = delete;

  static constexpr SourceLocation current(PrivateTag tag = PrivateTag{},
                                          std::uint32_t line = __builtin_LINE(),
                                          const char* file_name = __builtin_FILE()) {
    return SourceLocation(line, file_name);
  }

  [[nodiscard]] constexpr std::uint32_t line() const { return line_; }
  [[nodiscard]] constexpr const char* file_name() const { return file_name_; }

 private:
  constexpr SourceLocation(std::uint32_t line, const char* file_name)
      : line_(line), file_name_(file_name) {}

  std::uint32_t line_;
  const char* file_name_;
};

}  // namespace cobalt::util

#endif  // COBALT_SRC_LIB_UTIL_SOURCE_LOCATION_H_
