// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LIB_UTIL_SLEEPER_H_
#define COBALT_SRC_LIB_UTIL_SLEEPER_H_

#include <chrono>
#include <thread>

namespace cobalt::util {

// Allows us to mock out sleeping for tests.
class SleeperInterface {
 public:
  virtual ~SleeperInterface() = default;

  virtual void sleep_for(const std::chrono::milliseconds& sleep_duration) = 0;
};

// Implements SleeperInterface by invoking the real std::this_thread::sleep_for
// method.
class Sleeper : public SleeperInterface {
 public:
  Sleeper() = default;

  void sleep_for(const std::chrono::milliseconds& sleep_duration) override {
    std::this_thread::sleep_for(sleep_duration);
  }
};

}  // namespace cobalt::util

#endif  // COBALT_SRC_LIB_UTIL_SLEEPER_H_
