// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/lib/util/hash.h"

#include <string>

#include <gtest/gtest.h>

namespace cobalt::util {
namespace {

char Byte(const unsigned char c) { return static_cast<char>(c); }

TEST(HashTest, FarmhashFingerprint) {
  EXPECT_EQ(FarmhashFingerprint("cobalt"), std::string({
                                               Byte(0xf1),
                                               Byte(0xc1),
                                               Byte(0xef),
                                               Byte(0x24),
                                               Byte(0x81),
                                               Byte(0x79),
                                               Byte(0xa1),
                                               Byte(0x91),
                                               Byte(0x02),
                                               Byte(0x04),
                                               Byte(0xdb),
                                               Byte(0x03),
                                               Byte(0xf9),
                                               Byte(0x8f),
                                               Byte(0x2c),
                                               Byte(0xd7),
                                           }));
}

TEST(HashTest, FarmhashFingerprint64) {
  EXPECT_EQ(FarmhashFingerprint64("cobalt"), std::string({
                                                 Byte(0x8b),
                                                 Byte(0x46),
                                                 Byte(0x65),
                                                 Byte(0xd5),
                                                 Byte(0x07),
                                                 Byte(0x36),
                                                 Byte(0x74),
                                                 Byte(0x2e),
                                             }));
}

}  // namespace
}  // namespace cobalt::util
