// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOGGER_TYPES_H_
#define COBALT_SRC_LOGGER_TYPES_H_

#include <memory>

#include <google/protobuf/repeated_field.h>

#include "src/pb/observation.pb.h"

namespace cobalt::logger {

// A HistogramPtr provides a moveable way of passing the buckets of a Histogram.
using HistogramPtr = std::unique_ptr<google::protobuf::RepeatedPtrField<HistogramBucket>>;

}  // namespace cobalt::logger

#endif  // COBALT_SRC_LOGGER_TYPES_H_
