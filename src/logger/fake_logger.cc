// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/logger/fake_logger.h"

#include <string>
#include <vector>

#include "src/public/lib/status.h"

namespace cobalt::logger::testing {

namespace {

template <class EventType>
void CopyEventCodesAndComponent(const std::vector<uint32_t>& event_codes,
                                const std::string& component, EventType* event) {
  for (auto event_code : event_codes) {
    event->add_event_code(event_code);
  }
  event->set_component(component);
}

template <class EventType>
void CopyEventCodes(const std::vector<uint32_t>& event_codes, EventType* event) {
  for (auto event_code : event_codes) {
    event->add_event_code(event_code);
  }
}

}  // namespace

// TODO(https://fxbug.dev/278930401): NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
Status FakeLogger::LogOccurrence(uint32_t metric_id, uint64_t count,
                                 const std::vector<uint32_t>& event_codes) {
  call_count_ += 1;

  Event event;
  event.set_metric_id(metric_id);
  auto* occurrence_event = event.mutable_occurrence_event();
  CopyEventCodes(event_codes, occurrence_event);
  occurrence_event->set_count(count);
  TrackEvent(event);

  return Status::OkStatus();
}

// TODO(https://fxbug.dev/278930401): NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
Status FakeLogger::LogInteger(uint32_t metric_id, int64_t value,
                              const std::vector<uint32_t>& event_codes) {
  call_count_ += 1;

  Event event;
  event.set_metric_id(metric_id);
  auto* integer_event = event.mutable_integer_event();
  CopyEventCodes(event_codes, integer_event);
  integer_event->set_value(value);
  TrackEvent(event);

  return Status::OkStatus();
}

Status FakeLogger::LogIntegerHistogram(uint32_t metric_id, HistogramPtr histogram,
                                       const std::vector<uint32_t>& event_codes) {
  call_count_ += 1;

  Event event;
  event.set_metric_id(metric_id);
  auto* integer_histogram_event = event.mutable_integer_histogram_event();
  CopyEventCodes(event_codes, integer_histogram_event);
  integer_histogram_event->mutable_buckets()->Swap(histogram.get());
  TrackEvent(event);

  return Status::OkStatus();
}

Status FakeLogger::LogString(uint32_t metric_id, const std::string& string_value,
                             const std::vector<uint32_t>& event_codes) {
  call_count_ += 1;

  Event event;
  event.set_metric_id(metric_id);
  auto* string_event = event.mutable_string_event();
  CopyEventCodes(event_codes, string_event);
  string_event->set_string_value(string_value);
  TrackEvent(event);

  return Status::OkStatus();
}

}  // namespace cobalt::logger::testing
