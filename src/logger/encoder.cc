// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/logger/encoder.h"

#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "src/pb/observation.pb.h"
#include "src/public/lib/statusor/statusor.h"

namespace cobalt::logger::encoder {

lib::statusor::StatusOr<std::unique_ptr<Observation>> EncodeIntegerObservation(
    const std::vector<std::tuple<std::vector<uint32_t>, int64_t>>& data) {
  auto observation = std::make_unique<Observation>();

  IntegerObservation* integer_observation = observation->mutable_integer();

  for (const auto& [event_codes, value] : data) {
    IntegerObservation_Value* value_proto = integer_observation->add_values();
    for (uint32_t code : event_codes) {
      value_proto->add_event_codes(code);
    }
    value_proto->set_value(value);
  }

  return observation;
}

lib::statusor::StatusOr<std::unique_ptr<Observation>> EncodeSumAndCountObservation(
    const std::vector<std::tuple<std::vector<uint32_t>, int64_t, uint32_t>>& data) {
  auto observation = std::make_unique<Observation>();

  SumAndCountObservation* sum_and_count_observation = observation->mutable_sum_and_count();

  for (const auto& [event_codes, sum, count] : data) {
    SumAndCountObservation_SumAndCount* sum_and_count =
        sum_and_count_observation->add_sums_and_counts();
    for (uint32_t code : event_codes) {
      sum_and_count->add_event_codes(code);
    }
    sum_and_count->set_sum(sum);
    sum_and_count->set_count(count);
  }

  return observation;
}

lib::statusor::StatusOr<std::unique_ptr<Observation>> EncodeIndexHistogramObservation(
    const std::vector<
        std::tuple<std::vector<uint32_t>, std::vector<std::tuple<uint32_t, int64_t>>>>& data) {
  auto observation = std::make_unique<Observation>();

  IndexHistogramObservation* index_histogram_observation = observation->mutable_index_histogram();

  for (const auto& [event_codes, histogram] : data) {
    IndexHistogram* index_histogram = index_histogram_observation->add_index_histograms();
    for (uint32_t code : event_codes) {
      index_histogram->add_event_codes(code);
    }
    for (const auto& [index, count] : histogram) {
      index_histogram->add_bucket_indices(index);
      index_histogram->add_bucket_counts(count);
    }
  }

  return observation;
}

lib::statusor::StatusOr<std::unique_ptr<Observation>> EncodeStringHistogramObservation(
    const std::vector<std::string>& hashes,
    const std::vector<std::tuple<EventCodes, Histogram>>& data, bool use_legacy_hash) {
  auto observation = std::make_unique<Observation>();

  StringHistogramObservation* string_histogram_observation =
      observation->mutable_string_histogram();

  for (const std::string& hash : hashes) {
    if (use_legacy_hash) {
      string_histogram_observation->add_string_hashes(hash);
    } else {
      string_histogram_observation->add_string_hashes_ff64(hash);
    }
  }

  for (const auto& [event_codes, histogram] : data) {
    IndexHistogram* index_histogram = string_histogram_observation->add_string_histograms();
    for (uint32_t code : event_codes) {
      index_histogram->add_event_codes(code);
    }
    for (const auto& [index, count] : histogram) {
      index_histogram->add_bucket_indices(index);
      index_histogram->add_bucket_counts(count);
    }
  }

  return observation;
}

}  // namespace cobalt::logger::encoder
