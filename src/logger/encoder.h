// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOGGER_ENCODER_H_
#define COBALT_SRC_LOGGER_ENCODER_H_

#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "src/pb/observation.pb.h"
#include "src/public/lib/statusor/statusor.h"

namespace cobalt::logger::encoder {

// A vector of event codes that the data corresponds to.
using EventCodes = std::vector<uint32_t>;

// Histogram vector of (index, count) pairs.
using Histogram = std::vector<std::tuple<uint32_t, int64_t>>;

// Encodes an Observation of type IntegerObservation.
//
// data: A vector of (event_codes, integer_value) pairs, that will be used to encode the
// Observation.
[[nodiscard]] lib::statusor::StatusOr<std::unique_ptr<Observation>> EncodeIntegerObservation(
    const std::vector<std::tuple<std::vector<uint32_t>, int64_t>>& data);

// Encodes an Observation of type SumAndCountObservation.
//
// data: A vector of (event_codes, sum, count) triples, that will be used to encode the
// Observation.
[[nodiscard]] lib::statusor::StatusOr<std::unique_ptr<Observation>> EncodeSumAndCountObservation(
    const std::vector<std::tuple<std::vector<uint32_t>, int64_t, uint32_t>>& data);

// Encodes an Observation of type IndexHistogramObservation.
//
// data: A vector of (event_codes, histogram vector of (index, count)) pairs that will be used to
// encode the Observation.
[[nodiscard]] lib::statusor::StatusOr<std::unique_ptr<Observation>> EncodeIndexHistogramObservation(
    const std::vector<
        std::tuple<std::vector<uint32_t>, std::vector<std::tuple<uint32_t, int64_t>>>>& data);

// Encodes an Observation of type StringHistogramObservation.
//
// hashes: Vector of hashes of strings (hashed using Farmhash Fingerprint128).
// data: A vector of (event_codes, histogram vector of (index, count)) pairs that will be used to
// encode the Observation. The bucket with index i in the histogram contains the count for the
// string whose hash is in position i in the hashes vector.
// use_legacy_hash: a boolean indicating if legacy hash function (Farmhash Fingerprint 128) is used
// to produce the string hashes.
//
// TODO(https://fxbug.dev/322409910): Delete usage of legacy hash after clients stop sending the
// filed.
[[nodiscard]] lib::statusor::StatusOr<std::unique_ptr<Observation>>
EncodeStringHistogramObservation(const std::vector<std::string>& hashes,
                                 const std::vector<std::tuple<EventCodes, Histogram>>& data,
                                 bool use_legacy_hash);

}  // namespace cobalt::logger::encoder

#endif  // COBALT_SRC_LOGGER_ENCODER_H_
