// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/logger/encoder.h"

#include <memory>
#include <vector>

#include <gtest/gtest.h>

#include "src/pb/observation.pb.h"
#include "src/public/lib/statusor/statusor.h"

namespace cobalt::logger::encoder {
using lib::statusor::StatusOr;

TEST(Encoder, EncodeIntegerObservationSingleValue) {
  StatusOr<std::unique_ptr<Observation>> obs_or = EncodeIntegerObservation({{{1, 2, 3}, 4}});
  ASSERT_EQ(obs_or.status(), Status::OkStatus());
  std::unique_ptr<Observation> obs = std::move(obs_or.value());
  ASSERT_TRUE(obs->has_integer());
  ASSERT_EQ(obs->integer().values_size(), 1);
  EXPECT_EQ(obs->integer().values(0).event_codes_size(), 3);
  EXPECT_EQ(obs->integer().values(0).value(), 4);
}

TEST(Encoder, EncodeIntegerObservationMultipleValues) {
  StatusOr<std::unique_ptr<Observation>> obs_or = EncodeIntegerObservation({
      {{}, 1},
      {{1}, 2},
      {{1, 2}, 3},
      {{1, 2, 3}, 4},
  });
  ASSERT_EQ(obs_or.status(), Status::OkStatus());
  std::unique_ptr<Observation> obs = std::move(obs_or.value());
  ASSERT_TRUE(obs->has_integer());
  ASSERT_EQ(obs->integer().values_size(), 4);
  for (int i = 0; i < 4; i++) {
    EXPECT_EQ(obs->integer().values(i).event_codes_size(), i);
    EXPECT_EQ(obs->integer().values(i).value(), i + 1);
  }
}

TEST(Encoder, EncodeSumAndCountObservationSingleValue) {
  StatusOr<std::unique_ptr<Observation>> obs_or = EncodeSumAndCountObservation({{{1, 2, 3}, 4, 5}});
  ASSERT_EQ(obs_or.status(), Status::OkStatus());
  std::unique_ptr<Observation> obs = std::move(obs_or.value());
  ASSERT_TRUE(obs->has_sum_and_count());
  ASSERT_EQ(obs->sum_and_count().sums_and_counts_size(), 1);
  EXPECT_EQ(obs->sum_and_count().sums_and_counts(0).event_codes_size(), 3);
  EXPECT_EQ(obs->sum_and_count().sums_and_counts(0).sum(), 4);
  EXPECT_EQ(obs->sum_and_count().sums_and_counts(0).count(), 5u);
}

TEST(Encoder, EncodeSumAndCountObservationMultipleValues) {
  StatusOr<std::unique_ptr<Observation>> obs_or =
      EncodeSumAndCountObservation({{{}, 1, 2}, {{1}, 2, 3}, {{1, 2}, 3, 4}, {{1, 2, 3}, 4, 5}});
  ASSERT_EQ(obs_or.status(), Status::OkStatus());
  std::unique_ptr<Observation> obs = std::move(obs_or.value());
  ASSERT_TRUE(obs->has_sum_and_count());
  ASSERT_EQ(obs->sum_and_count().sums_and_counts_size(), 4);
  for (int i = 0; i < 4; i++) {
    EXPECT_EQ(obs->sum_and_count().sums_and_counts(i).event_codes_size(), i);
    EXPECT_EQ(obs->sum_and_count().sums_and_counts(i).sum(), i + 1);
    EXPECT_EQ(obs->sum_and_count().sums_and_counts(i).count(), static_cast<uint64_t>(i + 2));
  }
}

TEST(Encoder, EncodeIndexHistogramObservationSingleValue) {
  StatusOr<std::unique_ptr<Observation>> obs_or =
      EncodeIndexHistogramObservation({{{1, 2, 3}, {{4, 5}}}});
  ASSERT_EQ(obs_or.status(), Status::OkStatus());
  std::unique_ptr<Observation> obs = std::move(obs_or.value());
  ASSERT_TRUE(obs->has_index_histogram());
  ASSERT_EQ(obs->index_histogram().index_histograms_size(), 1);
  EXPECT_EQ(obs->index_histogram().index_histograms(0).event_codes_size(), 3);
  ASSERT_EQ(obs->index_histogram().index_histograms(0).bucket_indices_size(), 1);
  EXPECT_EQ(obs->index_histogram().index_histograms(0).bucket_indices(0), 4u);
  ASSERT_EQ(obs->index_histogram().index_histograms(0).bucket_counts_size(), 1);
  EXPECT_EQ(obs->index_histogram().index_histograms(0).bucket_counts(0), 5);
}

TEST(Encoder, EncodeIndexHistogramObservationMultipleValues) {
  StatusOr<std::unique_ptr<Observation>> obs_or =
      EncodeIndexHistogramObservation({{{}, {{1, 2}}},
                                       {{1}, {{2, 3}, {4, 5}}},
                                       {{1, 2}, {{3, 4}, {5, 6}, {7, 8}}},
                                       {{1, 2, 3}, {{4, 5}, {6, 7}, {8, 9}, {10, 11}}}});
  ASSERT_EQ(obs_or.status(), Status::OkStatus());
  std::unique_ptr<Observation> obs = std::move(obs_or.value());
  ASSERT_TRUE(obs->has_index_histogram());
  ASSERT_EQ(obs->index_histogram().index_histograms_size(), 4);
  for (int i = 0; i < 4; i++) {
    EXPECT_EQ(obs->index_histogram().index_histograms(i).event_codes_size(), i);
    ASSERT_EQ(obs->index_histogram().index_histograms(i).bucket_indices_size(), i + 1);
    ASSERT_EQ(obs->index_histogram().index_histograms(i).bucket_counts_size(), i + 1);
    for (int j = 0; j < i + 1; j++) {
      EXPECT_EQ(obs->index_histogram().index_histograms(i).bucket_indices(j),
                static_cast<uint32_t>(i + j * 2 + 1));
      EXPECT_EQ(obs->index_histogram().index_histograms(i).bucket_counts(j), i + j * 2 + 2);
    }
  }
}

// TODO(https://fxbug.dev/322409910): Delete this test after clients stop using legacy hash.
TEST(Encoder, EncodeStringHistogramObservationSingleValueLegacy) {
  StatusOr<std::unique_ptr<Observation>> obs_or =
      EncodeStringHistogramObservation({"4"}, {{{1, 2, 3}, {{4, 5}}}}, /*use_legacy_hash=*/true);
  ASSERT_EQ(obs_or.status(), Status::OkStatus());
  std::unique_ptr<Observation> obs = std::move(obs_or.value());
  ASSERT_TRUE(obs->has_string_histogram());
  ASSERT_EQ(obs->string_histogram().string_hashes_size(), 1);
  ASSERT_EQ(obs->string_histogram().string_hashes_ff64_size(), 0);
  ASSERT_EQ(obs->string_histogram().string_hashes(0), "4");
  ASSERT_EQ(obs->string_histogram().string_histograms_size(), 1);
  EXPECT_EQ(obs->string_histogram().string_histograms(0).event_codes_size(), 3);
  ASSERT_EQ(obs->string_histogram().string_histograms(0).bucket_indices_size(), 1);
  EXPECT_EQ(obs->string_histogram().string_histograms(0).bucket_indices(0), 4u);
  ASSERT_EQ(obs->string_histogram().string_histograms(0).bucket_counts_size(), 1);
  EXPECT_EQ(obs->string_histogram().string_histograms(0).bucket_counts(0), 5);
}

TEST(Encoder, EncodeStringHistogramObservationSingleValueFF64) {
  StatusOr<std::unique_ptr<Observation>> obs_or =
      EncodeStringHistogramObservation({"4"}, {{{1, 2, 3}, {{4, 5}}}}, /*use_legacy_hash=*/false);
  ASSERT_EQ(obs_or.status(), Status::OkStatus());
  std::unique_ptr<Observation> obs = std::move(obs_or.value());
  ASSERT_TRUE(obs->has_string_histogram());
  ASSERT_EQ(obs->string_histogram().string_hashes_size(), 0);
  ASSERT_EQ(obs->string_histogram().string_hashes_ff64_size(), 1);
  ASSERT_EQ(obs->string_histogram().string_hashes_ff64(0), "4");
  ASSERT_EQ(obs->string_histogram().string_histograms_size(), 1);
  EXPECT_EQ(obs->string_histogram().string_histograms(0).event_codes_size(), 3);
  ASSERT_EQ(obs->string_histogram().string_histograms(0).bucket_indices_size(), 1);
  EXPECT_EQ(obs->string_histogram().string_histograms(0).bucket_indices(0), 4u);
  ASSERT_EQ(obs->string_histogram().string_histograms(0).bucket_counts_size(), 1);
  EXPECT_EQ(obs->string_histogram().string_histograms(0).bucket_counts(0), 5);
}

// TODO(https://fxbug.dev/322409910): Delete this test after clients stop using legacy hash.
TEST(Encoder, EncodeStringHistogramObservationMultipleValuesLegacy) {
  StatusOr<std::unique_ptr<Observation>> obs_or =
      EncodeStringHistogramObservation({"1", "2", "3", "4", "5", "6", "7", "8", "10"},
                                       {{{}, {{1, 2}}},
                                        {{1}, {{2, 3}, {4, 5}}},
                                        {{1, 2}, {{3, 4}, {5, 6}, {7, 8}}},
                                        {{1, 2, 3}, {{4, 5}, {6, 7}, {8, 9}, {10, 11}}}},
                                       /*use_legacy_hash=*/true);
  ASSERT_EQ(obs_or.status(), Status::OkStatus());
  std::unique_ptr<Observation> obs = std::move(obs_or.value());
  ASSERT_TRUE(obs->has_string_histogram());
  ASSERT_EQ(obs->string_histogram().string_hashes_size(), 9);
  ASSERT_EQ(obs->string_histogram().string_hashes_ff64_size(), 0);
  ASSERT_EQ(obs->string_histogram().string_hashes(0), "1");
  ASSERT_EQ(obs->string_histogram().string_histograms_size(), 4);
  for (int i = 0; i < 4; i++) {
    EXPECT_EQ(obs->string_histogram().string_histograms(i).event_codes_size(), i);
    ASSERT_EQ(obs->string_histogram().string_histograms(i).bucket_indices_size(), i + 1);
    ASSERT_EQ(obs->string_histogram().string_histograms(i).bucket_counts_size(), i + 1);
    for (int j = 0; j < i + 1; j++) {
      EXPECT_EQ(obs->string_histogram().string_histograms(i).bucket_indices(j),
                static_cast<uint32_t>(i + j * 2 + 1));
      EXPECT_EQ(obs->string_histogram().string_histograms(i).bucket_counts(j), i + j * 2 + 2);
    }
  }
}

TEST(Encoder, EncodeStringHistogramObservationMultipleValuesFF64) {
  StatusOr<std::unique_ptr<Observation>> obs_or =
      EncodeStringHistogramObservation({"1", "2", "3", "4", "5", "6", "7", "8", "10"},
                                       {{{}, {{1, 2}}},
                                        {{1}, {{2, 3}, {4, 5}}},
                                        {{1, 2}, {{3, 4}, {5, 6}, {7, 8}}},
                                        {{1, 2, 3}, {{4, 5}, {6, 7}, {8, 9}, {10, 11}}}},
                                       /*use_legacy_hash=*/false);
  ASSERT_EQ(obs_or.status(), Status::OkStatus());
  std::unique_ptr<Observation> obs = std::move(obs_or.value());
  ASSERT_TRUE(obs->has_string_histogram());
  ASSERT_EQ(obs->string_histogram().string_hashes_size(), 0);
  ASSERT_EQ(obs->string_histogram().string_hashes_ff64_size(), 9);
  ASSERT_EQ(obs->string_histogram().string_hashes_ff64(0), "1");
  ASSERT_EQ(obs->string_histogram().string_histograms_size(), 4);
  for (int i = 0; i < 4; i++) {
    EXPECT_EQ(obs->string_histogram().string_histograms(i).event_codes_size(), i);
    ASSERT_EQ(obs->string_histogram().string_histograms(i).bucket_indices_size(), i + 1);
    ASSERT_EQ(obs->string_histogram().string_histograms(i).bucket_counts_size(), i + 1);
    for (int j = 0; j < i + 1; j++) {
      EXPECT_EQ(obs->string_histogram().string_histograms(i).bucket_indices(j),
                static_cast<uint32_t>(i + j * 2 + 1));
      EXPECT_EQ(obs->string_histogram().string_histograms(i).bucket_counts(j), i + j * 2 + 2);
    }
  }
}

}  // namespace cobalt::logger::encoder
