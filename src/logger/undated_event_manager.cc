// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/logger/undated_event_manager.h"

#include <memory>
#include <string>

#include "src/logger/event_loggers.h"
#include "src/logger/event_record.h"
#include "src/logger/internal_metrics.h"
#include "src/public/lib/clock_interfaces.h"
#include "src/public/lib/compat/clock.h"
#include "src/registry/metric_definition.pb.h"

namespace cobalt::logger {

UndatedEventManager::UndatedEventManager(local_aggregation::LocalAggregation& local_aggregation,
                                         ObservationWriter& observation_writer,
                                         system_data::SystemDataInterface& system_data,
                                         util::CivilTimeConverterInterface& civil_time_converter,
                                         std::unique_ptr<util::BootClockInterface> boot_clock,
                                         int32_t max_saved_events)
    : local_aggregation_(local_aggregation),
      observation_writer_(observation_writer),
      system_data_(system_data),
      civil_time_converter_(civil_time_converter),
      max_saved_events_(max_saved_events),
      boot_clock_(std::move(boot_clock)) {}

Status UndatedEventManager::Save(std::unique_ptr<EventRecord> event_record) {
  auto id =
      std::make_pair(event_record->metric()->customer_id(), event_record->metric()->project_id());
  auto saved_record =
      std::make_unique<SavedEventRecord>(boot_clock_->now(), std::move(event_record));

  auto lock = protected_saved_records_fields_.lock();

  if (lock->flushed) {
    LOG(WARNING) << "Event saved after the queue has been flushed for metric: "
                 << saved_record->event_record->GetLogDetails();
    return FlushSavedRecord(std::move(saved_record), lock->reference_system_time_,
                            lock->reference_boot_time_);
  }

  // Save the event record to the FIFO queue.
  if (lock->saved_records_.size() >= max_saved_events_) {
    auto dropped_record = std::move(lock->saved_records_.front());
    lock->saved_records_.pop_front();
    ++lock->num_events_dropped_[std::make_pair(
        dropped_record->event_record->metric()->customer_id(),
        dropped_record->event_record->metric()->project_id())];
  }
  lock->saved_records_.emplace_back(std::move(saved_record));
  lock->num_events_cached_[id] += 1;

  return Status::OkStatus();
}

Status UndatedEventManager::Flush(util::SystemClockInterface* system_clock,
                                  InternalMetrics* internal_metrics) {
  // should no longer be incoming records to save as the clock is now accurate.
  auto lock = protected_saved_records_fields_.lock();

  // Get reference times that will be used to convert the saved event record's boot time to an
  // accurate system time.
  lock->reference_system_time_ = system_clock->now();
  lock->reference_boot_time_ = boot_clock_->now();

  std::unique_ptr<SavedEventRecord> saved_record;
  while (!lock->saved_records_.empty()) {
    saved_record = std::move(lock->saved_records_.front());
    auto log_details = saved_record->event_record->GetLogDetails();
    Status result = FlushSavedRecord(std::move(saved_record), lock->reference_system_time_,
                                     lock->reference_boot_time_);
    lock->saved_records_.pop_front();
    if (!result.ok()) {
      LOG_FIRST_N(ERROR, 10) << "Error " << result.error_message()
                             << " occurred while processing a saved event for metric: "
                             << log_details;
    }
  }
  NoOpInternalMetrics noop_internal_metrics;
  if (!internal_metrics) {
    internal_metrics = &noop_internal_metrics;
  }

  InternalMetrics::InternalMetricsFlusher flusher = internal_metrics->Flusher();
  // Record that we saved records due to clock inaccuracy.
  for (auto cached_events : lock->num_events_cached_) {
    internal_metrics->InaccurateClockEventsCached(cached_events.second);
  }
  lock->num_events_cached_.clear();

  // Record that we dropped events due to memory constraints.
  for (auto dropped_events : lock->num_events_dropped_) {
    internal_metrics->InaccurateClockEventsDropped(dropped_events.second);
  }
  lock->num_events_dropped_.clear();

  lock->flushed = true;

  return Status::OkStatus();
}

Status UndatedEventManager::FlushSavedRecord(
    std::unique_ptr<SavedEventRecord> saved_record,
    const std::chrono::system_clock::time_point& reference_system_time,
    const util::compat::boot_clock::time_point& reference_boot_time) {
  // Convert the recorded boot time to a system time.
  const std::chrono::seconds& time_shift = std::chrono::duration_cast<std::chrono::seconds>(
      reference_boot_time - saved_record->boot_time);
  std::chrono::system_clock::time_point event_timestamp = reference_system_time - time_shift;

  auto event_logger = internal::EventLogger::Create(
      saved_record->event_record->metric()->metric_type(), local_aggregation_, observation_writer_,
      system_data_, civil_time_converter_);

  if (event_logger == nullptr) {
    return Status(StatusCode::INVALID_ARGUMENT, "No event_logger found");
  }
  return event_logger->Log(std::move(saved_record->event_record), event_timestamp);
}

int UndatedEventManager::NumSavedEvents() const {
  return static_cast<int>(protected_saved_records_fields_.const_lock()->saved_records_.size());
}

}  // namespace cobalt::logger
