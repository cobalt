// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOGGER_AT_TIME_POINT_LOGGER_INTERFACE_H_
#define COBALT_SRC_LOGGER_AT_TIME_POINT_LOGGER_INTERFACE_H_

#include <chrono>
#include <vector>

#include "src/logger/types.h"
#include "src/public/lib/status.h"

namespace cobalt::logger {

// AtTimePointLoggerInterface is the client-facing interface to Cobalt on platforms where Cobalt
// does not run as a service. On those platforms, we rely on the caller to provide timestamps
// for log events directly rather than inferring those timestamps from the system clock.
class AtTimePointLoggerInterface {
 public:
  // AtTimePointLogger should be move-only
  AtTimePointLoggerInterface(AtTimePointLoggerInterface const&) = delete;
  AtTimePointLoggerInterface& operator=(AtTimePointLoggerInterface const&) = delete;
  explicit AtTimePointLoggerInterface() = default;

  virtual ~AtTimePointLoggerInterface() = default;

  // Logs that an occurrence event happened at a particular point in time given
  // by |event_timestamp|.
  virtual Status LogOccurrence(uint32_t metric_id, uint64_t count,
                               const ::std::vector<uint32_t>& event_codes,
                               const ::std::chrono::system_clock::time_point& event_timestamp) = 0;

  // Logs that an integer log event happened at a particular point in time given
  // by |event_timestamp|.
  virtual Status LogInteger(uint32_t metric_id, int64_t value,
                            const ::std::vector<uint32_t>& event_codes,
                            const ::std::chrono::system_clock::time_point& event_timestamp) = 0;

  // Logs that an integer histogram log event happened at a particular point in
  // time given by |event_timestamp|.
  virtual Status LogIntegerHistogram(
      uint32_t metric_id, HistogramPtr histogram, const ::std::vector<uint32_t>& event_codes,
      const ::std::chrono::system_clock::time_point& event_timestamp) = 0;

  // Logs that an string log event happened at a particular point in time given
  // by |event_timestamp|.
  virtual Status LogString(uint32_t metric_id, const std::string& string_value,
                           const ::std::vector<uint32_t>& event_codes,
                           const ::std::chrono::system_clock::time_point& event_timestamp) = 0;

  // Pauses Cobalt's internal metrics collection.
  virtual void PauseInternalLogging() = 0;

  // Resumes Cobalt's internal metrics collection.
  virtual void ResumeInternalLogging() = 0;
};

}  // namespace cobalt::logger

#endif  // COBALT_SRC_LOGGER_AT_TIME_POINT_LOGGER_INTERFACE_H_
