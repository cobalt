// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOGGER_FAKE_LOGGER_H_
#define COBALT_SRC_LOGGER_FAKE_LOGGER_H_

#include <map>
#include <string>
#include <utility>
#include <vector>

#include "src/logger/logger_interface.h"
#include "src/pb/event.pb.h"
#include "src/public/lib/status.h"

namespace cobalt::logger::testing {

// An implementation of LoggerInterface that counts how many times the Log*
// methods were called for purposes of testing that internal metrics are being
// collected properly.
class FakeLogger : public LoggerInterface {
 public:
  Status LogOccurrence(uint32_t metric_id, uint64_t count,
                       const std::vector<uint32_t>& event_codes) override;

  Status LogInteger(uint32_t metric_id, int64_t value,
                    const std::vector<uint32_t>& event_codes) override;

  Status LogIntegerHistogram(uint32_t metric_id, HistogramPtr histogram,
                             const std::vector<uint32_t>& event_codes) override;

  Status LogString(uint32_t metric_id, const std::string& string_value,
                   const std::vector<uint32_t>& event_codes) override;

  void RecordLoggerCall(LoggerCallsMadeMigratedMetricDimensionLoggerMethod method) override {
    if (!internal_logging_paused_) {
      internal_logger_calls_[method]++;
    }
  }

  void PauseInternalLogging() override { internal_logging_paused_ = true; }

  void ResumeInternalLogging() override { internal_logging_paused_ = false; }

  void TrackEvent(Event e) {
    events_logged_.push_back(e);
    last_event_logged_ = std::move(e);
  }
  uint32_t call_count() const { return call_count_; }
  Event last_event_logged() const { return last_event_logged_; }
  Event nth_event_logged(int64_t n) const { return events_logged_[n]; }
  std::map<LoggerCallsMadeMigratedMetricDimensionLoggerMethod, uint32_t> internal_logger_calls() {
    return internal_logger_calls_;
  }

 private:
  Event last_event_logged_;
  std::vector<Event> events_logged_;
  uint32_t call_count_ = 0;
  std::map<LoggerCallsMadeMigratedMetricDimensionLoggerMethod, uint32_t> internal_logger_calls_;
  bool internal_logging_paused_ = false;
};

}  // namespace cobalt::logger::testing

#endif  //  COBALT_SRC_LOGGER_FAKE_LOGGER_H_
