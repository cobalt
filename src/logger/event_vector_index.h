// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOGGER_EVENT_VECTOR_INDEX_H_
#define COBALT_SRC_LOGGER_EVENT_VECTOR_INDEX_H_

#include <google/protobuf/repeated_field.h>

#include "src/public/lib/statusor/statusor.h"
#include "src/registry/metric_definition.pb.h"

namespace cobalt::logger {

// Returns an integer representing |event_vector| with respect to the metric dimensions of
// |metric_def|, or else returns an error if |event_vector| is not a valid event vector for
// |metric_def|.
//
// If |metric_def| has M metric dimensions, and if n_j is the number of valid event codes for
// dimension j, then N := (n_1 * ... * n_M) is the number of valid event vectors for |metric_def|.
// EventVectorToIndex is a 1-to-1 map from the set of valid event vectors onto the range [0, N-1].
lib::statusor::StatusOr<uint64_t> EventVectorToIndex(std::vector<uint32_t> event_vector,
                                                     const MetricDefinition &metric_def);

// Returns the event vector corresponding to |index| with respect to |metric_dimensions|, or else
// returns an error if |index| does not correspond to a valid event vector.
//
// This function is inverse to EventVectorToIndex() if |metric_dimensions| are the MetricDimensions
// of the MetricDefinition passed to EventVectorToIndex(). With N as defined in the comment on
// EventVectorToIndex(), any |index| in the range [0, N-1] should result in an OK status.
lib::statusor::StatusOr<std::vector<uint32_t>> EventVectorFromIndex(
    uint64_t index,
    const google::protobuf::RepeatedPtrField<MetricDefinition::MetricDimension> &metric_dimensions);

// A helper function which returns the number of valid event codes associated to |metric_dim|.
// There are 2 cases:
//
// - If a max_event_code is specified in |metric_dim|, then every integer in the
//   range [0, max_event_code] is a valid event code.
//
// - Otherwise, an event code is valid if and only if it is enumerated in |metric_dim|.
uint64_t GetNumEventCodes(const MetricDefinition::MetricDimension &metric_dim);

// A helper function which returns the number of valid event vectors associated to
// |metric_dimensions|.
uint64_t GetNumEventVectors(
    const google::protobuf::RepeatedPtrField<MetricDefinition::MetricDimension> &metric_dimensions);

// A helper function which returns the index of |event_code| in an enumeration of the valid event
// codes for |metric_dim|, or else returns an error if |event_code| is not a valid event code for
// |metric_dim|.
lib::statusor::StatusOr<uint64_t> EventCodeToIndexForDimension(
    uint32_t event_code, const MetricDefinition::MetricDimension &metric_dim);

// A helper function which, given an |index| into an enumeration of the valid event codes for
// |metric_dim|, returns the corresponding event code. An error is returned if |index| does not
// correspond to a valid event code of |metric_dim|.
lib::statusor::StatusOr<uint32_t> EventCodeFromIndexForDimension(
    uint64_t index, const MetricDefinition::MetricDimension &metric_dim);

}  // namespace cobalt::logger

#endif  // COBALT_SRC_LOGGER_EVENT_VECTOR_INDEX_H_
