// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOGGER_LOGGER_INTERFACE_H_
#define COBALT_SRC_LOGGER_LOGGER_INTERFACE_H_

#include <string>
#include <vector>

#include "src/logger/internal_metrics_config.cb.h"
#include "src/logger/types.h"
#include "src/public/lib/status.h"

namespace cobalt::logger {

// Logger is the client-facing interface to Cobalt.
//
// LoggerInterface is an abstract interface to Logger that allows Logger to
// be mocked out in tests.
class LoggerInterface {
 public:
  // LoggerInterface should be move-only
  LoggerInterface(LoggerInterface const&) = delete;
  LoggerInterface& operator=(LoggerInterface const&) = delete;
  explicit LoggerInterface() = default;

  virtual ~LoggerInterface() = default;

  // Logs that an event has occurred a given number of times.
  //
  // |metric_id| ID of the Metric the logged Event will belong to. It must
  // be one of the Metrics from the ProjectContext passed to the constructor,
  // and it must be of type OCCURRENCE.
  //
  // |count| The number of times the event occurred. One may choose to
  // always set this value to 1 if events are logged every time they occur.
  //
  // |event_codes| The event codes for the event that occurred. There must be
  // one for each metric dimension specified in the MetricDefinition. Use the
  // empty vector if no metric dimensions have been defined.
  virtual Status LogOccurrence(uint32_t metric_id, uint64_t count,
                               const std::vector<uint32_t>& event_codes) = 0;

  // Logs a measured integer value.
  //
  // |metric_id| ID of the Metric the logged Event will belong to. It must
  // be one of the Metrics from the ProjectContext passed to the constructor,
  // and it must be of type INTEGER.
  //
  // |value| The integer measured.
  //
  // |event_codes| The event codes for the event that occurred. There must be
  // one for each metric dimension specified in the MetricDefinition. Use the
  // empty vector if no metric dimensions have been defined.
  virtual Status LogInteger(uint32_t metric_id, int64_t value,
                            const std::vector<uint32_t>& event_codes) = 0;

  // Logs a histogram over a set of integer buckets. The meaning of the
  // Metric and the buckets is specified in the Metric definition.
  //
  // |metric_id| ID of the Metric the logged Event will belong to. It must
  // be one of the Metrics from the ProjectContext passed to the constructor,
  // and it must be of type INTEGER_HISTOGRAM.
  //
  // |histogram| The histogram to log. Each HistogramBucket gives the count
  //  for one bucket of the histogram. The definitions of the buckets is
  //  given in the Metric definition.
  //
  // |event_codes| The event codes for the event that occurred. There must be
  // one for each metric dimension specified in the MetricDefinition. Use the
  // empty vector if no metric dimensions have been defined.
  virtual Status LogIntegerHistogram(uint32_t metric_id, HistogramPtr histogram,
                                     const std::vector<uint32_t>& event_codes) = 0;

  // Logs a string value that was observed.
  //
  // |metric_id| ID of the Metric the logged Event will belong to. It must
  // be one of the Metrics from the ProjectContext passed to the constructor,
  // and it must be of type STRING.
  //
  // |string_value| The string that was observed.
  //
  // |event_codes| The event codes for the event that occurred. There must be
  // one for each metric dimension specified in the MetricDefinition. Use the
  // empty vector if no metric dimensions have been defined.
  virtual Status LogString(uint32_t metric_id, const std::string& string_value,
                           const std::vector<uint32_t>& event_codes) = 0;

  // LoggerCalled (cobalt_internal::metrics::logger_calls_made_migrated) is logged
  // for every call to Logger along with which method was called.
  virtual void RecordLoggerCall(LoggerCallsMadeMigratedMetricDimensionLoggerMethod method) = 0;

  // Pauses Cobalt's internal metrics collection.
  virtual void PauseInternalLogging() = 0;

  // Resumes Cobalt's internal metrics collection.
  virtual void ResumeInternalLogging() = 0;
};

}  // namespace cobalt::logger

#endif  // COBALT_SRC_LOGGER_LOGGER_INTERFACE_H_
