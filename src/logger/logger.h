// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOGGER_LOGGER_H_
#define COBALT_SRC_LOGGER_LOGGER_H_

#include <memory>
#include <string>
#include <vector>

#include "src/lib/util/not_null.h"
#include "src/local_aggregation/local_aggregation.h"
#include "src/logger/at_time_point_logger_interface.h"
#include "src/logger/internal_metrics.h"
#include "src/logger/internal_metrics_config.cb.h"
#include "src/logger/logger_interface.h"
#include "src/logger/observation_writer.h"
#include "src/logger/project_context.h"
#include "src/logger/undated_event_manager.h"

namespace cobalt::logger {

// Concrete implementation of both LoggerInterface and AtTimePointLoggerInterface.
//
// After constructing a Logger use the Log*() methods to Log Events to Cobalt.
//
// There should be an instance of Logger for each client-side Project.
// On a Fuchsia system instances of Logger are created by the Cobalt FIDL
// service for each FIDL connection from a client project.
class Logger : public LoggerInterface, public AtTimePointLoggerInterface {
 public:
  // Constructor for a Logger when the system clock is, and will remain, accurate.
  //
  // |project_context| The ProjectContext of the client-side project for which
  // the Logger will log events.
  //
  // |local_aggregation| The system's singleton instance of LocalAggregation.
  // This must remain valid as long as the Logger is being used. The Logger
  // uses this to aggregate values derived from Cobalt Events and to produce
  // locally aggregated Observations.
  //
  // |observation_writer| An instance of ObservationWriter, used by the Logger
  // to write immediate Observations to an ObservationStore. Must remain valid
  // as long as the Logger is in use.
  //
  // |system_data| A pointer to a SystemDataInterface.
  //
  // |civil_time_converter| The system's singleton instance of CivilTimeConverterInterface.
  // Must remain valid as long as the logger is used. The Logger uses this to convert
  // absolute times to civil times for metrics with the OTHER_TIME_ZONE TimeZonePolicy.
  //
  // |experiment_ids| The set of all active experiments to be associated with
  // this logger.
  //
  // |internal_metrics| An instance of InternalMetrics, used internally by the
  // Logger to send metrics about Cobalt to Cobalt. If nullptr, no such internal
  // logging will be performed by this Logger.
  Logger(util::NotNullUniquePtr<ProjectContext> project_context,
         local_aggregation::LocalAggregation& local_aggregation,
         ObservationWriter& observation_writer, system_data::SystemDataInterface& system_data,
         util::CivilTimeConverterInterface& civil_time_converter,
         std::vector<uint32_t> experiment_ids, InternalMetrics* internal_metrics = nullptr);

  // Constructor for a Logger when the clock is not yet accurate.
  //
  // |project_context| The ProjectContext of the client-side project for which
  // the Logger will log events.
  //
  // |local_aggregation| The system's singleton instance of LocalAggregation.
  // This must remain valid as long as the Logger is being used. The Logger
  // uses this to aggregate values derived from Cobalt Events and to produce
  // locally aggregated Observations.
  //
  // |observation_writer| An instance of ObservationWriter, used by the Logger
  // to write immediate Observations to an ObservationStore. Must remain valid
  // as long as the Logger is in use.
  //
  // |system_data| A pointer to a SystemDataInterface.
  //
  // |validated_clock| An instance of a clock that refuses to provide the time if a quality
  // condition is not satisfied.
  //
  // |civil_time_converter| The system's singleton instance of CivilTimeConverterInterface.
  // Must remain valid as long as the logger is used. The Logger uses this to convert
  // absolute times to civil times for metrics with the OTHER_TIME_ZONE TimeZonePolicy.
  //
  // |experiment_ids| The set of all active experiments to be associated with
  // this logger.
  //
  // |undated_event_manager| An instance of UndatedEventManager to use to save events until the
  // clock becomes accurate.
  //
  // |internal_metrics| An instance of InternalMetrics, used internally by the
  // Logger to send metrics about Cobalt to Cobalt. If nullptr, no such internal
  // logging will be performed by this Logger.
  Logger(util::NotNullUniquePtr<ProjectContext> project_context,
         local_aggregation::LocalAggregation& local_aggregation,
         ObservationWriter& observation_writer, system_data::SystemDataInterface& system_data,
         util::ValidatedClockInterface* validated_clock,
         util::CivilTimeConverterInterface& civil_time_converter,
         std::weak_ptr<UndatedEventManager> undated_event_manager,
         std::vector<uint32_t> experiment_ids, InternalMetrics* internal_metrics = nullptr);

  ~Logger() override = default;

  // Logging Methods

  Status LogOccurrence(uint32_t metric_id, uint64_t count,
                       const std::vector<uint32_t>& event_codes) override;

  Status LogInteger(uint32_t metric_id, int64_t value,
                    const std::vector<uint32_t>& event_codes) override;

  Status LogIntegerHistogram(uint32_t metric_id, HistogramPtr histogram,
                             const std::vector<uint32_t>& event_codes) override;

  Status LogString(uint32_t metric_id, const std::string& string_value,
                   const std::vector<uint32_t>& event_codes) override;

  // Logs that an occurrence event happened at a particular point in time given
  // by |event_timestamp|. In contrast, the corresponding method without the |event_timestamp|
  // parameter assumes that the event happened at the time the method is called.
  // This method implements the one in the AtTimePointLoggerInterface and is intended to be used
  // on platforms where Cobalt does not run as a service.
  Status LogOccurrence(uint32_t metric_id, uint64_t count, const std::vector<uint32_t>& event_codes,
                       const std::chrono::system_clock::time_point& event_timestamp) override;

  // Logs that an integer log event happened at a particular point in time given
  // by |event_timestamp|. In contrast, the corresponding method without the |event_timestamp|
  // parameter assumes that the event happened at the time the method is called.
  // This method implements the one in the AtTimePointLoggerInterface and is intended to be used
  // on platforms where Cobalt does not run as a service.
  Status LogInteger(uint32_t metric_id, int64_t value, const std::vector<uint32_t>& event_codes,
                    const std::chrono::system_clock::time_point& event_timestamp) override;

  // Logs that an integer histogram log event happened at a particular point in
  // time given by |event_timestamp|. In contrast, the corresponding method without the
  // |event_timestamp|
  // parameter assumes that the event happened at the time the method is called.
  // This method implements the one in the AtTimePointLoggerInterface and is intended to be used
  // on platforms where Cobalt does not run as a service.
  Status LogIntegerHistogram(uint32_t metric_id, HistogramPtr histogram,
                             const std::vector<uint32_t>& event_codes,
                             const std::chrono::system_clock::time_point& event_timestamp) override;

  // Logs that an string log event happened at a particular point in time given
  // by |event_timestamp|. In contrast, the corresponding method without the |event_timestamp|
  // parameter assumes that the event happened at the time the method is called.
  // This method implements the one in the AtTimePointLoggerInterface and is intended to be used
  // on platforms where Cobalt does not run as a service.
  Status LogString(uint32_t metric_id, const std::string& string_value,
                   const std::vector<uint32_t>& event_codes,
                   const std::chrono::system_clock::time_point& event_timestamp) override;

  // LoggerCalled (cobalt_internal::metrics::logger_calls_made_migrated) is logged
  // for every call to Logger along with which method was called.
  void RecordLoggerCall(LoggerCallsMadeMigratedMetricDimensionLoggerMethod method) override {
    InternalMetrics::InternalMetricsFlusher flusher = internal_metrics_.Flusher();
    internal_metrics_.LoggerCalled(method, project_context_->project());
  }

  // Pauses Cobalt's internal metrics collection.
  void PauseInternalLogging() override;

  // Resumes Cobalt's internal metrics collection.
  void ResumeInternalLogging() override;

 private:
  friend class LoggerTest;

  // Constructs an appropriate event logger and sends it an |event_record|. The timestamp for the
  // event record is determined by the current time.
  // All experiment IDs are added to the event_logger.
  Status Log(uint32_t metric_id, MetricDefinition::MetricType metric_type,
             std::unique_ptr<EventRecord> event_record);

  // Constructs an appropriate event logger and sends it an |event_record| with given
  // |event_timestamp| to log.
  // All experiment IDs are added to the event_logger.
  Status LogAtTimePoint(uint32_t metric_id, MetricDefinition::MetricType metric_type,
                        std::unique_ptr<EventRecord> event_record,
                        const std::chrono::system_clock::time_point& event_timestamp);

  // ProjectContext is shared as it is used in all created EventRecords, which can outlive the
  // Logger class.
  const util::NotNullSharedPtr<const ProjectContext> project_context_;

  local_aggregation::LocalAggregation& local_aggregation_;
  const ObservationWriter& observation_writer_;
  const system_data::SystemDataInterface& system_data_;
  util::ValidatedClockInterface* validated_clock_;
  std::unique_ptr<util::ValidatedClockInterface> local_validated_clock_;
  util::CivilTimeConverterInterface& civil_time_converter_;
  std::weak_ptr<UndatedEventManager> undated_event_manager_;
  std::vector<uint32_t> experiment_ids_;

  // Is this instance of Logger associated with Cobalt's own
  // internal logging, a.k.a Cobalt-measuring-Cobalt.
  bool is_internal_logger_;

  InternalMetricsPauseWrapper internal_metrics_;
};

}  // namespace cobalt::logger

#endif  // COBALT_SRC_LOGGER_LOGGER_H_
