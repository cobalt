// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/system_data/configuration_data.h"

#include "src/lib/clearcut/clearcut_log_source.pb.h"

namespace cobalt::system_data {

using cobalt::clearcut_protos::LogSourceEnum::LogSource;
using cobalt::clearcut_protos::LogSourceEnum::TURQUOISE_COBALT_SHUFFLER_INPUT_DEVEL;
using cobalt::clearcut_protos::LogSourceEnum::TURQUOISE_COBALT_SHUFFLER_INPUT_PROD;
using cobalt::clearcut_protos::LogSourceEnum::UNKNOWN;

const char* EnvironmentString(const Environment& environment) {
  switch (environment) {
    case PROD:
      return "PROD";
    case DEVEL:
      return "DEVEL";
    case LOCAL:
      return "LOCAL";
  }
}

std::ostream& operator<<(std::ostream& os, Environment environment) {
  return os << EnvironmentString(environment);
}

LogSource ConfigurationData::GetLogSourceId() const {
  switch (environment_) {
    case PROD:
      return TURQUOISE_COBALT_SHUFFLER_INPUT_PROD;
    case DEVEL:
      return TURQUOISE_COBALT_SHUFFLER_INPUT_DEVEL;
    case LOCAL:
      return UNKNOWN;
  }
}

}  // namespace cobalt::system_data
