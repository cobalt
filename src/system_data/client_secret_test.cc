// Copyright 2016 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/system_data/client_secret.h"

#include <string>
#include <utility>

#include <gtest/gtest.h>

namespace cobalt::system_data {

// Tests the basic functionality of ClientSecret.
TEST(ClientSecretTest, BasicTest) {
  // Generate two ClientSecrets and get tokens for them.
  ClientSecret secret1 = ClientSecret::GenerateNewSecret();
  std::string token1 = secret1.GetToken();

  ClientSecret secret2 = ClientSecret::GenerateNewSecret();
  std::string token2 = secret2.GetToken();

  // Now make copies of the secrets from their tokens.
  ClientSecret secret1b = ClientSecret::FromToken(token1);
  ClientSecret secret2b = ClientSecret::FromToken(token2);

  // Check that the two secrets are different from each other but
  // the copies are equal to their originals.
  EXPECT_EQ(secret1, secret1b);
  EXPECT_EQ(secret2, secret2b);
  EXPECT_NE(secret1, secret2);

  // Construct secret1c by moving data out of secret1b.
  // Now secret1 should equal secret 1c.
  ClientSecret secret1c(std::move(secret1b));
  EXPECT_EQ(secret1, secret1c);

  // All secrets are valid except for 1b because it was moved from.
  EXPECT_TRUE(secret1.valid());
  EXPECT_FALSE(secret1b.valid());  // NOLINT bugprone-use-after-move
  EXPECT_TRUE(secret1c.valid());
  EXPECT_TRUE(secret2.valid());
  EXPECT_TRUE(secret2b.valid());

  // A bad token yields an invalid ClientSecret.
  ClientSecret invalid_secret = ClientSecret::FromToken("fake token");
  EXPECT_FALSE(invalid_secret.valid());
  EXPECT_EQ(std::string(), invalid_secret.GetToken());
}

}  // namespace cobalt::system_data
