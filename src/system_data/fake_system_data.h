// Copyright 2017 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_SYSTEM_DATA_FAKE_SYSTEM_DATA_H_
#define COBALT_SRC_SYSTEM_DATA_FAKE_SYSTEM_DATA_H_

#include <string>
#include <vector>

#include "src/pb/common.pb.h"
#include "src/system_data/system_data.h"

namespace cobalt::system_data {

// Mock of the SystemDataInterface. Used for testing.
class FakeSystemData : public SystemDataInterface {
 public:
  FakeSystemData() {
    system_profile_.set_os(SystemProfile::FUCHSIA);
    system_profile_.set_arch(SystemProfile::ARM_64);
    system_profile_.set_board_name("Testing Board");
    system_profile_.set_product_name("Testing Product");
    system_profile_.set_app_version("Testing App Version");
    system_profile_.set_channel("Testing Channel");
    system_profile_.add_experiment_ids(1);
    system_profile_.add_experiment_ids(2);
  }

  const SystemProfile& system_profile() const override { return system_profile_; }

  void SetVersion(const std::string& version) {
    system_profile_.set_system_version(version);
    NotifyChange();
  }

  void SetAppVersion(const std::string& app_version) override {
    system_profile_.set_app_version(app_version);
    NotifyChange();
  }

  void SetChannel(const std::string& channel) override {
    system_profile_.set_channel(channel);
    NotifyChange();
  }

  void SetSoftwareDistributionInfo(SoftwareDistributionInfo info) override {
    if (info.channel) {
      system_profile_.set_channel(info.channel.value());
    }
    NotifyChange();
  }

  const std::string& app_version() const override { return system_profile_.app_version(); }

  const std::string& channel() const override { return system_profile_.channel(); }

  const ReleaseStage& release_stage() const override { return release_stage_; }

  void ResetInternalMetrics(logger::InternalMetrics* internal_metrics) override {}

  void OnChange(std::function<void()> callback) override {
    change_callbacks_.push_back(std::move(callback));
  }

  void NotifyChange() {
    for (const auto& callback : change_callbacks_) {
      callback();
    }
  }

  void ResetChangeCallbacks() { change_callbacks_.clear(); }

 private:
  SystemProfile system_profile_;
  ReleaseStage release_stage_ = ReleaseStage::GA;
  std::vector<std::function<void()>> change_callbacks_;
};

}  // namespace cobalt::system_data

#endif  // COBALT_SRC_SYSTEM_DATA_FAKE_SYSTEM_DATA_H_
