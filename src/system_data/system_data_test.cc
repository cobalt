// Copyright 2017 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/system_data/system_data.h"

#include <stdio.h>

#include <set>
#include <string>

#include <gtest/gtest.h>

#include "src/logger/fake_logger.h"
#include "src/logger/internal_metrics.h"
#include "src/logging.h"
#include "src/pb/common.pb.h"

namespace cobalt::system_data {

TEST(SystemDataTest, BasicTest) {
  std::vector<int64_t> experiment_ids;
  experiment_ids.push_back(1);
  experiment_ids.push_back(2);

  SystemData system_data("test_product", "", GA, "test_version", SystemProfile::ENG,
                         experiment_ids);
  EXPECT_NE(SystemProfile::UNKNOWN_OS, system_data.system_profile().os());
  EXPECT_NE(SystemProfile::UNKNOWN_ARCH, system_data.system_profile().arch());
  EXPECT_EQ(ReleaseStage::GA, system_data.release_stage());
  EXPECT_NE(system_data.system_profile().board_name(), "");
  EXPECT_EQ(system_data.system_profile().product_name(), "test_product");
  EXPECT_EQ(system_data.system_profile().system_version(), "test_version");

  EXPECT_EQ(system_data.system_profile().app_version(), "<unset>");
  EXPECT_EQ(system_data.system_profile().channel(), "<unset>");

  EXPECT_EQ(system_data.app_version(), "<unset>");
  EXPECT_EQ(system_data.channel(), "<unset>");

  EXPECT_EQ(system_data.system_profile().build_type(), SystemProfile::ENG);

  ASSERT_EQ(system_data.system_profile().experiment_ids_size(), 2);
  EXPECT_EQ(system_data.system_profile().experiment_ids(0), 1);
  EXPECT_EQ(system_data.system_profile().experiment_ids(1), 2);

  // Board names we expect to see.
  std::set<std::string> expected_board_names = {"Eve", "Generic ARM"};

  // CPU signatures we expect to see.
  static const std::set<int> expected_signatures = {
      0x0306D4,  // Intel Broadwell (model=0x3D family=0x6) stepping=0x4
      0x0306F0,  // Intel Broadwell (model=0x3F family=0x6) stepping=0x0
      0x0406E3,  // Intel Broadwell (model=0x4E family=0x6) stepping=0x3
      0x0406F1,  // Intel Broadwell (model=0x4F family=0x6) stepping=0x1
  };

  auto name = system_data.system_profile().board_name();
  std::string unknown_prefix = "unknown:";
  if (name.starts_with(unknown_prefix)) {
    int signature = 0;
    sscanf(name.c_str(), "unknown:0x%X", &signature);
    if (!expected_signatures.contains(signature)) {
      LOG(WARNING) << "***** found new signature: " << signature;
    }
    EXPECT_GE(signature, 0x030000);
    EXPECT_LE(signature, 0x900000);
  } else {
    EXPECT_NE(0ul, expected_board_names.count(name));
  }
}

TEST(SystemDataTest, SetAppVersionTest) {
  SystemData system_data("test_product", "", ReleaseStage::DEBUG);
  EXPECT_EQ(system_data.app_version(), "<unset>");
  EXPECT_EQ(system_data.release_stage(), ReleaseStage::DEBUG);
  system_data.SetAppVersion("App version");
  EXPECT_EQ(system_data.app_version(), "App version");
  EXPECT_EQ(system_data.release_stage(), ReleaseStage::DEBUG);
}

TEST(SystemDataTest, SetChannelTest) {
  SystemData system_data("test_product", "", ReleaseStage::DEBUG);
  EXPECT_EQ(system_data.channel(), "<unset>");
  EXPECT_EQ(system_data.release_stage(), ReleaseStage::DEBUG);
  system_data.SetChannel("Channel");
  EXPECT_EQ(system_data.channel(), "Channel");
  EXPECT_EQ(system_data.release_stage(), ReleaseStage::DEBUG);
}

TEST(SystemDataTest, SetSoftwareDistributionInfoTest) {
  SystemData system_data("test_product", "", ReleaseStage::DEBUG);
  logger::testing::FakeLogger logger;
  logger::InternalMetricsImpl internal_metrics(logger, nullptr);
  system_data.ResetInternalMetrics(&internal_metrics);

  EXPECT_EQ(system_data.channel(), "<unset>");

  SoftwareDistributionInfo info;
  system_data.SetSoftwareDistributionInfo(info);
  EXPECT_EQ(system_data.channel(), "<unset>");

  info.channel = "test_channel";
  system_data.SetSoftwareDistributionInfo(info);
  EXPECT_EQ(system_data.channel(), "test_channel");

  info.channel = std::nullopt;
  system_data.SetSoftwareDistributionInfo(info);
  EXPECT_EQ(system_data.channel(), "test_channel");

  info.channel = "dogfood_release";
  system_data.SetSoftwareDistributionInfo(info);
  EXPECT_EQ(system_data.channel(), "dogfood_release");
}

}  // namespace cobalt::system_data
