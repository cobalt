// Copyright 2016 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/system_data/client_secret.h"

#include "src/lib/crypto_util/random.h"
#include "third_party/abseil-cpp/absl/strings/escaping.h"

namespace cobalt::system_data {

// static
ClientSecret ClientSecret::GenerateNewSecret() {
  crypto::Random rand;
  return GenerateNewSecret(&rand);
}

// static
ClientSecret ClientSecret::GenerateNewSecret(crypto::Random* rand) {
  ClientSecret client_secret;
  client_secret.bytes_.resize(kNumSecretBytes);
  rand->RandomBytes(reinterpret_cast<byte*>(client_secret.bytes_.data()), kNumSecretBytes);
  return client_secret;
}

// static
ClientSecret ClientSecret::FromToken(const std::string& token) {
  ClientSecret client_secret;
  absl::Base64Unescape(token, &client_secret.bytes_);
  return client_secret;
}

std::string ClientSecret::GetToken() { return absl::Base64Escape(bytes_); }

}  // namespace cobalt::system_data
