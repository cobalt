// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_HOURLY_AGGREGATION_PROCEDURE_H_
#define COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_HOURLY_AGGREGATION_PROCEDURE_H_

#include <memory>
#include <set>
#include <vector>

#include "src/local_aggregation/aggregation_procedures/aggregation_procedure.h"
#include "src/public/lib/statusor/statusor.h"
#include "src/registry/report_definition.pb.h"

namespace cobalt::local_aggregation {

class HourlyAggregationProcedure : public AggregationProcedure {
 public:
  HourlyAggregationProcedure(const MetricDefinition &metric, const ReportDefinition &report)
      : AggregationProcedure(metric, report) {}

  lib::statusor::StatusOr<std::unique_ptr<Observation>> GenerateSingleObservation(
      const std::vector<AggregateDataToGenerate> &buckets,
      const std::set<std::vector<uint32_t>> & /* event_vectors */,
      const util::TimeInfo & /*time_info*/) final;

  [[nodiscard]] bool IsDaily() const final { return false; }

 protected:
  virtual lib::statusor::StatusOr<std::unique_ptr<Observation>> GenerateHourlyObservation(
      const AggregateDataToGenerate &bucket) = 0;
};

}  // namespace cobalt::local_aggregation

#endif  // COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_HOURLY_AGGREGATION_PROCEDURE_H_
