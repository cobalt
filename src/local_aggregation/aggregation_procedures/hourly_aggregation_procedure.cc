// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/local_aggregation/aggregation_procedures/hourly_aggregation_procedure.h"

namespace cobalt::local_aggregation {

lib::statusor::StatusOr<std::unique_ptr<Observation>>
HourlyAggregationProcedure::GenerateSingleObservation(
    const std::vector<AggregateDataToGenerate> &buckets,
    const std::set<std::vector<uint32_t>> & /*event_vectors*/,
    const util::TimeInfo & /*time_info*/) {
  if (buckets.size() != 1) {
    return Status(StatusCode::FAILED_PRECONDITION,
                  "Hourly metrics only have 1 aggregation period bucket");
  }

  return GenerateHourlyObservation(buckets[0]);
}

}  // namespace cobalt::local_aggregation
