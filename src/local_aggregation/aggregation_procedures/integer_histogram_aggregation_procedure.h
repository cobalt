// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_INTEGER_HISTOGRAM_AGGREGATION_PROCEDURE_H_
#define COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_INTEGER_HISTOGRAM_AGGREGATION_PROCEDURE_H_

#include "src/lib/client/cpp/buckets_config.h"
#include "src/local_aggregation/aggregation_procedures/hourly_aggregation_procedure.h"
#include "src/public/lib/statusor/statusor.h"
#include "src/registry/metric_definition.pb.h"
#include "src/registry/report_definition.pb.h"

namespace cobalt::local_aggregation {

class IntegerHistogramAggregationProcedure : public HourlyAggregationProcedure {
 public:
  explicit IntegerHistogramAggregationProcedure(const MetricDefinition &metric,
                                                const ReportDefinition &report);

  void UpdateAggregateData(const logger::EventRecord &event_record, AggregateData &aggregate_data,
                           AggregationPeriodBucket & /*bucket*/) override;

  void MergeAggregateData(AggregateData &merged_aggregate_data,
                          const AggregateData &aggregate_data) override;

  lib::statusor::StatusOr<std::unique_ptr<Observation>> GenerateHourlyObservation(
      const AggregateDataToGenerate &bucket) override;

  [[nodiscard]] std::string DebugString() const override;

 private:
  std::unique_ptr<config::IntegerBucketConfig> integer_buckets_;
};

}  // namespace cobalt::local_aggregation

#endif  // COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_INTEGER_HISTOGRAM_AGGREGATION_PROCEDURE_H_
