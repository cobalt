// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_STRING_HISTOGRAM_AGGREGATION_PROCEDURE_H_
#define COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_STRING_HISTOGRAM_AGGREGATION_PROCEDURE_H_

#include "src/local_aggregation/aggregation_procedures/hourly_aggregation_procedure.h"
#include "src/public/lib/statusor/statusor.h"
#include "src/registry/report_definition.pb.h"

namespace cobalt::local_aggregation {

class StringHistogramAggregationProcedure : public HourlyAggregationProcedure {
 public:
  explicit StringHistogramAggregationProcedure(const MetricDefinition &metric,
                                               const ReportDefinition &report)
      : HourlyAggregationProcedure(metric, report),
        string_buffer_max_(report.string_buffer_max()) {}

  void UpdateAggregateData(const logger::EventRecord &event_record, AggregateData &aggregate_data,
                           AggregationPeriodBucket &bucket) override;

  void MergeAggregateData(AggregateData &merged_aggregate_data,
                          const AggregateData &aggregate_data) override;

  lib::statusor::StatusOr<std::unique_ptr<Observation>> GenerateHourlyObservation(
      const AggregateDataToGenerate &bucket) override;

  [[nodiscard]] std::string DebugString() const override;

 private:
  uint32_t string_buffer_max_;
};

}  // namespace cobalt::local_aggregation

#endif  // COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_STRING_HISTOGRAM_AGGREGATION_PROCEDURE_H_
