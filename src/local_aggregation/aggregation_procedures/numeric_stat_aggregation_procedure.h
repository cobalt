// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_NUMERIC_STAT_AGGREGATION_PROCEDURE_H_
#define COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_NUMERIC_STAT_AGGREGATION_PROCEDURE_H_

#include <memory>
#include <set>
#include <vector>

#include "src/lib/util/not_null.h"
#include "src/local_aggregation/aggregation_procedures/aggregation_procedure.h"
#include "src/logger/event_record.h"
#include "src/public/lib/statusor/statusor.h"
#include "src/registry/metric_definition.pb.h"
#include "src/registry/report_definition.pb.h"

namespace cobalt::local_aggregation {

class NumericStatAggregationProcedure : public AggregationProcedure {
 public:
  static lib::statusor::StatusOr<util::NotNullUniquePtr<NumericStatAggregationProcedure>> New(
      const std::string &customer_name, const std::string &project_name,
      const MetricDefinition &metric, const ReportDefinition &report);

  explicit NumericStatAggregationProcedure(const MetricDefinition &metric,
                                           const ReportDefinition &report);

  [[nodiscard]] bool IsDaily() const override;

  lib::statusor::StatusOr<std::unique_ptr<Observation>> GenerateSingleObservation(
      const std::vector<AggregateDataToGenerate> &buckets,
      const std::set<std::vector<uint32_t>> &event_vectors,
      const util::TimeInfo & /*time_info*/) override;

 protected:
  virtual int64_t CollectValue(
      const std::vector<std::reference_wrapper<const AggregateData>> &aggregates) = 0;
};

class SumNumericStatAggregationProcedure : public NumericStatAggregationProcedure {
 public:
  explicit SumNumericStatAggregationProcedure(const MetricDefinition &metric,
                                              const ReportDefinition &report)
      : NumericStatAggregationProcedure(metric, report) {}

  void UpdateAggregateData(const logger::EventRecord &event_record, AggregateData &aggregate_data,
                           AggregationPeriodBucket & /*bucket*/) override;

  void MergeAggregateData(AggregateData &merged_aggregate_data,
                          const AggregateData &aggregate_data) override;

  [[nodiscard]] std::string DebugString() const override { return "SUM_NUMERIC_STAT"; }

 private:
  int64_t CollectValue(
      const std::vector<std::reference_wrapper<const AggregateData>> &aggregates) override;
};

class MinNumericStatAggregationProcedure : public NumericStatAggregationProcedure {
 public:
  explicit MinNumericStatAggregationProcedure(const MetricDefinition &metric,
                                              const ReportDefinition &report)
      : NumericStatAggregationProcedure(metric, report) {}

  void UpdateAggregateData(const logger::EventRecord &event_record, AggregateData &aggregate_data,
                           AggregationPeriodBucket & /*bucket*/) override;

  void MergeAggregateData(AggregateData &merged_aggregate_data,
                          const AggregateData &aggregate_data) override;

  [[nodiscard]] std::string DebugString() const override { return "MIN_NUMERIC_STAT"; }

 private:
  int64_t CollectValue(
      const std::vector<std::reference_wrapper<const AggregateData>> &aggregates) override;
};

class MaxNumericStatAggregationProcedure : public NumericStatAggregationProcedure {
 public:
  explicit MaxNumericStatAggregationProcedure(const MetricDefinition &metric,
                                              const ReportDefinition &report)
      : NumericStatAggregationProcedure(metric, report) {}

  void UpdateAggregateData(const logger::EventRecord &event_record, AggregateData &aggregate_data,
                           AggregationPeriodBucket & /*bucket*/) override;

  void MergeAggregateData(AggregateData &merged_aggregate_data,
                          const AggregateData &aggregate_data) override;

  [[nodiscard]] std::string DebugString() const override { return "MAX_NUMERIC_STAT"; }

 private:
  int64_t CollectValue(
      const std::vector<std::reference_wrapper<const AggregateData>> &aggregates) override;
};

class MeanNumericStatAggregationProcedure : public NumericStatAggregationProcedure {
 public:
  explicit MeanNumericStatAggregationProcedure(const MetricDefinition &metric,
                                               const ReportDefinition &report)
      : NumericStatAggregationProcedure(metric, report) {}

  void UpdateAggregateData(const logger::EventRecord &event_record, AggregateData &aggregate_data,
                           AggregationPeriodBucket & /*bucket*/) override;

  void MergeAggregateData(AggregateData &merged_aggregate_data,
                          const AggregateData &aggregate_data) override;

  [[nodiscard]] std::string DebugString() const override { return "MEAN_NUMERIC_STAT"; }

 private:
  int64_t CollectValue(
      const std::vector<std::reference_wrapper<const AggregateData>> &aggregates) override;
};

class MedianNumericStatAggregationProcedure : public NumericStatAggregationProcedure {
 public:
  explicit MedianNumericStatAggregationProcedure(const MetricDefinition &metric,
                                                 const ReportDefinition &report)
      : NumericStatAggregationProcedure(metric, report) {}

  void UpdateAggregateData(const logger::EventRecord &event_record, AggregateData &aggregate_data,
                           AggregationPeriodBucket & /*bucket*/) override;

  void MergeAggregateData(AggregateData &merged_aggregate_data,
                          const AggregateData &aggregate_data) override;

  [[nodiscard]] std::string DebugString() const override { return "MEDIAN_NUMERIC_STAT"; }

 private:
  int64_t CollectValue(
      const std::vector<std::reference_wrapper<const AggregateData>> &aggregates) override;
};

class PercentileNNumericStatAggregationProcedure : public MedianNumericStatAggregationProcedure {
 public:
  explicit PercentileNNumericStatAggregationProcedure(const MetricDefinition &metric,
                                                      const ReportDefinition &report)
      : MedianNumericStatAggregationProcedure(metric, report),
        percentile_n_(report.local_aggregation_procedure_percentile_n()) {}

  [[nodiscard]] std::string DebugString() const override { return "PERCENTILE_N_NUMERIC_STAT"; }

 private:
  int64_t CollectValue(
      const std::vector<std::reference_wrapper<const AggregateData>> &aggregates) override;

  uint32_t percentile_n_;
};

}  // namespace cobalt::local_aggregation

#endif  // COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_NUMERIC_STAT_AGGREGATION_PROCEDURE_H_
