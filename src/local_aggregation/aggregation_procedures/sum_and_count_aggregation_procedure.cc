// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/local_aggregation/aggregation_procedures/sum_and_count_aggregation_procedure.h"

#include <tuple>

#include "src/local_aggregation/aggregation_procedures/aggregation_procedure.h"
#include "src/logger/encoder.h"
#include "src/pb/observation.pb.h"

namespace cobalt::local_aggregation {

void SumAndCountAggregationProcedure::UpdateAggregateData(const logger::EventRecord &event_record,
                                                          AggregateData &aggregate_data,
                                                          AggregationPeriodBucket & /*bucket*/) {
  SumAndCount *sum_and_count = aggregate_data.mutable_sum_and_count();
  sum_and_count->set_count(sum_and_count->count() + 1);
  sum_and_count->set_sum(sum_and_count->sum() + event_record.event()->integer_event().value());
}

void SumAndCountAggregationProcedure::MergeAggregateData(AggregateData &merged_aggregate_data,
                                                         const AggregateData &aggregate_data) {
  SumAndCount *sum_and_count = merged_aggregate_data.mutable_sum_and_count();
  sum_and_count->set_sum(sum_and_count->sum() + aggregate_data.sum_and_count().sum());
  sum_and_count->set_count(sum_and_count->count() + aggregate_data.sum_and_count().count());
}

lib::statusor::StatusOr<std::unique_ptr<Observation>>
SumAndCountAggregationProcedure::GenerateHourlyObservation(const AggregateDataToGenerate &bucket) {
  std::vector<std::tuple<std::vector<uint32_t>, int64_t, uint32_t>> data;
  data.reserve(bucket.aggregate_data.size());

  for (const EventCodesAggregateData &aggregate_data : bucket.aggregate_data) {
    std::vector<uint32_t> event_codes(aggregate_data.event_codes().begin(),
                                      aggregate_data.event_codes().end());
    data.emplace_back(event_codes, aggregate_data.data().sum_and_count().sum(),
                      aggregate_data.data().sum_and_count().count());
  }

  if (data.empty()) {
    return {nullptr};
  }
  return logger::encoder::EncodeSumAndCountObservation(data);
}

std::string SumAndCountAggregationProcedure::DebugString() const { return "SUM_AND_COUNT"; }

}  // namespace cobalt::local_aggregation
