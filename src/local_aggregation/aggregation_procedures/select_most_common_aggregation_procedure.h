// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_SELECT_MOST_COMMON_AGGREGATION_PROCEDURE_H_
#define COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_SELECT_MOST_COMMON_AGGREGATION_PROCEDURE_H_

#include <set>
#include <vector>

#include "src/local_aggregation/aggregation_procedures/aggregation_procedure.h"
#include "src/public/lib/statusor/statusor.h"

namespace cobalt::local_aggregation {

class SelectMostCommonAggregationProcedure : public AggregationProcedure {
 public:
  explicit SelectMostCommonAggregationProcedure(const MetricDefinition &metric,
                                                const ReportDefinition &report);

  void UpdateAggregateData(const logger::EventRecord &event_record, AggregateData &aggregate_data,
                           AggregationPeriodBucket & /*bucket*/) override;

  void MergeAggregateData(AggregateData &merged_aggregate_data,
                          const AggregateData &aggregate_data) override;

  [[nodiscard]] std::string DebugString() const override;
  [[nodiscard]] bool IsDaily() const override { return true; }

  lib::statusor::StatusOr<std::unique_ptr<Observation>> GenerateSingleObservation(
      const std::vector<AggregateDataToGenerate> &buckets,
      const std::set<std::vector<uint32_t>> &event_vectors,
      const util::TimeInfo & /*time_info*/) override;
};

}  // namespace cobalt::local_aggregation

#endif  // COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_SELECT_MOST_COMMON_AGGREGATION_PROCEDURE_H_
