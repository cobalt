// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_SUM_AND_COUNT_AGGREGATION_PROCEDURE_H_
#define COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_SUM_AND_COUNT_AGGREGATION_PROCEDURE_H_

#include "src/local_aggregation/aggregation_procedures/hourly_aggregation_procedure.h"
#include "src/public/lib/statusor/statusor.h"
#include "src/registry/metric_definition.pb.h"
#include "src/registry/report_definition.pb.h"

namespace cobalt::local_aggregation {

class SumAndCountAggregationProcedure : public HourlyAggregationProcedure {
 public:
  SumAndCountAggregationProcedure(const MetricDefinition &metric, const ReportDefinition &report)
      : HourlyAggregationProcedure(metric, report) {}

  void UpdateAggregateData(const logger::EventRecord &event_record, AggregateData &aggregate_data,
                           AggregationPeriodBucket &bucket) override;

  void MergeAggregateData(AggregateData &merged_aggregate_data,
                          const AggregateData &aggregate_data) override;

  lib::statusor::StatusOr<std::unique_ptr<Observation>> GenerateHourlyObservation(
      const AggregateDataToGenerate &bucket) override;

  [[nodiscard]] std::string DebugString() const override;
};

}  // namespace cobalt::local_aggregation

#endif  // COBALT_SRC_LOCAL_AGGREGATION_AGGREGATION_PROCEDURES_SUM_AND_COUNT_AGGREGATION_PROCEDURE_H_
