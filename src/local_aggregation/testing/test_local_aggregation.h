
// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOCAL_AGGREGATION_TESTING_TEST_LOCAL_AGGREGATION_H_
#define COBALT_SRC_LOCAL_AGGREGATION_TESTING_TEST_LOCAL_AGGREGATION_H_

#include "src/local_aggregation/local_aggregation.h"

namespace cobalt::local_aggregation {

// LocalAggregation is the top-level object responsible for scheduling and managing the current
// aggregated observations.
class TestLocalAggregation : public LocalAggregation {
 public:
  TestLocalAggregation(const CobaltConfig &cfg,
                       const logger::ProjectContextFactory &global_project_context_factory,
                       system_data::SystemDataInterface &system_data, util::FileSystem &fs,
                       const logger::ObservationWriter &observation_writer,
                       util::CivilTimeConverterInterface &civil_time_converter,
                       logger::InternalMetrics *internal_metrics = nullptr)
      : LocalAggregation(cfg, global_project_context_factory, system_data, fs, observation_writer,
                         civil_time_converter, internal_metrics) {}

  Status AddEvent(const logger::EventRecord &event_record,
                  const std::chrono::system_clock::time_point &event_timestamp) override {
    last_timestamp_ = event_timestamp;
    timestamps_.push_back(event_timestamp);
    metric_ids_.push_back(event_record.metric()->id());
    events_added_ += 1;
    return LocalAggregation::AddEvent(event_record, event_timestamp);
  }

  int32_t num_events_added() const { return events_added_; }
  std::chrono::system_clock::time_point last_timestamp() const { return last_timestamp_; }
  std::chrono::system_clock::time_point timestamp(size_t index) const { return timestamps_[index]; }
  uint32_t metric_id(size_t index) const { return metric_ids_[index]; }

 private:
  std::chrono::system_clock::time_point last_timestamp_;
  int32_t events_added_ = 0;
  std::vector<uint32_t> metric_ids_;
  std::vector<std::chrono::system_clock::time_point> timestamps_;
};

}  // namespace cobalt::local_aggregation

#endif  // COBALT_SRC_LOCAL_AGGREGATION_TESTING_TEST_LOCAL_AGGREGATION_H_
