// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_LOCAL_AGGREGATION_BACKFILL_MANAGER_H_
#define COBALT_SRC_LOCAL_AGGREGATION_BACKFILL_MANAGER_H_

#include "src/local_aggregation/civil_time_manager.h"
#include "src/public/lib/statusor/statusor.h"
#include "src/registry/metric_definition.pb.h"

namespace cobalt::local_aggregation {

// BackfillManager is responsible for computing the list of aggregation periods for which no
// observations were generated for a particular report.
class BackfillManager {
 public:
  // |total_backfill_window| The maximum number of past days (with respect to UTC) to generate
  // daily/hourly observations for. In addition, the BackfillManager may return TimeInfos for
  // previous hours of the current day for hourly reports, and will return a TimeInfo for the
  // current day for expedited daily reports.
  explicit BackfillManager(uint32_t total_backfill_window);

  // CalculateBackfill returns a list of TimeInfos corresponding to aggregation periods for which
  // observations have not yet been generated. The returned list of TimeInfos will be sorted from
  // oldest to newest.
  //
  // |last_time_info|        The last `TimeInfo` for which an observation has been generated.
  // |civil_time_mgr|        Converts the end time of the observation generation interval, as well
  //                         as earlier hourly time points, into civil times for a given time zone.
  // |metric|                The metric definition associated with the observations.
  // |is_daily|              If true, we should generate daily TimeInfos, otherwise generate hourly.
  // |is_expedited|          If true, generate a TimeInfo for the day of |end_time|.
  [[nodiscard]] lib::statusor::StatusOr<std::vector<util::TimeInfo>> CalculateBackfill(
      util::TimeInfo last_time_info, CivilTimeManager* civil_time_mgr,
      const MetricDefinition& metric, bool is_daily, bool is_expedited) const;

 private:
  // The number of days in the past for which we should generate observations.
  uint32_t backstop_days_;
};

}  // namespace cobalt::local_aggregation

#endif  // COBALT_SRC_LOCAL_AGGREGATION_BACKFILL_MANAGER_H_
