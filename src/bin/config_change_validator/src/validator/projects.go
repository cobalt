// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package validator

import (
	"config"
	"fmt"
)

func CompareProjects(oldCfg, newCfg *config.ProjectConfig, ignoreCompatChecks bool) error {
	newMetrics := map[uint32]*config.MetricDefinition{}
	for _, metric := range newCfg.Metrics {
		newMetrics[metric.Id] = metric
	}

	oldMetrics := map[uint32]*config.MetricDefinition{}
	newlyDeletedMetricIds := map[uint32]string{}
	for _, metric := range oldCfg.Metrics {
		oldMetrics[metric.Id] = metric
		_, ok := newMetrics[metric.Id]
		if !ok {
			newlyDeletedMetricIds[metric.Id] = metric.MetricName
		}
	}

	// Validation for all metrics.
	for oldMetricId, oldMetric := range oldMetrics {
		newMetric, ok := newMetrics[oldMetricId]
		if ok {
			err := CompareMetrics(oldMetric, newMetric, ignoreCompatChecks)
			if err != nil {
				return fmt.Errorf("for metric named '%s': %v", oldMetric.MetricName, err)
			}
		}
	}

	for _, newMetric := range newMetrics {
		if newMetric.MetaData.AlsoLogLocally {
			return fmt.Errorf("The parameter also_log_locally is intended only for use on a development machine. This change may not be committed to the central Cobalt metrics registry. Metric `%s.", newMetric.MetricName)
		}
	}

	for newlyDeletedMetricId, newlyDeletedMetricName := range newlyDeletedMetricIds {
		found := false
		for _, deletedMetricId := range newCfg.DeletedMetricIds {
			if deletedMetricId == newlyDeletedMetricId {
				found = true
				break
			}
		}
		if !found && !ignoreCompatChecks {
			return fmt.Errorf("for metric named '%s': the deleted metric's ID %v must be added to the deleted_metrics for the project", newlyDeletedMetricName, newlyDeletedMetricId)
		}
	}

	return nil
}
