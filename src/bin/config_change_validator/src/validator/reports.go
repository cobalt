// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package validator

import (
	"config"
	"fmt"
)

func CompareReports(oldReport, newReport *config.ReportDefinition, ignoreCompatChecks bool) error {
	if oldReport.SystemProfileSelection == config.SystemProfileSelectionPolicy_REPORT_ALL {
		if newReport.SystemProfileSelection == config.SystemProfileSelectionPolicy_SELECT_LAST || newReport.SystemProfileSelection == config.SystemProfileSelectionPolicy_SELECT_FIRST {
			if !ignoreCompatChecks {
				return fmt.Errorf("Switching from REPORT_ALL to SELECT_* is not well supported.")
			}
		}
	}

	return nil
}
