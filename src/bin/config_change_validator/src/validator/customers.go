// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package validator

import (
	"config"
	"fmt"
)

func CompareCustomers(oldCfg, newCfg *config.CustomerConfig, ignoreCompatChecks bool) error {
	newProjects := map[string]*config.ProjectConfig{}
	newProjectIds := map[uint32]bool{}
	for _, proj := range newCfg.Projects {
		newProjects[proj.ProjectName] = proj
		newProjectIds[proj.ProjectId] = true
	}

	oldProjects := map[string]*config.ProjectConfig{}
	newlyDeletedProjectIds := map[uint32]string{}
	for _, proj := range oldCfg.Projects {
		oldProjects[proj.ProjectName] = proj
		_, ok := newProjectIds[proj.ProjectId]
		if !ok {
			newlyDeletedProjectIds[proj.ProjectId] = proj.ProjectName
		}
	}

	for name, oldProj := range oldProjects {
		newProj, ok := newProjects[name]
		if ok {
			err := CompareProjects(oldProj, newProj, ignoreCompatChecks)
			if err != nil {
				return fmt.Errorf("for project named '%s': %v", name, err)
			}
		}
	}

	for newlyDeletedProjectId, newlyDeletedProjectName := range newlyDeletedProjectIds {
		found := false
		for _, deletedProjectId := range newCfg.DeletedProjectIds {
			if deletedProjectId == newlyDeletedProjectId {
				found = true
				break
			}
		}
		if !found && !ignoreCompatChecks {
			return fmt.Errorf("for project named '%s': the deleted project's ID %v must be added to the deleted_projects for the customer", newlyDeletedProjectName, newlyDeletedProjectId)
		}
	}

	return nil
}
