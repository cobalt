// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package validator

import (
	"config"
	"testing"
)

func TestDeletedProjectIds(t *testing.T) {
	oldCfg := &config.CustomerConfig{
		Projects: []*config.ProjectConfig{
			{
				ProjectId: 123,
			},
		},
	}
	newCfg := &config.CustomerConfig{}

	err := CompareCustomers(oldCfg, newCfg, false)
	if err == nil {
		t.Errorf("expected invalid change, but was accepted")
	}

	if err = CompareCustomers(oldCfg, newCfg, true); err != nil {
		t.Errorf("rejected change when ignoreCompatChecks=true, got %v", err)
	}

	newCfg.DeletedProjectIds = append(newCfg.DeletedProjectIds, 123)
	if err = CompareCustomers(oldCfg, newCfg, false); err != nil {
		t.Errorf("rejected change when deleted project ID was properly recorded, got %v", err)
	}
}
