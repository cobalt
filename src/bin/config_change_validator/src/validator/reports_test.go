// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package validator

import (
	"config"
	"testing"
)

var reportstests = []struct {
	name    string
	o       *config.ReportDefinition
	n       *config.ReportDefinition
	isValid bool
}{
	{
		"CanChangeSystemProfileSelectionToReportAll",
		&config.ReportDefinition{
			SystemProfileSelection: config.SystemProfileSelectionPolicy_SELECT_LAST,
		},
		&config.ReportDefinition{
			SystemProfileSelection: config.SystemProfileSelectionPolicy_REPORT_ALL,
		}, true,
	},
	{
		"CannotChangeSystemProfileSelectionFromReportAll",
		&config.ReportDefinition{
			SystemProfileSelection: config.SystemProfileSelectionPolicy_REPORT_ALL,
		},
		&config.ReportDefinition{
			SystemProfileSelection: config.SystemProfileSelectionPolicy_SELECT_LAST,
		}, false,
	},
}

func TestCompareReports(t *testing.T) {
	for _, tt := range reportstests {
		t.Run(tt.name, func(t *testing.T) {
			err := CompareReports(tt.o, tt.n, false)
			if err != nil && tt.isValid {
				t.Errorf("expected valid change, got %v", err)
			}
			if err == nil && !tt.isValid {
				t.Errorf("expected invalid change, but was accepted")
			}

			if err = CompareReports(tt.o, tt.n, true); err != nil {
				t.Errorf("rejected change when ignoreCompatChecks=true, got %v", err)
			}
		})
	}
}
