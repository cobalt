// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package validator

import (
	"config"
	"testing"
)

func TestAlsoLogLocally(t *testing.T) {
	oldCfg := &config.ProjectConfig{}
	invalidCfg := &config.ProjectConfig{
		Metrics: []*config.MetricDefinition{
			{
				MetaData: &config.MetricDefinition_Metadata{
					MaxReleaseStage: config.ReleaseStage_GA,
					AlsoLogLocally:  true,
				},
			},
		},
	}

	err := CompareProjects(oldCfg, invalidCfg, false)
	if err == nil {
		t.Errorf("expected invalid change, but was accepted")
	}

	if err = CompareProjects(oldCfg, invalidCfg, true); err == nil {
		t.Errorf("ignoreCompatChecks should not allow also_log_locally")
	}

	invalidCfg.Metrics[0].MetaData.MaxReleaseStage = config.ReleaseStage_DEBUG
	if err = CompareProjects(oldCfg, invalidCfg, false); err == nil {
		t.Errorf("even DEBUG metrics cannot have also_log_locally set")
	}
}

func TestDeletedMetricIds(t *testing.T) {
	oldCfg := &config.ProjectConfig{
		Metrics: []*config.MetricDefinition{
			{
				Id: 123,
				MetaData: &config.MetricDefinition_Metadata{
					MaxReleaseStage: config.ReleaseStage_GA,
				},
			},
		},
	}
	newCfg := &config.ProjectConfig{}

	err := CompareProjects(oldCfg, newCfg, false)
	if err == nil {
		t.Errorf("expected invalid change, but was accepted")
	}

	if err = CompareProjects(oldCfg, newCfg, true); err != nil {
		t.Errorf("rejected change when ignoreCompatChecks=true, got %v", err)
	}

	newCfg.DeletedMetricIds = append(newCfg.DeletedMetricIds, 123)
	if err = CompareProjects(oldCfg, newCfg, false); err != nil {
		t.Errorf("rejected change when deleted metric IDs were added, got %v", err)
	}
}

func TestMovedMetricsWithDeletedReports(t *testing.T) {
	oldCfg := &config.ProjectConfig{
		Metrics: []*config.MetricDefinition{
			{
				Id:         1,
				MetricName: "metric_a",
				CustomerId: 1,
				ProjectId:  2,
				MetricType: config.MetricDefinition_INTEGER,
				MetaData: &config.MetricDefinition_Metadata{
					MaxReleaseStage: config.ReleaseStage_GA,
				},
				Reports: []*config.ReportDefinition{
					{
						Id:         1,
						ReportName: "report_1",
					},
					{
						Id:         2,
						ReportName: "report_2",
					},
				},
			},
		},
	}
	newCfg := &config.ProjectConfig{
		Metrics: []*config.MetricDefinition{
			{
				Id:         2,
				MetricName: "metric_a",
				CustomerId: 1,
				ProjectId:  2,
				MetricType: config.MetricDefinition_INTEGER,
				MetaData: &config.MetricDefinition_Metadata{
					MaxReleaseStage: config.ReleaseStage_GA,
				},
				Reports: []*config.ReportDefinition{
					{
						Id:         1,
						ReportName: "report_1",
					},
				},
			},
		},
		DeletedMetricIds: []uint32{1},
	}

	err := CompareProjects(oldCfg, newCfg, false)
	if err != nil {
		t.Errorf("rejected valid change, got %v", err)
	}

	if err = CompareProjects(oldCfg, newCfg, true); err != nil {
		t.Errorf("rejected valid change when ignoreCompatChecks=true, got %v", err)
	}
}
