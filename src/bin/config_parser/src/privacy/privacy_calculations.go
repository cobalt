// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// Calculations needed for privacy enabled reports.

package privacy

import (
	"config"
	"fmt"
)

// Returns the maximum number of event vectors for which a device stores local aggregates.
// This is either the event_vector_buffer_max value specified in the ReportDefinition, or
// (if that field is unset) the total number of valid event vectors for the parent metric.
func GetEventVectorBufferMax(metric *config.MetricDefinition, report *config.ReportDefinition) (bufferMax uint64) {
	if report.EventVectorBufferMax != 0 {
		return report.EventVectorBufferMax
	}
	return GetNumEventVectors(metric)
}

// Returns the total number of valid event vectors for a MetricDefinition.
func GetNumEventVectors(metric *config.MetricDefinition) (numEventVectors uint64) {
	numEventVectors = 1
	for _, dim := range metric.MetricDimensions {
		numEventVectors *= uint64(numEventCodes(dim))
	}
	return numEventVectors
}

// A helper function returning the number of valid event codes for a MetricDimension.
func numEventCodes(dim *config.MetricDefinition_MetricDimension) (numEventCodes uint32) {
	if dim.MaxEventCode != 0 {
		return dim.MaxEventCode + 1
	}
	return uint32(len(dim.EventCodes))
}

// Returns the number of histogram buckets in an IntegerBuckets.
func GetNumHistogramBuckets(buckets *config.IntegerBuckets) (numBuckets uint64, err error) {
	switch buckets.Buckets.(type) {
	case *config.IntegerBuckets_Exponential:
		numBuckets, err = uint64(buckets.GetExponential().GetNumBuckets()), nil
	case *config.IntegerBuckets_Linear:
		numBuckets, err = uint64(buckets.GetLinear().GetNumBuckets()), nil
	case nil:
		err = fmt.Errorf("IntegerBuckets type not set")
	default:
		err = fmt.Errorf("unexpected IntegerBuckets type")
	}
	return numBuckets, err
}

// Returns the dimensions of a CountMin sketch for a report of type StringCounts, or an error if
// the report is of a different type. Currently these dimensions are hard-coded.
func GetCountMinSketchDimensionsForReport(report *config.ReportDefinition) (numCellsPerHash int32, numHashes int32, err error) {
	switch report.ReportType {
	case config.ReportDefinition_STRING_COUNTS,
		config.ReportDefinition_UNIQUE_DEVICE_STRING_COUNTS:
		{
			numCellsPerHash = 10
			numHashes = 5
			err = nil
		}
	default:
		err = fmt.Errorf("expected report of type StringCounts or UniqueDeviceStringCounts, found %v",
			report.ReportType)
	}
	return numCellsPerHash, numHashes, err
}

// Returns the total number of possible private index values for a report.
func GetNumPrivateIndices(metric *config.MetricDefinition, report *config.ReportDefinition) (numPrivateIndices uint64, err error) {
	numPrivateIndices = 0
	err = nil
	switch report.GetReportType() {
	case config.ReportDefinition_UNIQUE_DEVICE_COUNTS:
		numPrivateIndices = GetNumEventVectors(metric)
	case config.ReportDefinition_FLEETWIDE_OCCURRENCE_COUNTS,
		config.ReportDefinition_HOURLY_VALUE_NUMERIC_STATS,
		config.ReportDefinition_UNIQUE_DEVICE_NUMERIC_STATS:
		numPrivateIndices = GetNumEventVectors(metric) * uint64(report.GetNumIndexPoints())
	case config.ReportDefinition_FLEETWIDE_MEANS:
		numPrivateIndices = 2 * GetNumEventVectors(metric) * uint64(report.GetNumIndexPoints())
	case config.ReportDefinition_HOURLY_VALUE_HISTOGRAMS,
		config.ReportDefinition_UNIQUE_DEVICE_HISTOGRAMS:
		var numBuckets uint64
		numBuckets, err = GetNumHistogramBuckets(report.GetIntBuckets())
		numPrivateIndices = GetNumEventVectors(metric) * numBuckets
	case config.ReportDefinition_FLEETWIDE_HISTOGRAMS:
		var numBuckets uint64
		switch metric.GetMetricType() {
		case config.MetricDefinition_INTEGER:
			numBuckets, err = GetNumHistogramBuckets(report.GetIntBuckets())
			numPrivateIndices = GetNumEventVectors(metric) * numBuckets * uint64(report.GetNumIndexPoints())
		case config.MetricDefinition_INTEGER_HISTOGRAM:
			numBuckets, err = GetNumHistogramBuckets(metric.GetIntBuckets())
			numPrivateIndices = GetNumEventVectors(metric) * numBuckets * uint64(report.GetNumIndexPoints())
		}
	case config.ReportDefinition_STRING_COUNTS,
		config.ReportDefinition_UNIQUE_DEVICE_STRING_COUNTS:
		var numCellsPerHash, numHashes int32
		numCellsPerHash, numHashes, err = GetCountMinSketchDimensionsForReport(report)
		numPrivateIndices = uint64(numCellsPerHash) * uint64(numHashes) * GetNumEventVectors(metric)
	default:
		err = fmt.Errorf("unexpected report type %v", report.ReportType)
	}
	return numPrivateIndices, err
}
