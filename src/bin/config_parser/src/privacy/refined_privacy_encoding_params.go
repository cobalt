// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package privacy

import (
	"fmt"
	"math"
	"sort"
)

// getPmfBinomialDistribution computes a PMF of a Binomial distribution with a truncation of a tail mass
// We compute the probability of each outcome using the recurrent formula for computing probability of each outcome k as follows:
// Bin(n,k) := Bin(n,k-1) * p * (n+1-k) / k(1-p)
// Bin(n,0) := (1-p)^n
func getPmfBinomialDistribution(n uint, p float64, truncatedTailMass float64) (ProbabilityMassFunction, error) {
	if p > 1 || p < 0 {
		return nil, fmt.Errorf("Invalid value of input parameters for Binomial distribution. Expected number of trials to be non-negative, and probability within [0,1] range, got: n=%d and p=%f", n, p)
	}
	distr := make([]float64, n+1)
	pmf := make(ProbabilityMassFunction)
	distr[0] = math.Pow(1-p, float64(n))
	for i := 1; i <= int(n); i++ {
		distr[i] = distr[i-1] * p * (float64(n) + float64(1-i)) / (float64(i) * (1 - p))
	}

	minBound, maxBound := computeTruncationValueBounds(distr, truncatedTailMass)
	for i := range distr {
		if i >= minBound && i <= maxBound {
			pmf[i] = distr[i]
		}
	}

	return pmf, nil
}

// getPmfShiftedBinomialDistribution computes a PMF of a distribution D_{n,1,p}=Bin(1,1-p) +* Bin(n-1,p), where +* denotes a convolution
// We compute this convolution directly without extra calls to Convolution computation using FFT, as it is simple and more efficient: linear time instead of O(nlogn) of FFT.
func getPmfShiftedBinomialDistribution(n uint, p float64, truncatedTailMass float64) (ProbabilityMassFunction, error) {
	if n < 1 || p > 1 || p < 0 {
		return nil, fmt.Errorf("Invalid value of input parameters for Shifted Binomial distribution. Expected number of trials to be positive (at least 1), and probability within [0,1] range, got: n=%d and p=%f", n, p)
	}
	pmfBinomial, err := getPmfBinomialDistribution(n-1, p, 0)
	if err != nil {
		return nil, err
	}
	shiftedDistr := make([]float64, n+1)
	shiftedDistr[0] = pmfBinomial[0] * p
	for i := 1; i <= int(n); i++ {
		shiftedDistr[i] = (1 - p) * pmfBinomial[i-1]
		shiftedDistr[i] += p * pmfBinomial[i]
	}

	pmfShiftedDistr := make(ProbabilityMassFunction)
	minBound, maxBound := computeTruncationValueBounds(shiftedDistr, truncatedTailMass)
	for i := range shiftedDistr {
		if i >= minBound && i <= maxBound {
			pmfShiftedDistr[i] = shiftedDistr[i]
		}
	}

	return pmfShiftedDistr, nil
}

// getPrivacyLossDistribution computes a privacy loss distribution of the distribution
// that gives worst-case privacy loss for Cobalt's fragmented RAPPOR encoding scheme.
// First, we generate two Binomial distributions, throwing away all values that have mass < truncatedMass/2*sparsity.
// Then we compute two PLDs that correspond to PLD between these two Bin distributions.
// Then we compute convolution of a cross product of these PLDs, getting a PLD of a cross-product distribution.
// And finally, we compute k-times-cross-product-convolution of this PLD, getting a PLD of the distribution from Lemma 5.
func getPrivacyLossDistribution(population uint, sparsity uint64, p float64, truncatedMass float64, discretization float64) (*PrivacyLossDistribution, error) {
	// Compute a mass we will truncated from each of the distribution we are composing.
	// Overall we compose 4 * sparsity one-dimensional distributions.
	truncatedTailMass := truncatedMass / (2 * float64(sparsity))

	pmfBinomial, err := getPmfBinomialDistribution(population, p, truncatedTailMass)
	if err != nil {
		return nil, err
	}

	pmfShiftedBinomial, err := getPmfShiftedBinomialDistribution(population, p, truncatedTailMass)
	if err != nil {
		return nil, err
	}

	loggedTruncatedPldMass := -50.0
	// Here we are switching from PMF of a simple distribution to PMF of PLD.
	// Note that here we truncate all values with very small probabilities, and this value is independent
	// from the original truncation bound.
	pldFirst := generatePLD(pmfBinomial, pmfShiftedBinomial, loggedTruncatedPldMass, discretization, true)

	pldSecond := generatePLD(pmfShiftedBinomial, pmfBinomial, loggedTruncatedPldMass, discretization, true)

	compositionTailTruncation := 1e-15
	pldOfCompositionOfPair, err := composeHeterogeneousPLD(pldFirst, pldSecond, compositionTailTruncation)
	if err != nil {
		return nil, err
	}

	pldFinal, err := composeHomogeneousPLD(pldOfCompositionOfPair, sparsity, compositionTailTruncation)
	if err != nil {
		return nil, err
	}

	return pldFinal, nil
}

// checkPrivacy checks whether the fragmented RAPPOR encoding with bit-flip probability p
// gives (epsilon, delta)-differentially private output for a report with parameters (population, sparsity).
func checkPrivacy(epsilon float64, population uint64, sparsity uint64, delta float64, p float64, discretization float64) (bool, error) {
	// As we cannot efficiently compute exact bound for large values of population and sparsity, we compute an approximate bound.
	// To get an approximate bound we ignore and truncate from the distribution values that have very small probabilities.
	// 0.001 is some constant = fraction of probability mass that is far from the expected value
	truncatedMass := 0.001 * delta
	if discretization <= 0 {
		discretization = 0.001
	}
	// Discretization affects the accuracy of the computed PLD, as it defines in which range we assign mass of all values in the range
	// to one of the values in this range. If discretization is small, then we loose accuracy of PLD values.
	// The smaller discretization is, the bigger is a number of distinct values in the distribution,
	// hence we spend more times on various operations on such distribution.
	pld, err := getPrivacyLossDistribution(uint(population), sparsity, p, truncatedMass, discretization)
	if err != nil {
		return false, err
	}
	return pld.getDeltaForPrivacyLossDistribution(epsilon)+truncatedMass <= delta, err
}

// findOptimalBitFlipProbability computes the minimal value of bit-flip-probability
// that guarantees (epsilon,delta)-DP for an encoding with population 0-1 values with such sparsity.
// Increment defines the granularity of value p that we are searching through.
// For example, if we set increment=0.01, then we find the smallest value of p among the values 0, 0.01, 0.02, 0.03,..., 0.49, 0.5.
// Delta should be positive; otherwise, the function uses the default value of delta = 1/population.
func findOptimalBitFlipProbability(epsilon float64, delta float64, population uint64, sparsity uint64, discretization float64, increment float64) (float64, error) {
	// First we set delta=1/population
	// Then for the fixed epsilon, delta, population, and sparsity
	// we run binary search to find bitFlipProb p such that Div_epsilon(Dist1 || Dist2) <= delta
	// the check of the inequality is a separate function
	if delta < 0 {
		delta = 1 / float64(population)
	}

	// Need to find the smallest value of p between 0 and 0.5 such that checkPrivacy for the epsilon, delta,
	// sparsity, and population sizes on this p returns true. For that we use binary search on integer values from 0 to 0.5 / increment.
	maxIntValueForBinarySearch := int(math.Ceil(0.5/increment)) + 1

	// Helper function for the binary search.
	checkPrivacyForFixedParams := func(index int) bool {
		prob := float64(index) * increment
		if discretization <= 0 {
			discretization = 0.001
		}
		isPrivate, err := checkPrivacy(epsilon, population, sparsity, delta, prob, discretization)
		if err != nil {
			return false
		}
		return isPrivate
	}

	p := sort.Search(maxIntValueForBinarySearch, checkPrivacyForFixedParams)

	return float64(p) * increment, nil
}

// pyramidNumber computes the sum of squares of the integer from 0 to n, inclusive.
func pyramidNumber(n float64) float64 {
	if n < 1 {
		return 0
	}
	// the number in the numerator is always divisible by 6
	return (n * (n + 1.0) * (2*n + 1.0)) / 6
}

// rootMeanSquaredError computes RMSE of the fragmented RAPPOR privacy encoding algorithms used in Cobalt.
func rootMeanSquaredError(p float64, population uint64, discretization uint64) float64 {
	rmsePer2dBucket := math.Sqrt(float64(population)*p*(1.0-p)) / (1.0 - 2.0*p)
	return math.Sqrt(pyramidNumber(float64(discretization))) * rmsePer2dBucket
}

// realSumRMSE computes an upper bound on RMSE of the probabilistic rounding encoding algorithm.
func realSumRMSE(p float64, population uint64, discretization uint64) float64 {
	return math.Sqrt(math.Pow(rootMeanSquaredError(p, population, discretization), 2.0)+float64(population)/4.0) / float64(discretization)
}

// findOptimalDiscretization copmutes the discretization parameter
// for which error from probabilistic rounding is roughly equal to the error we get from privacy encoding algorithm.
func findOptimalDiscretization(p float64, population uint64) uint64 {
	var discretization uint64 = 1
	minDiscretization := discretization
	scaledPerBucketRMSE := rootMeanSquaredError(p, population, discretization) / float64(discretization)
	minRealSumRMSE := realSumRMSE(p, population, discretization)

	for discretization = 2; scaledPerBucketRMSE <= minRealSumRMSE; discretization++ {
		realSumRMSE := realSumRMSE(p, population, discretization)
		if realSumRMSE < minRealSumRMSE {
			minRealSumRMSE = realSumRMSE
			minDiscretization = discretization
		}
		scaledPerBucketRMSE = rootMeanSquaredError(p, population, discretization) / float64(discretization)
	}

	return minDiscretization
}

// FindPrivacyEncodingParameters computes optimal bit-flip probability and discretization for privacy encoding.
func FindPrivacyEncodingParameters(epsilon float64, population uint64, sparsity uint64) (float64, uint64, error) {
	delta := 1.0 / float64(population)
	bitFlipProbability, err := findOptimalBitFlipProbability(epsilon, delta, population, sparsity, 0.001, 0.001)
	if err != nil {
		return 0, 0, err
	}
	discretization := findOptimalDiscretization(bitFlipProbability, population)
	return bitFlipProbability, discretization, err
}
