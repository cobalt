// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package privacy

import (
	"config"
	"testing"

	"github.com/golang/glog"
)

// The try-bots expect glog to be imported, but we do not use it.
var _ = glog.Info

func TestGetEventVectorBufferMax(t *testing.T) {
	var eventVectorBufferMax uint64 = 5
	var maxEventCode uint32 = 2

	dimensionMaxEventCode := config.MetricDefinition_MetricDimension{
		Dimension:    "dim0",
		MaxEventCode: maxEventCode,
	}
	dimensionNoMaxEventCode := config.MetricDefinition_MetricDimension{
		Dimension:  "dim1",
		EventCodes: map[uint32]string{0: "event0", 1: "event1", 5: "event5"},
	}
	metricDimensions := []*config.MetricDefinition_MetricDimension{
		&dimensionMaxEventCode,
		&dimensionNoMaxEventCode,
	}
	metricWithDimensions := config.MetricDefinition{
		MetricName:       "MetricWithDimensions",
		MetricDimensions: metricDimensions,
	}

	reportMaxSet := config.ReportDefinition{
		ReportName:           "ReportWithEventVectorBufferMaxSet",
		EventVectorBufferMax: eventVectorBufferMax,
	}
	reportMaxUnset := config.ReportDefinition{
		ReportName: "ReportWithEventVectorBufferMaxUnset",
	}
	var tests = []struct {
		metric   *config.MetricDefinition
		report   *config.ReportDefinition
		expected uint64
	}{
		{&metricWithDimensions, &reportMaxSet, uint64(eventVectorBufferMax)},
		{&metricWithDimensions, &reportMaxUnset, uint64((maxEventCode + 1) * 3)},
	}
	for _, test := range tests {
		result := GetEventVectorBufferMax(test.metric, test.report)
		if result != test.expected {
			t.Errorf("eventVectorBufferMax() for metric %s and report %s: expected %d, got %d",
				test.metric.MetricName, test.report.ReportName, test.expected, result)
		}
	}

}
