// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package privacy

import (
	"config"
	"testing"

	"github.com/golang/glog"
)

// The try-bots expect glog to be imported, but we do not use it.
var _ = glog.Info

func TestGetIntegerRangeForReport(t *testing.T) {
	var minValue int64 = -5
	var maxValue int64 = 10
	var maxCount uint64 = 20
	var smallMaxCount uint64 = 5

	// Reports with MinValue and MaxValue set
	fleetwideOccurrenceCountsReport := config.ReportDefinition{
		ReportName: "FleetwideOccurrenceCounts",
		ReportType: config.ReportDefinition_FLEETWIDE_OCCURRENCE_COUNTS,
		MinValue:   minValue,
		MaxValue:   maxValue,
	}
	uniqueDeviceNumericStatsReport := config.ReportDefinition{
		ReportName: "UniqueDeviceNumericStats",
		ReportType: config.ReportDefinition_UNIQUE_DEVICE_NUMERIC_STATS,
		MinValue:   minValue,
		MaxValue:   maxValue,
	}
	hourlyValueNumericStatsReport := config.ReportDefinition{
		ReportName: "HourlyValueNumericStats",
		ReportType: config.ReportDefinition_HOURLY_VALUE_NUMERIC_STATS,
		MinValue:   minValue,
		MaxValue:   maxValue,
	}
	// Reports with MaxCount set
	fleetwideHistogramsReport := config.ReportDefinition{
		ReportName: "FleetwideHistograms",
		ReportType: config.ReportDefinition_FLEETWIDE_HISTOGRAMS,
		MaxCount:   maxCount,
	}
	stringCountsReport := config.ReportDefinition{
		ReportName: "StringCounts",
		ReportType: config.ReportDefinition_STRING_COUNTS,
		MaxCount:   maxCount,
	}
	// Reports with MinValue, MaxValue, and MaxCount set
	fleetwideMeansLargerSumRangeReport := config.ReportDefinition{
		ReportName: "FleetwideMeansLargerSumRange",
		ReportType: config.ReportDefinition_FLEETWIDE_MEANS,
		MinValue:   minValue,
		MaxValue:   maxValue,
		MaxCount:   smallMaxCount,
	}
	fleetwideMeansLargerCountRangeReport := config.ReportDefinition{
		ReportName: "FleetwideMeansLargerCountRange",
		ReportType: config.ReportDefinition_FLEETWIDE_MEANS,
		MinValue:   minValue,
		MaxValue:   maxValue,
		MaxCount:   maxCount,
	}
	// Reports without an integer range
	uniqueDeviceCountsReport := config.ReportDefinition{
		ReportName: "UniqueDeviceCounts",
		ReportType: config.ReportDefinition_UNIQUE_DEVICE_COUNTS,
	}
	uniqueDeviceHistogramsReport := config.ReportDefinition{
		ReportName: "UniqueDeviceHistograms",
		ReportType: config.ReportDefinition_UNIQUE_DEVICE_HISTOGRAMS,
	}
	hourlyValueHistogramsReport := config.ReportDefinition{
		ReportName: "HourlyValueHistograms",
		ReportType: config.ReportDefinition_HOURLY_VALUE_HISTOGRAMS,
	}
	uniqueDeviceStringCountsReport := config.ReportDefinition{
		ReportName: "UniqueDeviceStringCounts",
		ReportType: config.ReportDefinition_UNIQUE_DEVICE_STRING_COUNTS,
	}
	// Invalid report type
	unsetReportTypeReport := config.ReportDefinition{
		ReportName: "UnsetReportType",
	}
	var tests = []struct {
		report   *config.ReportDefinition
		valid    bool
		expected uint64
	}{
		// Valid input:
		{&fleetwideOccurrenceCountsReport, true, uint64(maxValue - minValue + 1)},
		{&uniqueDeviceNumericStatsReport, true, uint64(maxValue - minValue + 1)},
		{&hourlyValueNumericStatsReport, true, uint64(maxValue - minValue + 1)},
		{&fleetwideHistogramsReport, true, maxCount + 1},
		{&stringCountsReport, true, maxCount + 1},
		{&fleetwideMeansLargerSumRangeReport, true, uint64(maxValue - minValue + 1)},
		{&fleetwideMeansLargerCountRangeReport, true, maxCount + 1},
		{&uniqueDeviceCountsReport, true, 1},
		{&uniqueDeviceHistogramsReport, true, 1},
		{&hourlyValueHistogramsReport, true, 1},
		{&uniqueDeviceStringCountsReport, true, 1},
		// Invalid input:
		{&unsetReportTypeReport, false, 0},
	}
	for _, test := range tests {
		result, err := GetIntegerRangeSizeForReport(test.report)
		if test.valid && err != nil {
			t.Errorf("getIntegerRangeSizeForReport() failed for report %s: %v", test.report.ReportName, err)
		} else if !test.valid && err == nil {
			t.Errorf("getIntegerRangeSizeForReport() accepted invalid report: %s", test.report.ReportName)
		} else if test.valid && result != test.expected {
			t.Errorf("getIntegerRangeSizeForReport() for report %s: expected %d, got %d", test.report.ReportName, test.expected, result)
		}
	}
}

func TestGetSparsityForReport(t *testing.T) {
	var eventVectorBufferMax uint64 = 5
	var stringBufferMax uint32 = 3
	var numLinearBuckets uint32 = 7
	var maxEventCode uint32 = 2
	var numCellsPerHash uint64 = 10
	var numHashes uint64 = 5

	linearBuckets := config.LinearIntegerBuckets{NumBuckets: numLinearBuckets}
	buckets := config.IntegerBuckets{
		Buckets: &config.IntegerBuckets_Linear{&linearBuckets},
	}

	stringSketchParams := config.StringSketchParameters{
		NumHashes:       int32(numHashes),
		NumCellsPerHash: int32(numCellsPerHash),
	}

	dimension := config.MetricDefinition_MetricDimension{
		Dimension:    "dim0",
		MaxEventCode: maxEventCode,
	}

	// Metrics
	occurrenceMetric := config.MetricDefinition{
		MetricName:       "OccurrenceMetric",
		MetricType:       config.MetricDefinition_OCCURRENCE,
		MetricDimensions: []*config.MetricDefinition_MetricDimension{&dimension},
	}
	integerMetric := config.MetricDefinition{
		MetricName: "IntegerMetric",
		MetricType: config.MetricDefinition_INTEGER,
	}
	integerHistogramMetric := config.MetricDefinition{
		MetricName: "IntegerHistogramMetric",
		MetricType: config.MetricDefinition_INTEGER_HISTOGRAM,
		IntBuckets: &buckets,
	}
	stringMetric := config.MetricDefinition{
		MetricName: "StringMetric",
		MetricType: config.MetricDefinition_STRING,
	}

	// Reports
	fleetwideOccurrenceCountsReport := config.ReportDefinition{
		ReportName:           "FleetwideOccurrenceCounts",
		ReportType:           config.ReportDefinition_FLEETWIDE_OCCURRENCE_COUNTS,
		EventVectorBufferMax: eventVectorBufferMax,
	}
	fleetwideOccurrenceCountsReportNoBufferMax := config.ReportDefinition{
		ReportName: "FleetwideOccurrenceCountsNoBufferMax",
		ReportType: config.ReportDefinition_FLEETWIDE_OCCURRENCE_COUNTS,
	}
	atLeastOnceUniqueDeviceCountsReport := config.ReportDefinition{
		ReportName:                "AtLeastOnceUniqueDeviceCounts",
		ReportType:                config.ReportDefinition_UNIQUE_DEVICE_COUNTS,
		LocalAggregationProcedure: config.ReportDefinition_AT_LEAST_ONCE,
		EventVectorBufferMax:      eventVectorBufferMax,
	}
	selectFirstUniqueDeviceCountsReport := config.ReportDefinition{
		ReportName:                "SelectFirstUniqueDeviceCounts",
		ReportType:                config.ReportDefinition_UNIQUE_DEVICE_COUNTS,
		LocalAggregationProcedure: config.ReportDefinition_SELECT_FIRST,
		EventVectorBufferMax:      eventVectorBufferMax,
	}
	selectMostCommonUniqueDeviceCountsReport := config.ReportDefinition{
		ReportName:                "SelectMostCommonUniqueDeviceCounts",
		ReportType:                config.ReportDefinition_UNIQUE_DEVICE_COUNTS,
		LocalAggregationProcedure: config.ReportDefinition_SELECT_MOST_COMMON,
		EventVectorBufferMax:      eventVectorBufferMax,
	}
	selectMostCommonUniqueDeviceCountsReportNoBufferMax := config.ReportDefinition{
		ReportName:                "SelectMostCommonUniqueDeviceCountsNoBufferMax",
		ReportType:                config.ReportDefinition_UNIQUE_DEVICE_COUNTS,
		LocalAggregationProcedure: config.ReportDefinition_SELECT_MOST_COMMON,
	}
	uniqueDeviceNumericStatsReport := config.ReportDefinition{
		ReportName:           "UniqueDeviceNumericStats",
		ReportType:           config.ReportDefinition_UNIQUE_DEVICE_NUMERIC_STATS,
		EventVectorBufferMax: eventVectorBufferMax,
	}
	hourlyValueNumericStatsReport := config.ReportDefinition{
		ReportName:           "HourlyValueNumericStats",
		ReportType:           config.ReportDefinition_HOURLY_VALUE_NUMERIC_STATS,
		EventVectorBufferMax: eventVectorBufferMax,
	}
	uniqueDeviceHistogramsReport := config.ReportDefinition{
		ReportName:           "UniqueDeviceHistograms",
		ReportType:           config.ReportDefinition_UNIQUE_DEVICE_HISTOGRAMS,
		EventVectorBufferMax: eventVectorBufferMax,
	}
	hourlyValueHistogramsReport := config.ReportDefinition{
		ReportName:           "HourlyValueHistograms",
		ReportType:           config.ReportDefinition_HOURLY_VALUE_HISTOGRAMS,
		EventVectorBufferMax: eventVectorBufferMax,
	}
	fleetwideHistogramsForIntegerReport := config.ReportDefinition{
		ReportName:           "FleetwideHistogramsForInteger",
		ReportType:           config.ReportDefinition_FLEETWIDE_HISTOGRAMS,
		IntBuckets:           &buckets,
		EventVectorBufferMax: eventVectorBufferMax,
	}
	fleetwideMeansReport := config.ReportDefinition{
		ReportName:           "FleetwideMeans",
		ReportType:           config.ReportDefinition_FLEETWIDE_MEANS,
		EventVectorBufferMax: eventVectorBufferMax,
	}
	fleetwideHistogramsForIntegerHistogramReport := config.ReportDefinition{
		ReportName:           "FleetwideHistogramsForIntegerHistogram",
		ReportType:           config.ReportDefinition_FLEETWIDE_HISTOGRAMS,
		EventVectorBufferMax: eventVectorBufferMax,
	}
	stringCountsReport := config.ReportDefinition{
		ReportName:           "StringCounts",
		ReportType:           config.ReportDefinition_STRING_COUNTS,
		EventVectorBufferMax: eventVectorBufferMax,
		StringBufferMax:      stringBufferMax,
		StringSketchParams:   &stringSketchParams,
	}
	stringCountsReportWithNoStringBufferMax := config.ReportDefinition{
		ReportName:           "StringCountsWithNoStringBufferMax",
		ReportType:           config.ReportDefinition_STRING_COUNTS,
		EventVectorBufferMax: eventVectorBufferMax,
		StringSketchParams:   &stringSketchParams,
	}
	uniqueDeviceStringCountsReport := config.ReportDefinition{
		ReportName:           "UniqueDeviceStringCounts",
		ReportType:           config.ReportDefinition_UNIQUE_DEVICE_STRING_COUNTS,
		EventVectorBufferMax: eventVectorBufferMax,
		StringBufferMax:      stringBufferMax,
		StringSketchParams:   &stringSketchParams,
	}
	uniqueDeviceStringCountsReportWithNoStringBufferMax := config.ReportDefinition{
		ReportName:           "UniqueDeviceStringCountsWithNoStringBufferMax",
		ReportType:           config.ReportDefinition_UNIQUE_DEVICE_STRING_COUNTS,
		EventVectorBufferMax: eventVectorBufferMax,
		StringSketchParams:   &stringSketchParams,
	}
	unsetReportTypeReport := config.ReportDefinition{
		ReportName: "UnsetReportType",
	}

	type args struct {
		metric *config.MetricDefinition
		report *config.ReportDefinition
	}
	var tests = []struct {
		input    args
		valid    bool
		expected uint64
	}{
		// Valid input:
		{args{&occurrenceMetric, &fleetwideOccurrenceCountsReport}, true, eventVectorBufferMax},
		{args{&occurrenceMetric, &fleetwideOccurrenceCountsReportNoBufferMax}, true,
			uint64(maxEventCode) + 1},
		{args{&occurrenceMetric, &atLeastOnceUniqueDeviceCountsReport}, true, eventVectorBufferMax},
		{args{&occurrenceMetric, &selectFirstUniqueDeviceCountsReport}, true, 1},
		{args{&occurrenceMetric, &selectMostCommonUniqueDeviceCountsReport}, true, 1},
		{args{&occurrenceMetric, &selectMostCommonUniqueDeviceCountsReportNoBufferMax}, true, 1},
		{args{&occurrenceMetric, &uniqueDeviceHistogramsReport}, true, eventVectorBufferMax},
		{args{&occurrenceMetric, &hourlyValueHistogramsReport}, true, eventVectorBufferMax},
		{args{&occurrenceMetric, &uniqueDeviceNumericStatsReport}, true, eventVectorBufferMax},
		{args{&occurrenceMetric, &hourlyValueNumericStatsReport}, true, eventVectorBufferMax},

		{args{&integerMetric, &uniqueDeviceHistogramsReport}, true, eventVectorBufferMax},
		{args{&integerMetric, &hourlyValueHistogramsReport}, true, eventVectorBufferMax},
		{args{&integerMetric, &fleetwideHistogramsForIntegerReport}, true, eventVectorBufferMax * uint64(numLinearBuckets)},
		{args{&integerMetric, &fleetwideMeansReport}, true, eventVectorBufferMax * 2},
		{args{&integerMetric, &uniqueDeviceNumericStatsReport}, true, eventVectorBufferMax},
		{args{&integerMetric, &hourlyValueNumericStatsReport}, true, eventVectorBufferMax},

		{args{&integerHistogramMetric, &fleetwideHistogramsForIntegerHistogramReport}, true,
			eventVectorBufferMax * uint64(numLinearBuckets)},

		{args{&stringMetric, &stringCountsReportWithNoStringBufferMax}, true, eventVectorBufferMax * numCellsPerHash * numHashes},
		{args{&stringMetric, &stringCountsReport}, true, eventVectorBufferMax * uint64(stringBufferMax) * numHashes},
		{args{&stringMetric, &uniqueDeviceStringCountsReportWithNoStringBufferMax}, true, eventVectorBufferMax * numCellsPerHash * numHashes},
		{args{&stringMetric, &uniqueDeviceStringCountsReport}, true, eventVectorBufferMax * uint64(stringBufferMax) * numHashes},

		// Invalid input:
		// This report does not have a report type set.
		{args{&occurrenceMetric, &unsetReportTypeReport}, false, 0},
	}
	for _, test := range tests {
		result, err := getSparsityForReport(test.input.metric, test.input.report)
		if test.valid && err != nil {
			t.Errorf("getSparsityForReport() failed for report %s: %v", test.input.report.ReportName, err)
		} else if !test.valid && err == nil {
			t.Errorf("getSparsityForReport() accepted invalid report: %s", test.input.report.ReportName)
		} else if test.valid && result != test.expected {
			t.Errorf("getSparsityForReport() for metric %s and report %s: expected %d, got %d",
				test.input.metric.MetricName, test.input.report.ReportName, test.expected, result)
		}
	}
}
