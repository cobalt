// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package registry_util

import (
	"config"
	"testing"

	"github.com/golang/glog"
)

// The try-bots expect glog to be imported, but we do not use it.
var _ = glog.Info

var (
	validId   = uint32(1)
	invalidId = uint32(0)
	validName = "valid"
)

var (
	testReports = []*config.ReportDefinition{
		&config.ReportDefinition{
			ReportName: validName,
			Id:         validId,
		}}
	testMetrics = []*config.MetricDefinition{
		&config.MetricDefinition{
			MetricName: validName,
			Id:         validId,
			Reports:    testReports,
		}}
	testProjects = []*config.ProjectConfig{
		&config.ProjectConfig{
			ProjectName: validName,
			ProjectId:   validId,
			Metrics:     testMetrics,
		}}
	testCustomers = []*config.CustomerConfig{
		&config.CustomerConfig{
			CustomerName: validName,
			CustomerId:   validId,
			Projects:     testProjects,
		}}
	testRegistryUtil = RegistryUtil{
		registry: config.CobaltRegistry{
			Customers: testCustomers,
		}}
)

func TestFindCustomer(t *testing.T) {
	c, err := testRegistryUtil.FindCustomer(1)
	if err != nil {
		t.Errorf("Valid customer returned err: %v", err)
	} else if c.CustomerName != validName {
		t.Errorf("Wrong customer name got: %v, want: %v", c.CustomerName, validName)
	}

	c, err = testRegistryUtil.FindCustomer(0)
	if err == nil {
		t.Error("Missing customer got: nil, want: error")
	}

	emptyRegistryUtil := RegistryUtil{}
	c, err = emptyRegistryUtil.FindCustomer(1)
	if err == nil {
		t.Error("Find customer with empty registry got: nil, want: error")
	}
}

func TestFindProject(t *testing.T) {
	p, err := testRegistryUtil.FindProject(1, 1)
	if err != nil {
		t.Errorf("Valid project returned err: %v", err)
	} else if p.ProjectName != validName {
		t.Errorf("Wrong project name got: %v, want: %v", p.ProjectName, validName)
	}

	p, err = testRegistryUtil.FindProject(1, 0)
	if err == nil {
		t.Error("Missing project got: nil, want: error")
	}

	p, err = testRegistryUtil.FindProject(0, 1)
	if err == nil {
		t.Error("Find project with missing parent got: nil, want: error")
	}
}

func TestFindMetric(t *testing.T) {
	m, err := testRegistryUtil.FindMetric(1, 1, 1)
	if err != nil {
		t.Errorf("Valid metric returned err: %v", err)
	} else if m.MetricName != validName {
		t.Errorf("Wrong metric name got: %v, want: %v", m.MetricName, validName)
	}

	m, err = testRegistryUtil.FindMetric(1, 1, 0)
	if err == nil {
		t.Error("Missing metric got: nil, want: error")
	}

	m, err = testRegistryUtil.FindMetric(1, 0, 1)
	if err == nil {
		t.Error("Find metric with missing parent got: nil, want: error")
	}
}

func TestFindReport(t *testing.T) {
	r, err := testRegistryUtil.FindReport(1, 1, 1, 1)
	if err != nil {
		t.Errorf("Valid report returned err: %v", err)
	} else if r.ReportName != validName {
		t.Errorf("Wrong report name got: %v, want: %v", r.ReportName, validName)
	}

	r, err = testRegistryUtil.FindReport(1, 1, 1, 0)
	if err == nil {
		t.Error("Missing report got: nil, want: error")
	}

	r, err = testRegistryUtil.FindReport(1, 1, 0, 1)
	if err == nil {
		t.Error("Find report with missing parent got: nil, want: error")
	}
}
