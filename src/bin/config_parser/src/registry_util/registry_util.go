// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package registry_util

import (
	"config"
	"fmt"
	"io/ioutil"

	"google.golang.org/protobuf/proto"
)

type RegistryUtil struct {
	registry config.CobaltRegistry
}

func NewRegistryUtil(fname string) (*RegistryUtil, error) {
	data, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}
	var out config.CobaltRegistry
	err = proto.Unmarshal(data, &out)
	return &RegistryUtil{registry: out}, err
}

func (l *RegistryUtil) FindCustomer(customerId uint32) (*config.CustomerConfig, error) {
	for _, c := range l.registry.GetCustomers() {
		if c.CustomerId == uint32(customerId) {
			return c, nil
		}
	}
	return nil, fmt.Errorf("No customer found with id %v", customerId)
}

func (l *RegistryUtil) FindProject(customerId uint32, projectId uint32) (*config.ProjectConfig, error) {
	customer, err := l.FindCustomer(customerId)
	if err != nil {
		return nil, err
	}
	for _, p := range customer.GetProjects() {
		if p.ProjectId == uint32(projectId) {
			return p, nil
		}
	}
	return nil, fmt.Errorf("No project found with id %v", projectId)
}

func (l *RegistryUtil) FindMetric(customerId uint32, projectId uint32, metricId uint32) (*config.MetricDefinition, error) {
	project, err := l.FindProject(customerId, projectId)
	if err != nil {
		return nil, err
	}
	for _, m := range project.GetMetrics() {
		if m.Id == uint32(metricId) {
			return m, nil
		}
	}
	return nil, fmt.Errorf("No metric found with id %v", metricId)
}

func (l *RegistryUtil) FindReport(customerId uint32, projectId uint32, metricId uint32, reportId uint32) (*config.ReportDefinition, error) {
	metric, err := l.FindMetric(customerId, projectId, metricId)
	if err != nil {
		return nil, err
	}
	for _, r := range metric.GetReports() {
		if r.Id == uint32(reportId) {
			return r, nil
		}
	}
	return nil, fmt.Errorf("No report found with id %v", reportId)
}
