// This file was generated by Cobalt's Registry parser based on the registry YAML
// in the cobalt_config repository. Edit the YAML there to make changes.
package package
const CustomerName string = "the_customer";
const CustomerId uint32 = 10;
const ProjectName string = "the_project";
const ProjectId uint32 = 5;

// Linear bucket constants for linear buckets
const LinearBucketsIntBucketsFloor int64 = 0;
const LinearBucketsIntBucketsNumBuckets uint32 = 140;
const LinearBucketsIntBucketsStepSize uint32 = 5;

// Exponential bucket constants for exponential buckets report
const ExponentialBucketsReportIntBucketsFloor int64 = 0;
const ExponentialBucketsReportIntBucketsNumBuckets uint32 = 3;
const ExponentialBucketsReportIntBucketsInitialStep uint32 = 2;
const ExponentialBucketsReportIntBucketsStepMultiplier uint32 = 2;

// Metric ID Constants
// the_metric_name
const TheMetricNameMetricId uint32 = 100;
// the_other_metric_name
const TheOtherMetricNameMetricId uint32 = 200;
// event groups
const EventGroupsMetricId uint32 = 300;
// linear buckets
const LinearBucketsMetricId uint32 = 400;
// exponential buckets
const ExponentialBucketsMetricId uint32 = 500;
// metric
const MetricMetricId uint32 = 600;
// second metric
const SecondMetricMetricId uint32 = 601;

// Enum for the_other_metric_name (Metric Dimension 0)
type TheOtherMetricNameMetricDimension0 uint32
const (
_ TheOtherMetricNameMetricDimension0 = iota
  TheOtherMetricNameMetricDimension0_AnEvent = 0
  TheOtherMetricNameMetricDimension0_AnotherEvent = 1
  TheOtherMetricNameMetricDimension0_AThirdEvent = 2
)

// Alias for event groups (Metric Dimension The First Group) which has the same event codes
type EventGroupsMetricDimensionTheFirstGroup = TheOtherMetricNameMetricDimension0

// Enum for the_project (Metric Dimension A second group)
type TheProjectMetricDimensionASecondGroup uint32
const (
_ TheProjectMetricDimensionASecondGroup = iota
  TheProjectMetricDimensionASecondGroup_This = 1
  TheProjectMetricDimensionASecondGroup_Is = 2
  TheProjectMetricDimensionASecondGroup_Another = 3
  TheProjectMetricDimensionASecondGroup_Test = 4
)
// Alias for event groups (Metric Dimension A second group) which has the same event codes
type EventGroupsMetricDimensionASecondGroup = TheProjectMetricDimensionASecondGroup

// Enum for the_project (Metric Dimension 2)
type TheProjectMetricDimension2 uint32
const (
_ TheProjectMetricDimension2 = iota
  TheProjectMetricDimension2_ThisMetric = 0
  TheProjectMetricDimension2_HasNo = 2
  TheProjectMetricDimension2_Name = 4
)
// Alias for event groups (Metric Dimension 2) which has the same event codes
type EventGroupsMetricDimension2 = TheProjectMetricDimension2

// Enum for the_project (Metric Dimension First)
type TheProjectMetricDimensionFirst uint32
const (
_ TheProjectMetricDimensionFirst = iota
  TheProjectMetricDimensionFirst_A = 1
  TheProjectMetricDimensionFirst_Set = 2
  TheProjectMetricDimensionFirst_OfEvent = 3
  TheProjectMetricDimensionFirst_Codes = 4
)
// Alias for metric (Metric Dimension First) which has the same event codes
type MetricMetricDimensionFirst = TheProjectMetricDimensionFirst

// Alias for second metric (Metric Dimension First) which has the same event codes
type SecondMetricMetricDimensionFirst = TheProjectMetricDimensionFirst

// Enum for the_project (Metric Dimension Second)
type TheProjectMetricDimensionSecond uint32
const (
_ TheProjectMetricDimensionSecond = iota
  TheProjectMetricDimensionSecond_Some = 0
  TheProjectMetricDimensionSecond_More = 4
  TheProjectMetricDimensionSecond_Event = 8
  TheProjectMetricDimensionSecond_Codes = 16
)
// Alias for metric (Metric Dimension Second) which has the same event codes
type MetricMetricDimensionSecond = TheProjectMetricDimensionSecond

// Alias for second metric (Metric Dimension Second) which has the same event codes
type SecondMetricMetricDimensionSecond = TheProjectMetricDimensionSecond

type EventGroupsEventCodes struct {
  TheFirstGroup EventGroupsMetricDimensionTheFirstGroup
  ASecondGroup EventGroupsMetricDimensionASecondGroup
  Dimension2 EventGroupsMetricDimension2
}

func (s EventGroupsEventCodes) ToArray() []uint32 {
  return []uint32{
    uint32(s.TheFirstGroup),
    uint32(s.ASecondGroup),
    uint32(s.Dimension2),
  }
}

type MetricEventCodes struct {
  First MetricMetricDimensionFirst
  Second MetricMetricDimensionSecond
}

func (s MetricEventCodes) ToArray() []uint32 {
  return []uint32{
    uint32(s.First),
    uint32(s.Second),
  }
}

type SecondMetricEventCodes struct {
  First SecondMetricMetricDimensionFirst
  Second SecondMetricMetricDimensionSecond
}

func (s SecondMetricEventCodes) ToArray() []uint32 {
  return []uint32{
    uint32(s.First),
    uint32(s.Second),
  }
}

// The base64 encoding of the bytes of a serialized CobaltRegistry proto message.
const Config string = "KpgGCgx0aGVfY3VzdG9tZXIQChqFBgoLdGhlX3Byb2plY3QQBRpMCg90aGVfbWV0cmljX25hbWUQChgFIGRiGwoKdGhlX3JlcG9ydBAKGAvCBgjR/KsX0vyrF2IWChB0aGVfb3RoZXJfcmVwb3J0EBQYEhpsChV0aGVfb3RoZXJfbWV0cmljX25hbWUQChgFIMgBKAhQAWIQCgp0aGVfcmVwb3J0EAoYC4IBNRILCAASB0FuRXZlbnQSEAgBEgxBbm90aGVyRXZlbnQSEQgCEg1BIHRoaXJkIGV2ZW50GMgBGuoBCgxldmVudCBncm91cHMQChgFIKwCKAhQAWITCgp0aGVfcmVwb3J0EB4YC9AGAoIBRQoPVGhlIEZpcnN0IEdyb3VwEgsIABIHQW5FdmVudBIQCAESDEFub3RoZXJFdmVudBIRCAISDUEgdGhpcmQgZXZlbnQYAoIBOQoOQSBzZWNvbmQgZ3JvdXASCAgBEgRUaGlzEgYIAhICSXMSCwgDEgdhbm90aGVyEggIBBIEVGVzdIIBNRIOCAASClRoaXNNZXRyaWMSCQgCEgVIYXNObxIICAQSBE5hbWUqDgoFSGFzTm8SBUFsaWFzGiAKDmxpbmVhciBidWNrZXRzEAoYBSCQA0IHEgUQjAEYBRoyChNleHBvbmVudGlhbCBidWNrZXRzEAoYBSD0A2IUCgZyZXBvcnQQKFIICgYQAxgCIAIadgoGbWV0cmljEAoYBSDYBIIBLwoFRmlyc3QSBQgBEgFBEgcIAhIDU2V0EgsIAxIHT2ZFdmVudBIJCAQSBUNvZGVzggEyCgZTZWNvbmQSCAgAEgRTb21lEggIBBIETW9yZRIJCAgSBUV2ZW50EgkIEBIFQ29kZXMafQoNc2Vjb25kIG1ldHJpYxAKGAUg2QSCAS8KBUZpcnN0EgUIARIBQRIHCAISA1NldBILCAMSB09mRXZlbnQSCQgEEgVDb2Rlc4IBMgoGU2Vjb25kEggIABIEU29tZRIICAQSBE1vcmUSCQgIEgVFdmVudBIJCBASBUNvZGVz";
