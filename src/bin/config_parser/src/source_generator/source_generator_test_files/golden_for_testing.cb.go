// This file was generated by Cobalt's Registry parser based on the registry YAML
// in the cobalt_config repository. Edit the YAML there to make changes.
package package
const CustomerName string = "the_customer";
const CustomerId uint32 = 10;
const ProjectName string = "the_project";
const ProjectId uint32 = 5;

// Linear bucket constants for linear buckets
const LinearBucketsIntBucketsFloor int64 = 0;
const LinearBucketsIntBucketsNumBuckets uint32 = 140;
const LinearBucketsIntBucketsStepSize uint32 = 5;

// Exponential bucket constants for exponential buckets report
const ExponentialBucketsReportIntBucketsFloor int64 = 0;
const ExponentialBucketsReportIntBucketsNumBuckets uint32 = 3;
const ExponentialBucketsReportIntBucketsInitialStep uint32 = 2;
const ExponentialBucketsReportIntBucketsStepMultiplier uint32 = 2;

// Metric ID Constants
// the_metric_name
const TheMetricNameMetricId uint32 = 100;
// the_other_metric_name
const TheOtherMetricNameMetricId uint32 = 200;
// event groups
const EventGroupsMetricId uint32 = 300;
// linear buckets
const LinearBucketsMetricId uint32 = 400;
// exponential buckets
const ExponentialBucketsMetricId uint32 = 500;
// metric
const MetricMetricId uint32 = 600;
// second metric
const SecondMetricMetricId uint32 = 601;

// Index into the list of Metrics. Used to look up Metrics in the registry proto directly.
const TheMetricNameMetricIndex uint32 = 0;
const TheOtherMetricNameMetricIndex uint32 = 1;
const EventGroupsMetricIndex uint32 = 2;
const LinearBucketsMetricIndex uint32 = 3;
const ExponentialBucketsMetricIndex uint32 = 4;
const MetricMetricIndex uint32 = 5;
const SecondMetricMetricIndex uint32 = 6;

// Report ID Constants
// the_metric_name the_report
const TheMetricNameTheReportReportId uint32 = 10;
// the_metric_name the_other_report
const TheMetricNameTheOtherReportReportId uint32 = 20;

// Index into the list of Reports. Used to look up Reports in the registry proto directly.
const TheMetricNameTheReportReportIndex uint32 = 0;
const TheMetricNameTheOtherReportReportIndex uint32 = 1;

// Report ID Constants
// the_other_metric_name the_report
const TheOtherMetricNameTheReportReportId uint32 = 10;

// Index into the list of Reports. Used to look up Reports in the registry proto directly.
const TheOtherMetricNameTheReportReportIndex uint32 = 0;

// Report ID Constants
// event groups the_report
const EventGroupsTheReportReportId uint32 = 30;

// Index into the list of Reports. Used to look up Reports in the registry proto directly.
const EventGroupsTheReportReportIndex uint32 = 0;

// Report ID Constants
// exponential buckets report
const ExponentialBucketsReportReportId uint32 = 40;

// Index into the list of Reports. Used to look up Reports in the registry proto directly.
const ExponentialBucketsReportReportIndex uint32 = 0;

// Enum for the_other_metric_name (Metric Dimension 0)
type TheOtherMetricNameMetricDimension0 uint32
const (
_ TheOtherMetricNameMetricDimension0 = iota
  TheOtherMetricNameMetricDimension0_AnEvent = 0
  TheOtherMetricNameMetricDimension0_AnotherEvent = 1
  TheOtherMetricNameMetricDimension0_AThirdEvent = 2
)

// Alias for event groups (Metric Dimension The First Group) which has the same event codes
type EventGroupsMetricDimensionTheFirstGroup = TheOtherMetricNameMetricDimension0

// Enum for the_project (Metric Dimension A second group)
type TheProjectMetricDimensionASecondGroup uint32
const (
_ TheProjectMetricDimensionASecondGroup = iota
  TheProjectMetricDimensionASecondGroup_This = 1
  TheProjectMetricDimensionASecondGroup_Is = 2
  TheProjectMetricDimensionASecondGroup_Another = 3
  TheProjectMetricDimensionASecondGroup_Test = 4
)
// Alias for event groups (Metric Dimension A second group) which has the same event codes
type EventGroupsMetricDimensionASecondGroup = TheProjectMetricDimensionASecondGroup

// Enum for the_project (Metric Dimension 2)
type TheProjectMetricDimension2 uint32
const (
_ TheProjectMetricDimension2 = iota
  TheProjectMetricDimension2_ThisMetric = 0
  TheProjectMetricDimension2_HasNo = 2
  TheProjectMetricDimension2_Name = 4
)
// Alias for event groups (Metric Dimension 2) which has the same event codes
type EventGroupsMetricDimension2 = TheProjectMetricDimension2

// Enum for the_project (Metric Dimension First)
type TheProjectMetricDimensionFirst uint32
const (
_ TheProjectMetricDimensionFirst = iota
  TheProjectMetricDimensionFirst_A = 1
  TheProjectMetricDimensionFirst_Set = 2
  TheProjectMetricDimensionFirst_OfEvent = 3
  TheProjectMetricDimensionFirst_Codes = 4
)
// Alias for metric (Metric Dimension First) which has the same event codes
type MetricMetricDimensionFirst = TheProjectMetricDimensionFirst

// Alias for second metric (Metric Dimension First) which has the same event codes
type SecondMetricMetricDimensionFirst = TheProjectMetricDimensionFirst

// Enum for the_project (Metric Dimension Second)
type TheProjectMetricDimensionSecond uint32
const (
_ TheProjectMetricDimensionSecond = iota
  TheProjectMetricDimensionSecond_Some = 0
  TheProjectMetricDimensionSecond_More = 4
  TheProjectMetricDimensionSecond_Event = 8
  TheProjectMetricDimensionSecond_Codes = 16
)
// Alias for metric (Metric Dimension Second) which has the same event codes
type MetricMetricDimensionSecond = TheProjectMetricDimensionSecond

// Alias for second metric (Metric Dimension Second) which has the same event codes
type SecondMetricMetricDimensionSecond = TheProjectMetricDimensionSecond

type EventGroupsEventCodes struct {
  TheFirstGroup EventGroupsMetricDimensionTheFirstGroup
  ASecondGroup EventGroupsMetricDimensionASecondGroup
  Dimension2 EventGroupsMetricDimension2
}

func (s EventGroupsEventCodes) ToArray() []uint32 {
  return []uint32{
    uint32(s.TheFirstGroup),
    uint32(s.ASecondGroup),
    uint32(s.Dimension2),
  }
}

type MetricEventCodes struct {
  First MetricMetricDimensionFirst
  Second MetricMetricDimensionSecond
}

func (s MetricEventCodes) ToArray() []uint32 {
  return []uint32{
    uint32(s.First),
    uint32(s.Second),
  }
}

type SecondMetricEventCodes struct {
  First SecondMetricMetricDimensionFirst
  Second SecondMetricMetricDimensionSecond
}

func (s SecondMetricEventCodes) ToArray() []uint32 {
  return []uint32{
    uint32(s.First),
    uint32(s.Second),
  }
}

