// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file implements outputLanguage for Go
package source_generator

import (
	"sort"
)

type Go struct {
	packageName string
	currentEnum string
}

func (*Go) getCommentPrefix() string { return "//" }
func (*Go) supportsTypeAlias() bool  { return true }

func (g *Go) writeExtraHeader(so *sourceOutputter, projectName, customerName string, namespaces []string) {
	so.writeLineFmt("package %s", g.packageName)
}

func (*Go) writeExtraFooter(so *sourceOutputter, projectName, customerName string, namespaces []string) {
}

func (g *Go) writeEnumBegin(so *sourceOutputter, name ...string) {
	g.currentEnum = toPascalCase(name...)
	so.writeLineFmt("type %s uint32", toPascalCase(name...))
	so.writeLine("const (")
	so.writeLineFmt("_ %s = iota", toPascalCase(name...))
}

func (g *Go) writeEnumEntry(so *sourceOutputter, value uint32, name ...string) {
	so.writeLineFmt("  %s_%s = %d", g.currentEnum, toPascalCase(name...), value)
}

func (*Go) writeEnumAliasesBegin(so *sourceOutputter, name ...string) {}

func (*Go) writeEnumAlias(so *sourceOutputter, name, from, to []string) {}

func (*Go) writeEnumEnd(so *sourceOutputter, name ...string) {
	so.writeLineFmt(")")
}

func (*Go) writeEnumExport(so *sourceOutputter, enumName, name []string) {}

func (*Go) writeTypeAlias(so *sourceOutputter, from, to []string) {
	so.writeLineFmt("type %s = %s", toPascalCase(to...), toPascalCase(from...))
}

func (*Go) writeNamespacesBegin(so *sourceOutputter, namespaces []string, outputFilename string) {}

func (*Go) writeNamespacesEnd(so *sourceOutputter, namespaces []string, outputFilename string) {}

func (*Go) writeConstUint32(so *sourceOutputter, value uint32, name ...string) {
	so.writeLineFmt("const %s uint32 = %d;", toPascalCase(name...), value)
}

func (*Go) writeConstInt64(so *sourceOutputter, value int64, name ...string) {
	so.writeLineFmt("const %s int64 = %d;", toPascalCase(name...), value)
}

func (*Go) writeConstMap(so *sourceOutputter, value map[uint32]string, name ...string) {
	var keys []uint32
	for k := range value {
		keys = append(keys, k)
	}
	sort.Slice(keys, func(i, j int) bool { return keys[i] < keys[j] })

	so.writeLineFmt("var %s = map[string]uint32{", toPascalCase(name...))
	for _, id := range keys {
		dimensionValue := value[id]
		so.writeLineFmt("    \"%s\": %d,", dimensionValue, id)
	}
	so.writeLineFmt("}")
}

func (*Go) writeStringConstant(so *sourceOutputter, value string, name ...string) {
	so.writeLineFmt("const %s string = \"%s\";", toPascalCase(name...), value)
}

func (*Go) writeStructBegin(so *sourceOutputter, name ...string) {
	so.writeLineFmt("type %s struct {", toPascalCase(name...))
}

func (*Go) writeStructField(so *sourceOutputter, name, typeName []string) {
	so.writeLineFmt("  %s %s", toPascalCase(name...), toPascalCase(typeName...))
}

func (*Go) writeToVectorMethod(so *sourceOutputter, structName []string, fields [][]string) {
	so.writeLine("}")
	so.writeLine("")
	so.writeLineFmt("func (s %s) ToArray() []uint32 {", toPascalCase(structName...))
	so.writeLine("  return []uint32{")
	for _, field := range fields {
		so.writeLineFmt("    uint32(s.%s),", toPascalCase(field...))
	}
	so.writeLine("  }")
}

func (*Go) writeStructEnd(so *sourceOutputter) {
	so.writeLine("}")
}

// varName will be the name of the variable containing the base64-encoded serialized proto.
// packageName is the name of the go package for the generated code.
// If forTesting is true, a constant will be generated for each report ID, based on the report's name.
func GoOutputFactory(varName string, packageName string, options generatorOptions) OutputFormatter {
	return newSourceOutputter(&Go{packageName: packageName}, varName, options).getOutputFormatter()
}
