package config_parser

import (
	"fmt"
	"strconv"
	"strings"
)

// parseCustomerAndProject parses a customer ID and project ID from a project string.
func parseCustomerAndProject(project string) (uint32, uint32, error) {
	projectStrings := strings.Split(project, ":")
	if len(projectStrings) > 2 {
		return 0, 0, fmt.Errorf("project string must be in the form 'customer_id:project_id': %v", project)
	}
	if len(projectStrings) < 2 {
		return 0, 0, fmt.Errorf("project string must be in the form 'customer_id:project_id': %v", project)
	}
	customerId, err := strconv.ParseUint(projectStrings[0], 10, 32)
	if err != nil {
		return 0, 0, err
	}
	projectId, err := strconv.ParseUint(projectStrings[1], 10, 32)
	if err != nil {
		return 0, 0, err
	}
	return uint32(customerId), uint32(projectId), nil
}

// parseCustomerAndProjectAndMetric parses a customer ID, project ID, and metric ID from a metric string.
func parseCustomerAndProjectAndMetric(metric string) (uint32, uint32, uint32, error) {
	metricStrings := strings.Split(metric, ":")
	if len(metricStrings) > 3 {
		return 0, 0, 0, fmt.Errorf("metric string must be in the form 'customer_id:project_id:metric_id': %v", metric)
	}
	if len(metricStrings) < 3 {
		return 0, 0, 0, fmt.Errorf("metric string must be in the form 'customer_id:project_id:metric_id': %v", metric)
	}
	customerId, err := strconv.ParseUint(metricStrings[0], 10, 32)
	if err != nil {
		return 0, 0, 0, err
	}
	projectId, err := strconv.ParseUint(metricStrings[1], 10, 32)
	if err != nil {
		return 0, 0, 0, err
	}
	metricId, err := strconv.ParseUint(metricStrings[2], 10, 32)
	if err != nil {
		return 0, 0, 0, err
	}
	return uint32(customerId), uint32(projectId), uint32(metricId), nil
}

// RemoveExcludedProjects removes filtered projects from the configs.
func RemoveExcludedProjects(excludeProjects []string, configs []ProjectConfigData) ([]ProjectConfigData, error) {
	// Build a map of customer IDs to a set of the project IDs to filter.
	customersToFilter := make(map[uint32]map[uint32]bool)
	for _, excludeProject := range excludeProjects {
		customerId, projectId, err := parseCustomerAndProject(excludeProject)
		if err != nil {
			return nil, err
		}
		customer, ok := customersToFilter[customerId]
		if !ok {
			customersToFilter[customerId] = make(map[uint32]bool)
			customer = customersToFilter[customerId]
		}
		customer[projectId] = true
	}
	filteredConfigs := configs[:0]
	for _, config := range configs {
		if customer, ok := customersToFilter[config.CustomerId]; ok {
			if _, ok = customer[config.ProjectId]; ok {
				continue
			}
		}
		filteredConfigs = append(filteredConfigs, config)
	}
	return filteredConfigs, nil
}

// RemoveExcludedMetrics removes filtered metrics from the configs.
func RemoveExcludedMetrics(excludeMetrics []string, configs []ProjectConfigData) ([]ProjectConfigData, error) {
	// Build a map of customer IDs to a map of the project IDs to a set of the metric IDs to filter.
	customersToFilter := make(map[uint32]map[uint32]map[uint32]bool)
	for _, excludeMetric := range excludeMetrics {
		customerId, projectId, metricId, err := parseCustomerAndProjectAndMetric(excludeMetric)
		if err != nil {
			return nil, err
		}
		projectFilters, ok := customersToFilter[customerId]
		if !ok {
			customersToFilter[customerId] = make(map[uint32]map[uint32]bool)
			projectFilters = customersToFilter[customerId]
		}
		metricFilters, ok := projectFilters[projectId]
		if !ok {
			projectFilters[projectId] = make(map[uint32]bool)
			metricFilters = projectFilters[projectId]
		}
		metricFilters[metricId] = true
	}
	filteredConfigs := configs[:0]
	for _, config := range configs {
		projectFilters, ok := customersToFilter[config.CustomerId]
		if !ok {
			filteredConfigs = append(filteredConfigs, config)
			continue
		}
		metricFilters, ok := projectFilters[config.ProjectId]
		if !ok {
			filteredConfigs = append(filteredConfigs, config)
			continue
		}
		filteredMetrics := config.ProjectConfigFile.MetricDefinitions[:0]
		for _, metric := range config.ProjectConfigFile.MetricDefinitions {
			if _, ok := metricFilters[metric.Id]; !ok {
				filteredMetrics = append(filteredMetrics, metric)
			}
		}

		if len(filteredMetrics) > 0 {
			config.ProjectConfigFile.MetricDefinitions = filteredMetrics
			filteredConfigs = append(filteredConfigs, config)
		}
	}
	return filteredConfigs, nil
}

// RemoveProjectsNotIncluded filters customers/projects in the configs.
func RemoveProjectsNotIncluded(includeCustomers []string, includeProjects []string, configs []ProjectConfigData) ([]ProjectConfigData, error) {
	// Build a map of customer IDs to include.
	customersToInclude := make(map[uint32]bool)
	for _, includeCustomer := range includeCustomers {
		customerId, err := strconv.ParseUint(includeCustomer, 10, 32)
		if err != nil {
			return nil, err
		}
		customersToInclude[uint32(customerId)] = true
	}
	// Build a map of customer IDs to a set of the project IDs to include.
	customersWithProjectsToInclude := make(map[uint32]map[uint32]bool)
	for _, includeProject := range includeProjects {
		customerId, projectId, err := parseCustomerAndProject(includeProject)
		if err != nil {
			return nil, err
		}
		customer, ok := customersWithProjectsToInclude[customerId]
		if !ok {
			customersWithProjectsToInclude[customerId] = make(map[uint32]bool)
			customer = customersWithProjectsToInclude[customerId]
		}
		customer[projectId] = true
	}
	includedConfigs := configs[:0]
	for _, config := range configs {
		if _, ok := customersToInclude[config.CustomerId]; ok {
			includedConfigs = append(includedConfigs, config)
		} else if customer, ok := customersWithProjectsToInclude[config.CustomerId]; ok {
			if _, ok = customer[config.ProjectId]; ok {
				includedConfigs = append(includedConfigs, config)
			}
		}
	}
	return includedConfigs, nil
}
