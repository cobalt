// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package config_parser

import (
	"config"
	"fmt"
	"strings"
	"testing"

	proto "google.golang.org/protobuf/proto"
)

type memConfigReader struct {
	customers string
	projects  map[string]string
}

func (r memConfigReader) Customers() (string, error) {
	return r.customers, nil
}

func (r memConfigReader) CustomersFilePath() string {
	return "fake/customers.yaml"
}

func (r memConfigReader) Project(customerName string, projectName string) (string, error) {
	key := customerName + "|" + projectName
	yaml, ok := r.projects[key]
	if !ok {
		return yaml, fmt.Errorf("Project could not be read!")
	}
	return yaml, nil
}

func (r *memConfigReader) SetProject(customerName string, projectName string, yaml string) {
	if r.projects == nil {
		r.projects = map[string]string{}
	}
	key := customerName + "|" + projectName
	r.projects[key] = yaml
}

const customersYaml = `
customers:
- customer_name: fuchsia
  customer_id: 1
  projects:
    - project_name: ledger
      project_id: 1
      project_contact: bob
    - project_name: module_usage_tracking
      project_id: 2
      project_contact: bob
- customer_name: test_customer
  customer_id: 100
  projects:
    - project_name: test_project
      project_id: 1
      project_contact: bob
`

const customersYamlWithDeletedCustomerIds = `
customers:
- customer_name: fuchsia
  customer_id: 1
  projects:
    - project_name: ledger
      project_id: 1
      project_contact: bob
    - project_name: module_usage_tracking
      project_id: 2
      project_contact: bob
- customer_name: test_customer
  customer_id: 100
  projects:
    - project_name: test_project
      project_id: 1
      project_contact: bob
deleted_customer_ids: [200, 201, 202]
`

const customersYamlWithReusedDeletedCustomerIds = `
customers:
- customer_name: fuchsia
  customer_id: 1
  projects:
    - project_name: ledger
      project_id: 1
      project_contact: bob
    - project_name: module_usage_tracking
      project_id: 2
      project_contact: bob
- customer_name: test_customer
  customer_id: 100
  projects:
    - project_name: test_project
      project_id: 1
      project_contact: bob
deleted_customer_ids: [100, 200, 201, 202]
`

const invalidCustomersYaml = `
- customer_name: fuchsia
  customer_id: 1
  projects:
    - naINVALIDme: ledger
      project_id: 1
      project_contact: bob
    - project_name: module_usage_tracking
      project_id: 2
      project_contact: bob
- customer_name: test_customer
  customer_id: 100
  projects:
    - project_name: test_project
      project_id: 1
      project_contact: bob
`

const projectConfigYaml = `
metric_definitions:
- id: 1
  metric_name: "MetricB1a"
`

// Tests the ReadProjectConfigData function's basic functionality.
func TestReadProjectConfigData(t *testing.T) {
	r := memConfigReader{}
	r.SetProject("customer", "project", projectConfigYaml)
	c := ProjectConfigData{
		CustomerName: "customer",
		CustomerId:   10,
		ProjectName:  "project",
		ProjectId:    5,
	}
	if err := ReadProjectConfigData(r, &c); err != nil {
		t.Errorf("Error reading project config: %v", err)
	}
}

// Tests the ReadConfig function's basic functionality.
func TestReadConfig(t *testing.T) {
	r := memConfigReader{
		customers: customersYaml}
	r.SetProject("fuchsia", "ledger", projectConfigYaml)
	r.SetProject("fuchsia", "module_usage_tracking", projectConfigYaml)
	r.SetProject("test_customer", "test_project", projectConfigYaml)
	l := []ProjectConfigData{}
	if err := ReadConfig(r, &l); err != nil {
		t.Errorf("Error reading project config: %v", err)
	}

	if 3 != len(l) {
		t.Errorf("Expected 3 customers. Got %v.", len(l))
	}
}

// Tests the ReadConfig function's functionality with deleted customer IDs.
func TestReadConfigWithDeletedCustomerIds(t *testing.T) {
	r := memConfigReader{
		customers: customersYamlWithDeletedCustomerIds}
	r.SetProject("fuchsia", "ledger", projectConfigYaml)
	r.SetProject("fuchsia", "module_usage_tracking", projectConfigYaml)
	r.SetProject("test_customer", "test_project", projectConfigYaml)
	l := []ProjectConfigData{}
	if err := ReadConfig(r, &l); err != nil {
		t.Errorf("Error reading project config: %v", err)
	}

	if len(l) != 6 {
		t.Errorf("Expected 3 customers plus 3 deleted customers. Got %v.", len(l))
	}
}

// Tests the ReadConfig function's functionality checks for reusing deleted customer IDs.
func TestReadConfigWithReusedDeletedCustomerIds(t *testing.T) {
	r := memConfigReader{
		customers: customersYamlWithReusedDeletedCustomerIds}
	r.SetProject("fuchsia", "ledger", projectConfigYaml)
	r.SetProject("fuchsia", "module_usage_tracking", projectConfigYaml)
	r.SetProject("test_customer", "test_project", projectConfigYaml)
	l := []ProjectConfigData{}

	if err := ReadConfig(r, &l); err == nil {
		t.Errorf("Error expected reusing deleted customer ID for project config: %v", l)
	}
}

// Tests that ReadConfig customer parsing failures include the YAML file project_name.
func TestReadInvalidConfig(t *testing.T) {
	r := memConfigReader{
		customers: invalidCustomersYaml}
	r.SetProject("fuchsia", "ledger", projectConfigYaml)
	r.SetProject("fuchsia", "module_usage_tracking", projectConfigYaml)
	r.SetProject("test_customer", "test_project", projectConfigYaml)
	l := []ProjectConfigData{}
	if err := ReadConfig(r, &l); err != nil {
		if !strings.Contains(err.Error(), "fake/customers.yaml") {
			t.Errorf("Error message did not contain the expected fake/customers.yaml: %v", err)
		}
	} else {
		t.Errorf("Expected to get an error")
	}
}

func TestMergeConfigs(t *testing.T) {
	l := []ProjectConfigData{
		{
			CustomerName:      "customer5",
			CustomerId:        5,
			ProjectName:       "project3",
			ProjectId:         3,
			Contact:           "project3@customer5.com",
			DeletedProjectIds: []uint32{4, 5},
		},
		{
			CustomerName:         "customer2",
			CustomerId:           2,
			ProjectName:          "project1",
			ProjectId:            1,
			Contact:              "project1@customer2.com",
			AppPackageIdentifier: "com.customer2.project1",
			DeletedProjectIds:    []uint32{2},
		},
		{
			CustomerName:      "customer5",
			CustomerId:        5,
			ProjectName:       "project2",
			ProjectId:         2,
			Contact:           "project2@customer5.com",
			DeletedProjectIds: []uint32{4, 5},
		},
		{
			CustomerId:        100,
			IsDeletedCustomer: true,
		},
		{
			CustomerId:        101,
			IsDeletedCustomer: true,
		},
		{
			CustomerId:        102,
			IsDeletedCustomer: true,
		},
	}

	s := MergeConfigs(l)

	expected := config.CobaltRegistry{
		Customers: []*config.CustomerConfig{
			{
				CustomerName:      "customer2",
				CustomerId:        2,
				DeletedProjectIds: []uint32{2},
				Projects: []*config.ProjectConfig{
					{
						ProjectName:          "project1",
						ProjectId:            1,
						ProjectContact:       "project1@customer2.com",
						AppPackageIdentifier: "com.customer2.project1",
					},
				},
			},
			{
				CustomerName:      "customer5",
				CustomerId:        5,
				DeletedProjectIds: []uint32{4, 5},
				Projects: []*config.ProjectConfig{
					{
						ProjectName:    "project2",
						ProjectId:      2,
						ProjectContact: "project2@customer5.com",
					},
					{
						ProjectName:    "project3",
						ProjectId:      3,
						ProjectContact: "project3@customer5.com",
					},
				},
			},
		},
		DeletedCustomerIds: []uint32{100, 101, 102},
	}
	if !proto.Equal(&s, &expected) {
		t.Errorf("%v != %v", s, expected)
	}
}
