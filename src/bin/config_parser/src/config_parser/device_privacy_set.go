// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package config_parser

import (
	"config"
)

func SetDevicePrivacyDependencySets(configs []ProjectConfigData) {
	for _, c := range configs {
		if !c.IsDeletedCustomer {
			setDevicePrivacyDependencySetForConfig(&c)
		}
	}
}

func setDevicePrivacyDependencySetForConfig(c *ProjectConfigData) {
	for _, metric := range c.ProjectConfigFile.MetricDefinitions {
		for _, report := range metric.Reports {
			setDevicePrivacyDependencySet(report)
		}
	}
}

func setDevicePrivacyDependencySet(report *config.ReportDefinition) {
	if report.PrivacyMechanism != config.ReportDefinition_SHUFFLED_DIFFERENTIAL_PRIVACY {
		return
	}

	// Assume all reports use V1 privacy dependency set.
	//
	// Any field affecting privacy or private index encodeing that is not one of
	// the below must result in V2 or higher being added and used.
	//  * metric dimensions
	//  * num_index_points
	//  * string_sketch_params
	//  * min_value
	//  * max_value
	//  * max_count
	//  * int_buckets
	//  * event_vector_buffer_max
	//  * string_buffer_max
	// - poisson_mean
	if report.GetShuffledDp() != nil {
		report.GetShuffledDp().DevicePrivacyDependencySet = config.ReportDefinition_ShuffledDifferentialPrivacyConfig_V1
	}

}
