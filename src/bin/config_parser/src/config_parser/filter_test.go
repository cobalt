package config_parser

import (
	"config"
	"reflect"
	"regexp"
	"testing"
)

var excludeProjectsTests = []struct {
	name              string
	before            []ProjectConfigData
	excludeProjects   []string
	after             []ProjectConfigData
	wantErrorMatching *regexp.Regexp
}{
	{
		name: "empty",
		before: []ProjectConfigData{
			ProjectConfigData{},
		},
		after: []ProjectConfigData{
			ProjectConfigData{},
		},
	},
	{
		name: "exclude only project",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
		},
		excludeProjects: []string{"1:456"},
		after:           []ProjectConfigData{},
	},
	{
		name: "exclude one project",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  789,
			},
		},
		excludeProjects: []string{"1:456"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  789,
			},
		},
	},
	{
		name: "exclude multiple projects",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  789,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  890,
			},
		},
		excludeProjects: []string{"1:890", "1:456"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  789,
			},
		},
	},
	{
		name: "exclude no matching projects",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  789,
			},
		},
		excludeProjects: []string{"1:654"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  789,
			},
		},
	},
	{
		name: "error too many separators",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  456,
			},
		},
		excludeProjects:   []string{"123:456:789"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("project string must be in the form 'customer_id:project_id'"),
	},
	{
		name: "error empty string",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  456,
			},
		},
		excludeProjects:   []string{""},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("project string must be in the form 'customer_id:project_id'"),
	},
	{
		name: "error not numeric customer",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  456,
			},
		},
		excludeProjects:   []string{"a:456"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("invalid syntax"),
	},
	{
		name: "error not numeric project",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  456,
			},
		},
		excludeProjects:   []string{"123:a"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("invalid syntax"),
	},
}

func TestExcludeProjects(t *testing.T) {
	for _, tt := range excludeProjectsTests {
		result, err := RemoveExcludedProjects(tt.excludeProjects, tt.before)

		if tt.wantErrorMatching != nil {
			if err == nil {
				t.Errorf("%s succeeded unexpectedly: wanted error matching: %v",
					tt.name, tt.wantErrorMatching.String())
			} else if !tt.wantErrorMatching.MatchString(err.Error()) {
				t.Errorf("%s got error %v wanted error matching: %v",
					tt.name, err.Error(), tt.wantErrorMatching.String())
			}
			continue
		}
		if err != nil {
			t.Errorf("%s failed unexpectedly; got error: %v wanted: %v",
				tt.name, err, tt.after)
			continue
		}
		if !reflect.DeepEqual(result, tt.after) {
			t.Errorf("%s was not as expected.\n Got: %v\n Expected: %v\n",
				tt.name, result, tt.after)
		}
	}
}

var excludeMetricsTests = []struct {
	name              string
	before            []ProjectConfigData
	excludeMetrics    []string
	after             []ProjectConfigData
	wantErrorMatching *regexp.Regexp
}{
	{
		name: "empty",
		before: []ProjectConfigData{
			ProjectConfigData{},
		},
		after: []ProjectConfigData{
			ProjectConfigData{},
		},
	},
	{
		name: "exclude only metric",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 567},
					},
				},
			},
		},
		excludeMetrics: []string{"1:234:567"},
		after:          []ProjectConfigData{},
	},
	{
		name: "exclude one metric",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 567},
						&config.MetricDefinition{Id: 890},
					},
				},
			},
		},
		excludeMetrics: []string{"1:234:567"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 890},
					},
				},
			},
		},
	},
	{
		name: "exclude multiple metrics",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 567},
						&config.MetricDefinition{Id: 890},
						&config.MetricDefinition{Id: 123},
					},
				},
			},
		},
		excludeMetrics: []string{"1:234:567", "1:234:123"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 890},
					},
				},
			},
		},
	},
	{
		name: "exclude multiple metrics multiple projects",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 567},
						&config.MetricDefinition{Id: 890},
					},
				},
			},
			ProjectConfigData{
				CustomerId: 10,
				ProjectId:  2340,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 5670},
						&config.MetricDefinition{Id: 8900},
					},
				},
			},
		},
		excludeMetrics: []string{"1:234:567", "10:2340:5670"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 890},
					},
				},
			},
			ProjectConfigData{
				CustomerId: 10,
				ProjectId:  2340,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 8900},
					},
				},
			},
		},
	},
	{
		name: "exclude no matching metrics",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 567},
						&config.MetricDefinition{Id: 890},
					},
				},
			},
		},
		excludeMetrics: []string{"1:234:001"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
				ProjectConfigFile: &config.ProjectConfigFile{
					MetricDefinitions: []*config.MetricDefinition{
						&config.MetricDefinition{Id: 567},
						&config.MetricDefinition{Id: 890},
					},
				},
			},
		},
	},
	{
		name: "error too many separators",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
		},
		excludeMetrics:    []string{"123:456:789:012"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("metric string must be in the form 'customer_id:project_id:metric_id'"),
	},
	{
		name: "error too few separators",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
		},
		excludeMetrics:    []string{"123:456"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("metric string must be in the form 'customer_id:project_id:metric_id'"),
	},
	{
		name: "error empty string",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
		},
		excludeMetrics:    []string{""},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("metric string must be in the form 'customer_id:project_id:metric_id'"),
	},
	{
		name: "error not numeric customer",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
		},
		excludeMetrics:    []string{"a:456:789"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("invalid syntax"),
	},
	{
		name: "error not numeric project",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
		},
		excludeMetrics:    []string{"123:a:456"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("invalid syntax"),
	},
	{
		name: "error not numeric metric",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
		},
		excludeMetrics:    []string{"123:456:a"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("invalid syntax"),
	},
}

func TestExcludeMetrics(t *testing.T) {
	for _, tt := range excludeMetricsTests {
		result, err := RemoveExcludedMetrics(tt.excludeMetrics, tt.before)

		if tt.wantErrorMatching != nil {
			if err == nil {
				t.Errorf("%s succeeded unexpectedly: wanted error matching: %v",
					tt.name, tt.wantErrorMatching.String())
			} else if !tt.wantErrorMatching.MatchString(err.Error()) {
				t.Errorf("%s got error %v wanted error matching: %v",
					tt.name, err.Error(), tt.wantErrorMatching.String())
			}
			continue
		}
		if err != nil {
			t.Errorf("%s failed unexpectedly; got error: %v wanted: %v",
				tt.name, err, tt.after)
			continue
		}
		if !reflect.DeepEqual(result, tt.after) {
			t.Errorf("%s was not as expected.\n Got: %v\n Expected: %v\n",
				tt.name, result, tt.after)
		}
	}
}

var removeProjectsNotIncludedTests = []struct {
	name              string
	before            []ProjectConfigData
	includeCustomers  []string
	includeProjects   []string
	after             []ProjectConfigData
	wantErrorMatching *regexp.Regexp
}{
	{
		name: "empty",
		before: []ProjectConfigData{
			ProjectConfigData{},
		},
		after: []ProjectConfigData{},
	},
	{
		name: "include only customer",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
		},
		includeCustomers: []string{"1"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
		},
	},
	{
		name: "include only project",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
		},
		includeProjects: []string{"1:456"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
		},
	},
	{
		name: "include one customer",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 2,
				ProjectId:  456,
			},
			ProjectConfigData{
				CustomerId: 3,
				ProjectId:  789,
			},
		},
		includeCustomers: []string{"2"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 2,
				ProjectId:  456,
			},
		},
	},
	{
		name: "include one project",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  789,
			},
		},
		includeProjects: []string{"1:456"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
		},
	},
	{
		name: "include multiple customers",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 2,
				ProjectId:  456,
			},
			ProjectConfigData{
				CustomerId: 3,
				ProjectId:  789,
			},
			ProjectConfigData{
				CustomerId: 4,
				ProjectId:  890,
			},
		},
		includeCustomers: []string{"4", "2"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 2,
				ProjectId:  456,
			},
			ProjectConfigData{
				CustomerId: 4,
				ProjectId:  890,
			},
		},
	},
	{
		name: "include multiple projects",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  789,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  890,
			},
		},
		includeProjects: []string{"1:890", "1:456"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  456,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  890,
			},
		},
	},
	{
		name: "include multiple customers and projects",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 2,
				ProjectId:  456,
			},
			ProjectConfigData{
				CustomerId: 2,
				ProjectId:  789,
			},
			ProjectConfigData{
				CustomerId: 3,
				ProjectId:  890,
			},
		},
		includeCustomers: []string{"2", "1"},
		includeProjects:  []string{"3:890", "2:456"},
		after: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 2,
				ProjectId:  456,
			},
			ProjectConfigData{
				CustomerId: 2,
				ProjectId:  789,
			},
			ProjectConfigData{
				CustomerId: 3,
				ProjectId:  890,
			},
		},
	},
	{
		name: "include no matching projects",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 1,
				ProjectId:  789,
			},
		},
		includeCustomers: []string{"2"},
		includeProjects:  []string{"1:654"},
		after:            []ProjectConfigData{},
	},
	{
		name: "error not numeric customer in include customers",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  456,
			},
		},
		includeCustomers:  []string{"a"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("invalid syntax"),
	},
	{
		name: "error too many separators",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  456,
			},
		},
		includeProjects:   []string{"123:456:789"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("project string must be in the form 'customer_id:project_id'"),
	},
	{
		name: "error empty string",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  456,
			},
		},
		includeProjects:   []string{""},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("project string must be in the form 'customer_id:project_id'"),
	},
	{
		name: "error not numeric customer in include projects",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  456,
			},
		},
		includeProjects:   []string{"a:456"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("invalid syntax"),
	},
	{
		name: "error not numeric project",
		before: []ProjectConfigData{
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  234,
			},
			ProjectConfigData{
				CustomerId: 123,
				ProjectId:  456,
			},
		},
		includeProjects:   []string{"123:a"},
		after:             []ProjectConfigData{},
		wantErrorMatching: regexp.MustCompile("invalid syntax"),
	},
}

func TestRemoveProjectsNotIncluded(t *testing.T) {
	for _, tt := range removeProjectsNotIncludedTests {
		result, err := RemoveProjectsNotIncluded(tt.includeCustomers, tt.includeProjects, tt.before)

		if tt.wantErrorMatching != nil {
			if err == nil {
				t.Errorf("%s succeeded unexpectedly: wanted error matching: %v",
					tt.name, tt.wantErrorMatching.String())
			} else if !tt.wantErrorMatching.MatchString(err.Error()) {
				t.Errorf("%s got error %v wanted error matching: %v",
					tt.name, err.Error(), tt.wantErrorMatching.String())
			}
			continue
		}
		if err != nil {
			t.Errorf("%s failed unexpectedly; got error: %v wanted: %v",
				tt.name, err, tt.after)
			continue
		}
		if !reflect.DeepEqual(result, tt.after) {
			t.Errorf("%s was not as expected.\n Got: %v\n Expected: %v\n",
				tt.name, result, tt.after)
		}
	}
}
