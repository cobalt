// Copyright 2017 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package config_parser

import (
	"testing"

	"config"

	"google.golang.org/protobuf/encoding/prototext"
	"google.golang.org/protobuf/proto"
)

// Basic test for parseProjectConfigData.
func TestParseProjectConfigData(t *testing.T) {
	y := `
metric_definitions:
- metric_name: the_metric_name
  id: 1
  time_zone_policy: UTC
  reports:
  - report_name: the_report
    id: 10
    report_type: FLEETWIDE_OCCURRENCE_COUNTS
  - report_name: the_other_report
    id: 20
    report_type: HOURLY_VALUE_NUMERIC_STATS
- metric_name: the_other_metric_name
  id: 2
  time_zone_policy: LOCAL
  reports:
  - report_name: the_report
    id: 10
    report_type: HOURLY_VALUE_NUMERIC_STATS
`
	c := ProjectConfigData{
		CustomerId:   1,
		ProjectId:    10,
		CustomerName: "customer_name",
		ProjectName:  "project_name",
	}

	if err := parseProjectConfigData(y, &c); err != nil {
		t.Error(err)
	}

	e := config.ProjectConfigFile{
		MetricDefinitions: []*config.MetricDefinition{
			{
				CustomerId:     1,
				ProjectId:      10,
				MetricName:     "the_metric_name",
				Id:             1,
				TimeZonePolicy: config.MetricDefinition_UTC,
				Reports: []*config.ReportDefinition{
					{
						ReportName: "the_report",
						Id:         10,
						ReportType: config.ReportDefinition_FLEETWIDE_OCCURRENCE_COUNTS,
					},
					{
						ReportName: "the_other_report",
						Id:         20,
						ReportType: config.ReportDefinition_HOURLY_VALUE_NUMERIC_STATS,
					},
				},
			},
			{
				CustomerId:     1,
				ProjectId:      10,
				MetricName:     "the_other_metric_name",
				Id:             2,
				TimeZonePolicy: config.MetricDefinition_LOCAL,
				Reports: []*config.ReportDefinition{
					{
						ReportName: "the_report",
						Id:         10,
						ReportType: config.ReportDefinition_HOURLY_VALUE_NUMERIC_STATS,
					},
				},
			},
		},
	}

	if !proto.Equal(&e, c.ProjectConfigFile) {
		t.Errorf("%v\n!=\n%v", prototext.Format(&e), prototext.Format(c.ProjectConfigFile))
	}

}

// Tests that we catch non-unique encoding ids.
func TestParseProjectConfigDataUniqueEncodingIds(t *testing.T) {
	y := `
metric_configs:
- id: 1
  name: metric_name
  time_zone_policy: UTC
- id: 2
  name: other_metric_name
  time_zone_policy: LOCAL
encoding_configs:
- id: 1
  basic_rappor:
    prob_0_becomes_1: 0.5
    prob_1_stays_1: 0.5
- id: 1
report_configs:
- id: 1
  metric_id: 5
- id: 2
`

	c := ProjectConfigData{
		CustomerId: 1,
		ProjectId:  10,
	}

	if err := parseProjectConfigData(y, &c); err == nil {
		t.Error("Accepted non-unique encoding id.")
	}
}

// Test for verifying that privacy maencahism and privacy config are parsed
// correctly if present.
func TestParseProjectConfigDataWithPrivacyMechanismAndConfig(t *testing.T) {
	y := `
metric_definitions:
- metric_name: the_metric_name
  id: 1
  time_zone_policy: UTC
  reports:
  - report_name: the_report
    id: 10
    report_type: FLEETWIDE_OCCURRENCE_COUNTS
    privacy_mechanism: DE_IDENTIFICATION
  - report_name: the_other_report
    id: 20
    report_type: HOURLY_VALUE_NUMERIC_STATS
    privacy_mechanism: SHUFFLED_DIFFERENTIAL_PRIVACY
    shuffled_dp:
      epsilon: 1
      delta: 0.001
      reporting_threshold: 10000
      poisson_mean: 1
      device_privacy_dependency_set: V1
`
	c := ProjectConfigData{
		CustomerId:   1,
		ProjectId:    10,
		CustomerName: "customer_name",
		ProjectName:  "project_name",
	}

	if err := parseProjectConfigData(y, &c); err != nil {
		t.Error(err)
	}

	e := config.ProjectConfigFile{
		MetricDefinitions: []*config.MetricDefinition{
			{
				CustomerId:     1,
				ProjectId:      10,
				MetricName:     "the_metric_name",
				Id:             1,
				TimeZonePolicy: config.MetricDefinition_UTC,
				Reports: []*config.ReportDefinition{
					{
						ReportName:       "the_report",
						Id:               10,
						ReportType:       config.ReportDefinition_FLEETWIDE_OCCURRENCE_COUNTS,
						PrivacyMechanism: config.ReportDefinition_DE_IDENTIFICATION,
					},
					{
						ReportName:       "the_other_report",
						Id:               20,
						ReportType:       config.ReportDefinition_HOURLY_VALUE_NUMERIC_STATS,
						PrivacyMechanism: config.ReportDefinition_SHUFFLED_DIFFERENTIAL_PRIVACY,
						PrivacyConfig: &config.ReportDefinition_ShuffledDp{
							ShuffledDp: &config.ReportDefinition_ShuffledDifferentialPrivacyConfig{
								Epsilon:                    1,
								Delta:                      0.001,
								ReportingThreshold:         10000,
								PoissonMean:                1,
								DevicePrivacyDependencySet: config.ReportDefinition_ShuffledDifferentialPrivacyConfig_V1,
							},
						},
					},
				},
			},
		},
	}

	if !proto.Equal(&e, c.ProjectConfigFile) {
		t.Errorf("%v\n!=\n%v", prototext.Format(&e), prototext.Format(c.ProjectConfigFile))
	}
}
