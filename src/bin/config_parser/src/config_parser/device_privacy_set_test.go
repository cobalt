// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package config_parser

import (
	"config"
	"testing"
)

func TestDeIdentifiedReportNoDependencySetSpecified(t *testing.T) {
	report := config.ReportDefinition{
		PrivacyMechanism: config.ReportDefinition_DE_IDENTIFICATION,
	}
	setDevicePrivacyDependencySet(&report)
	if report.PrivacyConfig != nil {
		t.Errorf("Set privacy dependency set on de-identified report")
	}
}

func TestShuffledDpReportNoDependencySetSpecified(t *testing.T) {
	report := config.ReportDefinition{
		PrivacyMechanism: config.ReportDefinition_SHUFFLED_DIFFERENTIAL_PRIVACY,
		PrivacyConfig: &config.ReportDefinition_ShuffledDp{
			ShuffledDp: &config.ReportDefinition_ShuffledDifferentialPrivacyConfig{},
		},
	}
	if report.PrivacyConfig == nil {
		t.Errorf("000: Missing privacy config")
		return
	}
	if report.GetShuffledDp() == nil {
		t.Errorf("Missing privacy config")
		return
	}

	setDevicePrivacyDependencySet(&report)

	if report.GetShuffledDp().DevicePrivacyDependencySet != config.ReportDefinition_ShuffledDifferentialPrivacyConfig_V1 {
		t.Errorf("Device privacy dependency set must be v1")
	}
}
