// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file contains flag definitions for the config_parser package as well
// as all the functions which make direct use of these flags.
// TODO(https://fxbug.dev/278917650): Refactor and test. This is half of the logic that was ripped out
// config_parser_main.go and it still needs to be refactored.
package config_parser

import (
	"flag"
	"fmt"
)

var (
	configDirs flagList
	customerId = flag.Int64("customer_id", -1, "Customer Id for the config to be read. Must be set if and only if 'config_file' is set.")
	projectId  = flag.Int64("project_id", -1, "Project Id for the config to be read. Must be set if and only if 'config_file' is set.")
)

type flagList []string

func (f *flagList) String() string {
	return fmt.Sprintf("%q", *f)
}

func (f *flagList) Set(value string) error {
	*f = append(*f, value)
	return nil
}

// Initialization function that is run before any main().
func init() {
	flag.Var(&configDirs, "config_dir", "Required. Directory containing the config, can be specified multiple times.")
}

// checkFlags verifies that the specified flags are compatible with each other.
func checkFlags() error {
	if len(configDirs) == 0 {
		return fmt.Errorf("One or more 'config_dir' must be set.")
	}
	return nil
}

// appendNewCustomers adds the new project configs to the list, verifying that
// none of their customers overlap with existing customers.
func appendNewCustomers(existingCustomers map[uint32]string, configs []ProjectConfigData, newConfigs ...ProjectConfigData) (map[uint32]string, []ProjectConfigData, error) {
	for _, config := range newConfigs {
		if customerName, ok := existingCustomers[config.CustomerId]; ok {
			return nil, nil, fmt.Errorf("Duplicate customer ID %v for customers %s and %s", config.CustomerId, customerName, config.CustomerName)
		}
	}
	configs = append(configs, newConfigs...)
	for _, config := range newConfigs {
		existingCustomers[config.CustomerId] = config.CustomerName
	}
	return existingCustomers, configs, nil
}

// ParseProjectConfigDataFromFlags uses the specified flags to find the
// specified registry, read and parse it.
func ParseProjectConfigDataFromFlags() ([]ProjectConfigData, error) {
	if err := checkFlags(); err != nil {
		return nil, err
	}

	existingCustomers := make(map[uint32]string)
	configs := []ProjectConfigData{}
	var pc ProjectConfigData
	var err error
	var tempErr error
	found := false
	if *customerId >= 0 && *projectId >= 0 {
		for _, configDir := range configDirs {
			if pc, tempErr = ReadProjectConfigDataFromDir(configDir, uint32(*customerId), uint32(*projectId)); tempErr != nil {
				err = tempErr
				continue
			}
			if existingCustomers, configs, tempErr = appendNewCustomers(existingCustomers, configs, pc); tempErr != nil {
				err = tempErr
				continue
			}
			found = true
		}
		if !found {
			return nil, err
		}
		err = nil
	} else {
		var dirConfigs []ProjectConfigData
		for _, configDir := range configDirs {
			if dirConfigs, err = ReadConfigFromDir(configDir); err != nil {
				return nil, err
			}
			if existingCustomers, configs, err = appendNewCustomers(existingCustomers, configs, dirConfigs...); err != nil {
				return nil, err
			}
		}
	}
	return configs, err
}

// GetConfigFilesListFromFlags returns a list of all the files that comprise
// the registry being parsed. This can be used to generate a depfile.
func GetConfigFilesListFromFlags() ([]string, error) {
	if err := checkFlags(); err != nil {
		return nil, err
	}
	files := []string{}
	var dirFiles []string
	var err error
	for _, configDir := range configDirs {
		dirFiles, err = GetConfigFilesListFromConfigDir(configDir)
		files = append(files, dirFiles...)
	}
	return files, err
}
