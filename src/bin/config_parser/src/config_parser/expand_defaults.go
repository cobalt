package config_parser

import (
	"config"
	"fmt"
)

func ExpandDefaults(configs []ProjectConfigData) {
	for _, c := range configs {
		if !c.IsDeletedCustomer {
			expandDefaultsForConfig(&c)
		}
	}
}

func expandDefaultsForConfig(c *ProjectConfigData) {
	for _, metric := range c.ProjectConfigFile.MetricDefinitions {
		if metric.MetricType == config.MetricDefinition_STRUCT {
			expandDefaultsForStructMetric(metric)
		}
		if metric.MetricType == config.MetricDefinition_INTEGER_HISTOGRAM {
			expandDefaultsForIntegerHistogramMetric(metric)
		}
		for _, report := range metric.Reports {
			expandDefaultsForReport(report)
			switch report.ReportType {
			case config.ReportDefinition_UNIQUE_DEVICE_HISTOGRAMS:
				fallthrough
			case config.ReportDefinition_HOURLY_VALUE_HISTOGRAMS:
				fallthrough
			case config.ReportDefinition_FLEETWIDE_HISTOGRAMS:
				expandDefaultsForIntegerHistogramReport(report, metric)
			}
		}
	}
}

var DefaultSystemProfileSelectionPolicy = map[config.ReportDefinition_ReportType]config.SystemProfileSelectionPolicy{
	config.ReportDefinition_FLEETWIDE_OCCURRENCE_COUNTS: config.SystemProfileSelectionPolicy_REPORT_ALL,
	config.ReportDefinition_FLEETWIDE_HISTOGRAMS:        config.SystemProfileSelectionPolicy_REPORT_ALL,
	config.ReportDefinition_FLEETWIDE_MEANS:             config.SystemProfileSelectionPolicy_REPORT_ALL,
	config.ReportDefinition_UNIQUE_DEVICE_NUMERIC_STATS: config.SystemProfileSelectionPolicy_REPORT_ALL,
	config.ReportDefinition_HOURLY_VALUE_NUMERIC_STATS:  config.SystemProfileSelectionPolicy_REPORT_ALL,
	config.ReportDefinition_STRING_COUNTS:               config.SystemProfileSelectionPolicy_REPORT_ALL,
}

func expandDefaultsForReport(report *config.ReportDefinition) {
	if report.LocalAggregationProcedure == config.ReportDefinition_SELECT_FIRST && report.EventVectorBufferMax == 0 {
		report.EventVectorBufferMax = 1
	}

	if report.SystemProfileSelection == config.SystemProfileSelectionPolicy_SYSTEM_PROFILE_SELECTION_POLICY_UNSET {
		if def, ok := DefaultSystemProfileSelectionPolicy[report.ReportType]; ok {
			report.SystemProfileSelection = def
		}
	}
}

func expandDefaultsForStructMetric(m *config.MetricDefinition) {
	nameToId := map[string]*config.MetricDefinition_StructField{}
	for _, f := range m.StructFields {
		nameToId[f.Name] = f
	}

	for _, r := range m.Reports {
		for _, a := range r.Aggregates {
			// Set Name to For if Name is unspecified.
			if len(a.Name) == 0 {
				a.Name = a.For
			}

			// Set For to Name if For is unspecified.
			if len(a.For) == 0 {
				a.For = a.Name
			}

			// Lookup the field corresponding to the specified For.
			if f, ok := nameToId[a.For]; ok {
				a.ForId = f.Id

				if f.Type == config.MetricDefinition_StructField_STRING && len(a.StringCandidateFile) == 0 {
					a.StringCandidateFile = f.StringCandidateFile
				}
			}
		}
	}
}

func expandDefaultsForIntegerHistogramReport(report *config.ReportDefinition, metric *config.MetricDefinition) error {
	// Do nothing because FLEETWIDE_HISTOGRAMS report for an INTEGER_HISTOGRAM metric should not redefine int_buckets.
	if report.ReportType == config.ReportDefinition_FLEETWIDE_HISTOGRAMS && metric.MetricType == config.MetricDefinition_INTEGER_HISTOGRAM {
		return nil
	}

	bucket_value := report.IntBuckets.GetExponential()
	if bucket_value == nil {
		return nil
	}
	if bucket_value.StepMultiplierFloat != 0 {
		return fmt.Errorf("step_multiplier_float should not be set by customer")
	}

	bucket_value.StepMultiplierFloat = float32(bucket_value.StepMultiplier)
	return nil
}

func expandDefaultsForIntegerHistogramMetric(m *config.MetricDefinition) error {
	bucket_value := m.IntBuckets.GetExponential()
	if bucket_value == nil {
		return nil
	}
	if bucket_value.StepMultiplierFloat != 0 {
		return fmt.Errorf("step_multiplier_float should not be set by customer")
	}

	bucket_value.StepMultiplierFloat = float32(bucket_value.StepMultiplier)
	return nil
}
