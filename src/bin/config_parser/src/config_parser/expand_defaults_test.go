package config_parser

import (
	"config"
	"fmt"
	"testing"
)

func TestExpandEventVectorBufferMax(t *testing.T) {
	rep := config.ReportDefinition{
		LocalAggregationProcedure: config.ReportDefinition_SELECT_FIRST,
		EventVectorBufferMax:      0,
	}
	expandDefaultsForReport(&rep)
	if rep.EventVectorBufferMax != 1 {
		t.Errorf("Failed to set EventVectorBufferMax to default")
	}

	rep = config.ReportDefinition{
		LocalAggregationProcedure: config.ReportDefinition_SELECT_FIRST,
		EventVectorBufferMax:      100,
	}
	expandDefaultsForReport(&rep)
	if rep.EventVectorBufferMax != 100 {
		t.Errorf("Overrode EventVectorBufferMax from the value set in the registry")
	}

	rep = config.ReportDefinition{
		LocalAggregationProcedure: config.ReportDefinition_AT_LEAST_ONCE,
		EventVectorBufferMax:      0,
	}
	expandDefaultsForReport(&rep)
	if rep.EventVectorBufferMax == 1 {
		t.Errorf("Expanded EventVectorBufferMax default for non SELECT_FIRST procedure")
	}
}

func TestExpandIntegerBucketStepMultiplierFloatForMetric(t *testing.T) {
	metric := config.MetricDefinition{
		MetricType: config.MetricDefinition_INTEGER_HISTOGRAM,
		IntBuckets: &config.IntegerBuckets{
			Buckets: &config.IntegerBuckets_Exponential{
				Exponential: &config.ExponentialIntegerBuckets{
					Floor:          0,
					NumBuckets:     0,
					InitialStep:    10,
					StepMultiplier: 2,
				},
			},
		},
	}

	expandDefaultsForIntegerHistogramMetric(&metric)
	if metric.IntBuckets.GetExponential().StepMultiplierFloat != 2.0 {
		t.Errorf("Failed to set step_multiplier_float to the value of step_multiplier for Integer Histogram metric")
	}
}

func TestExpandIntegerBucketStepMultiplierFloatForMetricWithStepMultiplierFloatSet(t *testing.T) {
	metric := config.MetricDefinition{
		MetricType: config.MetricDefinition_INTEGER_HISTOGRAM,
		IntBuckets: &config.IntegerBuckets{
			Buckets: &config.IntegerBuckets_Exponential{
				Exponential: &config.ExponentialIntegerBuckets{
					Floor:               0,
					NumBuckets:          0,
					InitialStep:         10,
					StepMultiplier:      2,
					StepMultiplierFloat: 2.0,
				},
			},
		},
	}

	err := expandDefaultsForIntegerHistogramMetric(&metric)
	if err == nil {
		t.Errorf("Expanding exponential integer buckets for Integer Histogram metric with step_multiplier_float set should return an error")
	}
}

func TestExpandIntegerBucketStepMultiplierFloatForIntegerHistogramsReportWithStepMultiplierFloatSet(t *testing.T) {
	integerMetric := config.MetricDefinition{
		MetricType: config.MetricDefinition_INTEGER,
	}

	uniqueDeviceHistogramReport := config.ReportDefinition{
		ReportType: config.ReportDefinition_UNIQUE_DEVICE_HISTOGRAMS,
		IntBuckets: &config.IntegerBuckets{
			Buckets: &config.IntegerBuckets_Exponential{
				Exponential: &config.ExponentialIntegerBuckets{
					Floor:               0,
					NumBuckets:          0,
					InitialStep:         10,
					StepMultiplier:      2,
					StepMultiplierFloat: 2.0,
				},
			},
		},
	}

	err := expandDefaultsForIntegerHistogramReport(&uniqueDeviceHistogramReport, &integerMetric)
	if err == nil {
		t.Errorf("Expanding exponential integer buckets for Integer Histogram report with step_multiplier_float set should return an error")
	}
}

// Tests that step_multiplier_float is not expanded for fleetwide histogram report from integer histogram metric.
func TestSkipExpandIntegerBucketStepMultiplierFloatForFleetwideHistogramsReport(t *testing.T) {
	intHistogramMetric := config.MetricDefinition{
		MetricType: config.MetricDefinition_INTEGER_HISTOGRAM,
		IntBuckets: &config.IntegerBuckets{
			Buckets: &config.IntegerBuckets_Exponential{
				Exponential: &config.ExponentialIntegerBuckets{
					Floor:          0,
					NumBuckets:     0,
					InitialStep:    10,
					StepMultiplier: 2,
				},
			},
		},
	}

	noBucketFleetwideHistogramReport := config.ReportDefinition{
		ReportType: config.ReportDefinition_FLEETWIDE_HISTOGRAMS,
	}

	expandDefaultsForIntegerHistogramReport(&noBucketFleetwideHistogramReport, &intHistogramMetric)
	if noBucketFleetwideHistogramReport.IntBuckets != nil {
		t.Errorf("IntBucket should not be redefined in Fleetwide Histogram report for Integer Histogram metric")
	}
}

var expandDefaultsForIntegerHistogramReportTest = []struct {
	report config.ReportDefinition_ReportType
	metric config.MetricDefinition_MetricType
}{
	{report: config.ReportDefinition_FLEETWIDE_HISTOGRAMS, metric: config.MetricDefinition_INTEGER},
	{report: config.ReportDefinition_HOURLY_VALUE_HISTOGRAMS, metric: config.MetricDefinition_INTEGER},
	{report: config.ReportDefinition_UNIQUE_DEVICE_HISTOGRAMS, metric: config.MetricDefinition_INTEGER},
}

func TestExpandDefaultsForIntegerHistogramReport(t *testing.T) {
	for _, tt := range expandDefaultsForIntegerHistogramReportTest {
		t.Run(fmt.Sprintf("ReportType_%v, MetricType_%v", tt.report, tt.metric), func(t *testing.T) {
			report := config.ReportDefinition{
				ReportType: tt.report,
				IntBuckets: &config.IntegerBuckets{
					Buckets: &config.IntegerBuckets_Exponential{
						Exponential: &config.ExponentialIntegerBuckets{
							Floor:          0,
							NumBuckets:     0,
							InitialStep:    10,
							StepMultiplier: 2,
						},
					},
				},
			}
			metric := config.MetricDefinition{
				MetricType: tt.metric,
			}

			expandDefaultsForIntegerHistogramReport(&report, &metric)
			if report.IntBuckets.GetExponential().StepMultiplierFloat != 2.0 {
				t.Errorf("Failed to set step_multiplier_float to the value of step_multiplier for Integer Histogram report")
			}
		})
	}
}

var expandDefaultsTest = []struct {
	ty     config.ReportDefinition_ReportType
	policy config.SystemProfileSelectionPolicy
}{
	{ty: config.ReportDefinition_FLEETWIDE_OCCURRENCE_COUNTS, policy: config.SystemProfileSelectionPolicy_REPORT_ALL},
	{ty: config.ReportDefinition_FLEETWIDE_HISTOGRAMS, policy: config.SystemProfileSelectionPolicy_REPORT_ALL},
	{ty: config.ReportDefinition_FLEETWIDE_MEANS, policy: config.SystemProfileSelectionPolicy_REPORT_ALL},
	{ty: config.ReportDefinition_UNIQUE_DEVICE_NUMERIC_STATS, policy: config.SystemProfileSelectionPolicy_REPORT_ALL},
	{ty: config.ReportDefinition_HOURLY_VALUE_NUMERIC_STATS, policy: config.SystemProfileSelectionPolicy_REPORT_ALL},
	{ty: config.ReportDefinition_STRING_COUNTS, policy: config.SystemProfileSelectionPolicy_REPORT_ALL},

	{ty: config.ReportDefinition_UNIQUE_DEVICE_COUNTS, policy: config.SystemProfileSelectionPolicy_SYSTEM_PROFILE_SELECTION_POLICY_UNSET},
	{ty: config.ReportDefinition_UNIQUE_DEVICE_HISTOGRAMS, policy: config.SystemProfileSelectionPolicy_SYSTEM_PROFILE_SELECTION_POLICY_UNSET},
	{ty: config.ReportDefinition_HOURLY_VALUE_HISTOGRAMS, policy: config.SystemProfileSelectionPolicy_SYSTEM_PROFILE_SELECTION_POLICY_UNSET},
}

func TestExpandSystemProfileSelectionDefaults(t *testing.T) {
	for _, tt := range expandDefaultsTest {
		t.Run(fmt.Sprintf("ReportType_%v", tt.ty), func(t *testing.T) {
			rep := config.ReportDefinition{
				ReportType:             tt.ty,
				SystemProfileSelection: config.SystemProfileSelectionPolicy_SYSTEM_PROFILE_SELECTION_POLICY_UNSET,
			}

			expandDefaultsForReport(&rep)

			if rep.SystemProfileSelection != tt.policy {
				t.Errorf("saw unexpected SystemProfileSelection, got %v, expected %v", rep.SystemProfileSelection, tt.policy)
			}
		})
	}
}

func TestNoOverrideSystemProfileSelectionDefaults(t *testing.T) {
	for _, tt := range expandDefaultsTest {
		t.Run(fmt.Sprintf("ReportType_%v", tt.ty), func(t *testing.T) {
			rep := config.ReportDefinition{
				ReportType:             tt.ty,
				SystemProfileSelection: config.SystemProfileSelectionPolicy_SELECT_FIRST,
			}
			if tt.policy == rep.SystemProfileSelection {
				rep.SystemProfileSelection = config.SystemProfileSelectionPolicy_SELECT_LAST
			}

			expandDefaultsForReport(&rep)

			if rep.SystemProfileSelection == tt.policy {
				t.Errorf("explicitly set SystemProfileSelection got overridden")
			}
		})
	}

}

func TestExpandDefaultsForStructMetricNameAndId(t *testing.T) {
	fieldName := "field_name"
	r := &config.ReportDefinition{
		Aggregates: []*config.ReportDefinition_AggregateDefinition{
			&config.ReportDefinition_AggregateDefinition{
				// Neither For nor Name should be changed.
				// ForId should be set
				Id:   1,
				Name: "some_name",
				For:  fieldName,
			},
			&config.ReportDefinition_AggregateDefinition{
				// Name should be copied to For
				// ForId should be set
				Id:   2,
				Name: fieldName,
			},
			&config.ReportDefinition_AggregateDefinition{
				// For should be copied to Name
				// ForId should be set
				Id:  3,
				For: fieldName,
			},
			&config.ReportDefinition_AggregateDefinition{
				// ForId should remain unset
				Id: 4,
			},
			&config.ReportDefinition_AggregateDefinition{
				// ForId should remain unset
				Id:  5,
				For: "unknown_field",
			},
		},
	}

	id := uint32(75)
	m := &config.MetricDefinition{
		StructFields: []*config.MetricDefinition_StructField{
			&config.MetricDefinition_StructField{
				Name: fieldName,
				Id:   id,
			},
		},

		Reports: []*config.ReportDefinition{r},
	}

	expandDefaultsForStructMetric(m)

	// For and Name were set. Neither should be changed and ForId should be set.
	aggregate := r.Aggregates[0]
	if aggregate.For != fieldName {
		t.Errorf("For was unexpectedly changed. Expected %v but got %v", fieldName, aggregate.For)
	}
	if aggregate.Name != "some_name" {
		t.Errorf("Name was unexpectedly changed. Expected \"some_name\" but got %v", aggregate.Name)
	}
	if aggregate.ForId != id {
		t.Errorf("Expected id %v but got %v", id, aggregate.ForId)
	}

	// Name was set. For should be copied from Name and ForId should be set.
	aggregate = r.Aggregates[1]
	if aggregate.For != fieldName {
		t.Errorf("For was not copied from Name. Expected %v but got %v", fieldName, aggregate.For)
	}
	if aggregate.Name != fieldName {
		t.Errorf("Name was unexpectedly changed. Expected %v but got %v", fieldName, aggregate.Name)
	}
	if aggregate.ForId != id {
		t.Errorf("Expected id %v but got %v", id, aggregate.ForId)
	}

	// For was set. Name should be copied from For and ForId should be set.
	aggregate = r.Aggregates[2]
	if aggregate.For != fieldName {
		t.Errorf("For was not copied from Name. Expected %v but got %v", fieldName, aggregate.For)
	}
	if aggregate.Name != fieldName {
		t.Errorf("Name was unexpectedly changed. Expected %v but got %v", fieldName, aggregate.Name)
	}
	if aggregate.ForId != id {
		t.Errorf("Expected id %v but got %v", id, aggregate.ForId)
	}

	// Neither Name not For was set. ForId should remain unset.
	aggregate = r.Aggregates[3]
	if aggregate.ForId != 0 {
		t.Errorf("Expected id to remain unset but got %v", aggregate.ForId)
	}

	// For was set to non-existent field. ForId should remain unset.
	aggregate = r.Aggregates[4]
	if aggregate.ForId != 0 {
		t.Errorf("Expected id to remain unset but got %v", aggregate.ForId)
	}
}

func TestExpandDefaultsForStructMetricStringCandidateFile(t *testing.T) {
	fieldName := "field_name"
	fileNameMetric := "some_file.txt"
	fileNameReport := "some_file_for_report.txt"
	r := &config.ReportDefinition{
		Aggregates: []*config.ReportDefinition_AggregateDefinition{
			// When no string_candidate_file is specified on aggregate, it should be
			// copied from the field.
			&config.ReportDefinition_AggregateDefinition{
				For: fieldName,
			},
			// When a string_candidate_file is specified on the aggregate it should
			// not be changed.
			&config.ReportDefinition_AggregateDefinition{
				For:                 fieldName,
				StringCandidateFile: fileNameReport,
			},
		},
	}

	m := &config.MetricDefinition{
		StructFields: []*config.MetricDefinition_StructField{
			&config.MetricDefinition_StructField{
				Name:                fieldName,
				Id:                  1,
				Type:                config.MetricDefinition_StructField_STRING,
				StringCandidateFile: fileNameMetric,
			},
		},

		Reports: []*config.ReportDefinition{r},
	}

	expandDefaultsForStructMetric(m)

	aggregate := r.Aggregates[0]
	if aggregate.StringCandidateFile != fileNameMetric {
		t.Error("Expected string_candidate_file to have been copied from metric field.")
	}

	aggregate = r.Aggregates[1]
	if aggregate.StringCandidateFile != fileNameReport {
		t.Error("Expected string_candidate_file to not have been copied from metric field.")
	}
}
