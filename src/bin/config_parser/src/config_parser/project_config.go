// Copyright 2017 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file contains the representation for the configuration of a cobalt
// project (See projectConfig) and a way to parse that configuration information
// from a yaml string.

package config_parser

import (
	"config"
	"fmt"
	"yamlpb"
)

// Represents the configuration of a single project. If `IsDeleted` is true,
// the object represents a deleted customer and only should have a CustomerId
// also be set.
type ProjectConfigData struct {
	CustomerName                  string
	CustomerId                    uint32
	ProjectName                   string
	ProjectId                     uint32
	Contact                       string
	AppPackageIdentifier          string
	CustomerExperimentsNamespaces []interface{}
	ProjectExperimentsNamespaces  []interface{}
	DeletedProjectIds             []uint32
	ProjectConfigFile             *config.ProjectConfigFile
	IsDeletedCustomer             bool
}

// Parse the configuration for one project from the yaml string provided into
// the ProjectConfigFile field in ProjectConfigData.
func parseProjectConfigData(y string, c *ProjectConfigData) (err error) {
	c.ProjectConfigFile = &config.ProjectConfigFile{}
	if err := yamlpb.UnmarshalString(y, c.ProjectConfigFile); err != nil {
		return fmt.Errorf("Error while parsing yaml: %v", err)
	}

	for _, e := range c.ProjectConfigFile.MetricDefinitions {
		e.CustomerId = c.CustomerId
		e.ProjectId = c.ProjectId
		if e.Id == 0 {
			return fmt.Errorf("Metric named '%v' does not have a metric id specified.", e.MetricName)
		}

		for _, r := range e.Reports {
			if r.Id == 0 {
				return fmt.Errorf("Report named '%v' does not have a report id specified.", r.ReportName)
			}
		}
	}

	return nil
}
