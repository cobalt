package config_parser

func PopulatePrivacyParams(configs []ProjectConfigData) error {
	// Reports that use the Poisson encoding scheme must manually specify privacy
	// encoding parameters.
	//
	// Note, if you're changing this function then the DevicePrivacyDependencySet enum likely needs
	// to be updated.
	//
	// TODO(https://fxbug.dev/278932979): update this comment once Poisson encoding parameters are
	// populated by the registry parser.
	return nil
}
