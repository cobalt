package config_validator

import (
	"config"
	"fmt"
	"math"
	"testing"
	"time"
)

////////////////////////////////////////////////////////////////////////////////
// Tests concerning sets of metrics.
////////////////////////////////////////////////////////////////////////////////

// Test that repeated ids are rejected.
func TestValidateUniqueMetricId(t *testing.T) {
	m1 := makeSomeValidMetric()
	m1.MetricName = "name1"
	m1.Id = 2

	m2 := makeSomeValidMetric()
	m2.MetricName = "name2"
	m2.Id = 2

	metrics := []*config.MetricDefinition{m1, m2}

	if err := validateConfiguredMetricDefinitions(metrics); err == nil {
		t.Error("Accepted metric definitions with identical ids.")
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tests for all/most metric types.
////////////////////////////////////////////////////////////////////////////////

// Test that repeated names are rejected.
func TestValidateUniqueMetricName(t *testing.T) {
	m1 := makeSomeValidMetric()
	m1.MetricName = "name"
	m1.Id = 2

	m2 := makeSomeValidMetric()
	m2.MetricName = "name"
	m2.Id = 3

	metrics := []*config.MetricDefinition{m1, m2}
	if err := validateConfiguredMetricDefinitions(metrics); err == nil {
		t.Error("Accepted metric definitions with identical names.")
	}
}

// Test that all metrics accept 0 or more dimensions.
func TestValidMetricWithNoDimensions(t *testing.T) {
	for _, mt := range metricTypesExcept() {
		m := makeValidMetric(mt)
		m.MetricDimensions = nil
		if err := validateMetricDefinition(m); err != nil {
			t.Errorf("Rejected valid %v metric with no dimension: %v", m.String(), err)
		}
	}

	for _, mt := range metricTypesExcept() {
		m := makeValidMetric(mt)
		addDimensions(m, 2)
		if err := validateMetricDefinition(m); err != nil {
			t.Errorf("Rejected valid %v metric with 2+ dimensions: %v", m.String(), err)
		}
	}
}

// Test that invalid names are rejected.
func TestValidateMetricInvalidMetricName(t *testing.T) {
	m := makeSomeValidMetric()
	m.MetricName = "_invalid_name"

	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted metric definition with invalid name.")
	}
}

// Test that metric id 0 is not accepted.
func TestValidateZeroMetricId(t *testing.T) {
	m := makeSomeValidMetric()
	m.Id = 0

	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted metric definition with 0 id.")
	}
}

// Test that we do not accept a metric with type UNSET.
func TestValidateUnsetMetricType(t *testing.T) {
	m := makeValidOccurrenceMetric()
	m.MetricType = config.MetricDefinition_UNSET

	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted metric definition with unset metric type.")
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tests concerning metadata.
////////////////////////////////////////////////////////////////////////////////

// Test that meta_data must be set.
func TestValidatePartsNoMetadata(t *testing.T) {
	m := makeSomeValidMetric()
	m.MetaData = nil

	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted metric definition with no meta_data set.")
	}
}

func TestValidateMetadataNoExpirationDate(t *testing.T) {
	m := makeValidMetadata()
	m.ExpirationDate = ""

	if err := validateMetadata(m); err == nil {
		t.Error("Accepted metadata with no expiration date.")
	}
}

func TestValidateMetadataInvalidExpirationDate(t *testing.T) {
	m := makeValidMetadata()
	m.ExpirationDate = "abcd"

	if err := validateMetadata(m); err == nil {
		t.Error("Accepted invalid expiration date")
	}
}

func TestValidateMetadataExpirationDateTooFar(t *testing.T) {
	m := makeValidMetadata()
	m.ExpirationDate = time.Now().AddDate(1, 0, 2).Format(dateFormat)

	if err := validateMetadata(m); err == nil {
		t.Errorf("Accepted expiration date more than 1 year out: %v", m.ExpirationDate)
	}
}

func TestValidateMetadataExpirationDateInPast(t *testing.T) {
	m := makeValidMetadata()
	m.ExpirationDate = "2010/01/01"

	if err := validateMetadata(m); err != nil {
		t.Errorf("Rejected expiration date in the past: %v", err)
	}
}

func TestValidateMetadataReleaseStageNotSet(t *testing.T) {
	m := makeValidMetadata()
	m.MaxReleaseStage = config.ReleaseStage_RELEASE_STAGE_NOT_SET

	if err := validateMetadata(m); err == nil {
		t.Error("Accepted metadata with no max_release_stage set.")
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tests concerning event codes.
////////////////////////////////////////////////////////////////////////////////

func TestValidateMetricDimensionsMaxEventCodesValid(t *testing.T) {
	m := makeSomeValidMetric()
	addDimensions(m, 2)
	m.MetricDimensions[0].MaxEventCode = 1
	m.MetricDimensions[1].MaxEventCode = 2

	if err := validateMetricDimensions(m); err != nil {
		t.Errorf("Rejected metric with valid max_event_codes: %v", err)
	}
}

func TestValidateMetricDimensionsNoMaxEventCodesSet(t *testing.T) {
	m := makeSomeValidMetric()
	addDimensions(m, 1)
	m.MetricDimensions[0].MaxEventCode = 0

	if err := validateMetricDimensions(m); err != nil {
		t.Errorf("Rejected metric without max_event_code set: %v", err)
	}
}

func TestValidateMetricDimensionsMaxEventCodeTooBig(t *testing.T) {
	m := makeSomeValidMetric()
	addDimensions(m, 2)
	m.MetricDimensions[0].MaxEventCode = 1
	m.MetricDimensions[1].MaxEventCode = math.MaxInt32 + 1

	if err := validateMetricDimensions(m); err == nil {
		t.Errorf("Accepted metric with invalid max_event_code value larger than max int32: %v", err)
	}
}

func TestValidateMetricDimensionsTooManyVariants(t *testing.T) {
	for _, mt := range metricTypesExcept() {
		m := makeValidMetric(mt)
		manyCodes := map[uint32]string{1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "F", 7: "G", 8: "H", 9: "I", 10: "J", 1055: "K"}
		m.MetricDimensions = []*config.MetricDefinition_MetricDimension{
			&config.MetricDefinition_MetricDimension{Dimension: "Dimension 1", EventCodes: manyCodes},
			&config.MetricDefinition_MetricDimension{Dimension: "Dimension 2", EventCodes: manyCodes},
			&config.MetricDefinition_MetricDimension{Dimension: "Dimension 3", EventCodes: manyCodes},
			&config.MetricDefinition_MetricDimension{Dimension: "Dimension 4", EventCodes: manyCodes},
		}

		if err := validateMetricDimensions(m); err != nil {
			t.Errorf("Rejected valid metric with many event codes: %v", err)
		}
	}
}

func TestValidateMetricDimensionsDimensionNames(t *testing.T) {
	m := makeSomeValidMetric()
	addDimensions(m, 2)
	m.MetricDimensions[1].Dimension = ""

	if err := validateMetricDimensions(m); err == nil {
		t.Error("Accepted invalid metric with an unnamed metric dimension")
	}

	m.MetricDimensions[1].Dimension = "Dimension 2"
	if err := validateMetricDimensions(m); err != nil {
		t.Error("Rejected valid metric with two distinctly named metric dimensions")
	}

	m.MetricDimensions[0].Dimension = "Dimension 1"
	m.MetricDimensions[1].Dimension = "Dimension 1"
	if err := validateMetricDimensions(m); err == nil {
		t.Error("Accepted invalid metric with two identically named metric dimensions")
	}
}

func TestValidateMetricDimensionsEventCodeAlias(t *testing.T) {
	m := makeSomeValidMetric()
	addDimensions(m, 1)
	m.MetricDimensions[0].EventCodes = map[uint32]string{
		0: "CodeName",
		1: "CodeName",
	}

	if err := validateMetricDimensions(m); err == nil {
		t.Error("Accepted invalid metric with duplicate event code names")
	}

	m.MetricDimensions[0].EventCodes = map[uint32]string{
		0: "CodeName",
	}
	m.MetricDimensions[0].EventCodeAliases = map[string]string{
		"Metric": "Alias",
	}

	if err := validateMetricDimensions(m); err == nil {
		t.Error("Accepted invalid metric with an invalid alias")
	}

	m.MetricDimensions[0].EventCodeAliases = map[string]string{
		"Alias": "CodeName",
	}
	if err := validateMetricDimensions(m); err == nil {
		t.Error("Accepted invalid metric with an alias in the wrong order")
	}

	m.MetricDimensions[0].EventCodeAliases = map[string]string{
		"CodeName": "Alias",
	}
	if err := validateMetricDimensions(m); err != nil {
		t.Errorf("Rejected valid metric: %v", err)
	}

	m.MetricDimensions[0].EventCodes[1] = "CodeName2"
	m.MetricDimensions[0].EventCodeAliases = map[string]string{
		"CodeName": "CodeName2",
	}
	if err := validateMetricDimensions(m); err == nil {
		t.Error("Accepted invalid metric that maps to an existing event code")
	}
}

func TestValidateEventCodesIndexTooBig(t *testing.T) {
	m := makeSomeValidMetric()
	addDimensions(m, 1)
	m.MetricDimensions[0].EventCodes = map[uint32]string{
		1:                 "hello_world",
		math.MaxInt32 + 1: "blah",
	}

	if err := validateMetricDimensions(m); err == nil {
		t.Error("Accepted event type with index larger than max int32")
	}
}

func TestValidateEventCodesIndexLargerThanMax(t *testing.T) {
	m := makeSomeValidMetric()
	addDimensions(m, 1)
	m.MetricDimensions[0].MaxEventCode = 100
	m.MetricDimensions[0].EventCodes = map[uint32]string{
		1:   "hello_world",
		101: "blah",
	}

	if err := validateMetricDimensions(m); err == nil {
		t.Error("Accepted event type with index larger than max_event_code.")
	}
}

func TestAllowNoEventCodesWhenMaxEventCodeIsSet(t *testing.T) {
	m := makeSomeValidMetric()
	addDimensions(m, 1)
	m.MetricDimensions[0].EventCodes = nil

	if err := validateMetricDimensions(m); err == nil {
		t.Error("Accepted metric without max_event_code and without any event_codes.")
	}

	m.MetricDimensions[0].MaxEventCode = 100

	if err := validateMetricDimensions(m); err != nil {
		t.Error("Did not accept metric with max_event_code set and no event codes defined.")
	}
}

func TestValidateEventCodesNoEventCodes(t *testing.T) {
	m := makeSomeValidMetric()
	addDimensions(m, 1)
	m.MetricDimensions[0].EventCodes = map[uint32]string{}

	if err := validateMetricDimensions(m); err == nil {
		t.Error("Accepted metric with no event types.")
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tests concerning metric units.
////////////////////////////////////////////////////////////////////////////////

func TestMetricUnitsForIntegerAndIntegerHistogramOnly(t *testing.T) {
	for _, mt := range metricTypesExcept(config.MetricDefinition_INTEGER, config.MetricDefinition_INTEGER_HISTOGRAM) {
		m := makeValidMetric(mt)
		m.MetricUnits = config.MetricUnits_NANOSECONDS
		m.MetricUnitsOther = ""
		if err := validateMetricDefinition(m); err == nil {
			t.Errorf("Accepted %v metric with metric_units set", mt.String())
		}

		m.MetricUnits = config.MetricUnits_METRIC_UNITS_OTHER
		m.MetricUnitsOther = "hello"
		if err := validateMetricDefinition(m); err == nil {
			t.Errorf("Accepted %v metric with metric_units_other set", mt.String())
		}
	}

}

func TestMetricUnitsForIntegers(t *testing.T) {
	m := makeValidIntegerMetric()
	m.MetricUnits = config.MetricUnits_METRIC_UNITS_OTHER
	m.MetricUnitsOther = "hello"
	if err := validateMetricDefinition(m); err != nil {
		t.Errorf("Rejected INTEGER metric with metric_units_other set: %v", err)
	}

	m.MetricUnits = config.MetricUnits_NANOSECONDS
	m.MetricUnitsOther = ""
	if err := validateMetricDefinition(m); err != nil {
		t.Errorf("Rejected INTEGER metric with metric_units set: %v", err)
	}

	m.MetricUnits = config.MetricUnits_NANOSECONDS
	m.MetricUnitsOther = "hello"
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted INTEGER metric with both metric_units and metric_units_other set.")
	}

	m.MetricUnits = config.MetricUnits_METRIC_UNITS_OTHER
	m.MetricUnitsOther = ""
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted INTEGER metric with neither metric_units nor metric_units_other set.")
	}
}

func TestMetricUnitsForIntegerHistograms(t *testing.T) {
	m := makeValidIntegerHistogramMetric()
	m.MetricUnits = config.MetricUnits_METRIC_UNITS_OTHER
	m.MetricUnitsOther = "hello"
	if err := validateMetricDefinition(m); err != nil {
		t.Errorf("Rejected INTEGER_HISTOGRAM metric with metric_units_other set: %v", err)
	}

	m.MetricUnits = config.MetricUnits_NANOSECONDS
	m.MetricUnitsOther = ""
	if err := validateMetricDefinition(m); err != nil {
		t.Errorf("Rejected INTEGER_HISTOGRAM metric with metric_units set: %v", err)
	}

	m.MetricUnits = config.MetricUnits_NANOSECONDS
	m.MetricUnitsOther = "hello"
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted INTEGER_HISTOGRAM metric with both metric_units and metric_units_other set.")
	}

	m.MetricUnits = config.MetricUnits_METRIC_UNITS_OTHER
	m.MetricUnitsOther = ""
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted INTEGER_HISTOGRAM metric with neither metric_units nor metric_units_other set.")
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tests concerning metric time zone policy.
////////////////////////////////////////////////////////////////////////////////

func TestValidateTimeZonePolicyOtherTimeZoneNotRequired(t *testing.T) {
	m := makeValidMetric(config.MetricDefinition_OCCURRENCE)
	m.TimeZonePolicy = config.MetricDefinition_UTC
	if err := validateMetricDefinition(m); err != nil {
		t.Error("Rejected metric with valid UTC TimeZonePolicy")
	}
	m.OtherTimeZone = "America/Los_Angeles"
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted metric with UTC TimeZonePolicy and with other_time_zone set")
	}

	m.TimeZonePolicy = config.MetricDefinition_LOCAL
	m.OtherTimeZone = ""
	if err := validateMetricDefinition(m); err != nil {
		t.Error("Rejected metric with valid LOCAL TimeZonePolicy")
	}
	m.OtherTimeZone = "America/Los_Angeles"
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted metric with LOCAL TimeZonePolicy and with other_time_zone set")
	}
}

func TestValidateTimeZonePolicyOtherTimeZoneRequired(t *testing.T) {
	m := makeValidMetric(config.MetricDefinition_OCCURRENCE)
	m.TimeZonePolicy = config.MetricDefinition_OTHER_TIME_ZONE
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted metric with OTHER_TIME_ZONE TimeZonePolicy but no other_time_zone set")
	}

	m.OtherTimeZone = "PST"
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted metric with OTHER_TIME_ZONE TimeZone and invalid time zone identifier as other_time_zone")
	}

	// "Local" is accepted by `time.LoadLocation` but is not an IANA time zone identifier
	// and may not be accepted by all consumers of the `other_time_zone` field, so the config
	// validator should reject it.
	m.OtherTimeZone = "Local"
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted metric with OTHER_TIME_ZONE TimeZone and invalid time zone identifier `Local` as other_time_zone")
	}

	m.OtherTimeZone = "America/Los_Angeles"
	if err := validateMetricDefinition(m); err != nil {
		t.Error("Rejected metric with OTHER_TIME_ZONE TimeZonePolicy and valid other_time_zone time zone identifier")
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tests concerning INTEGER and INTEGER_HISTOGRAM metrics.
////////////////////////////////////////////////////////////////////////////////

// Test that int_buckets can only be set if the metric type is INTEGER_HISTOGRAM.
func TestValidateIntBucketsRequiredForIntegerHistogramsOnly(t *testing.T) {
	for _, mt := range metricTypesExcept(config.MetricDefinition_INTEGER_HISTOGRAM) {
		m := makeValidMetric(mt)
		m.IntBuckets = &config.IntegerBuckets{}
		if err := validateMetricDefinition(m); err == nil {
			t.Errorf("Accepted metric definition with type %s with int_buckets set.", mt)
		}
	}

	m := makeValidIntegerHistogramMetric()
	m.IntBuckets = nil
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted INTEGER_HISTOGRAM metric definition with int_buckets not set.")
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tests concerning STRING metrics.
////////////////////////////////////////////////////////////////////////////////

func TestStringMetrics(t *testing.T) {
	m := makeValidStringMetric()
	if err := validateMetricDefinition(m); err != nil {
		t.Errorf("Rejected valid STRING metric definition: %v", err)
	}

	m = makeValidStringMetric()
	m.StringCandidateFile = ""
	if err := validateMetricDefinition(m); err == nil {
		t.Error("Accepted STRING metric definition with string_candidate_file not set.")
	}
}

// Test that all metrics are required to have metric_semantics set.
func TestValidateMetricDefinitionSemantics(t *testing.T) {
	for _, mt := range metricTypesExcept() {
		m := makeValidMetric(mt)
		m.MetricSemantics = nil
		if err := validateMetricDefinition(m); err == nil {
			t.Errorf("Accepted %v metric with no metric_semantics not set.", m.MetricType.String())
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tests concerning metrics with reports
////////////////////////////////////////////////////////////////////////////////

func TestValidateMetricDefinitionMultipleValidReports(t *testing.T) {
	m := makeValidMetricWithMultipleValidReports()
	if err := validateMetricDefinition(m); err != nil {
		t.Errorf("Rejected valid metrics with valid reports.")
	}
}

func TestValidateMetricDefinitionMultipleReportsWithSameIds(t *testing.T) {
	m := makeSomeValidMetric()
	r1 := makeValidReportWithName("report_one_name")
	r2 := makeValidReportWithName("report_two_name")
	m.Reports = []*config.ReportDefinition{r1, r2}
	if err := validateMetricDefinition(m); err == nil {
		t.Errorf("Accepted metric with duplicate report ids")
	}
}

func TestValidateMetricDefinitionMultipleReportsWithSameNames(t *testing.T) {
	m := makeSomeValidMetric()
	r1 := makeValidReportWithName("report_one_name")
	r1.Id = 1
	r2 := makeValidReportWithName("report_one_name")
	r2.Id = 2
	m.Reports = []*config.ReportDefinition{r1, r2}
	if err := validateMetricDefinition(m); err == nil {
		t.Errorf("Accepted metric with duplicate report names")
	}
}

func TestValidateMetricDefinitionReportWithUnsetSystemProfileSelectionPolicy(t *testing.T) {
	m := makeValidOccurrenceMetric()
	r := makeValidReportWithType(config.ReportDefinition_UNIQUE_DEVICE_COUNTS)
	r.LocalAggregationPeriod = config.WindowSize_WINDOW_1_DAY
	r.LocalAggregationProcedure = config.ReportDefinition_AT_LEAST_ONCE
	r.PrivacyMechanism = config.ReportDefinition_DE_IDENTIFICATION
	r.SystemProfileSelection = config.SystemProfileSelectionPolicy_SYSTEM_PROFILE_SELECTION_POLICY_UNSET
	m.Reports = []*config.ReportDefinition{r}
	if err := validateMetricDefinition(m); err == nil {
		t.Errorf("Accepted metric with unset system profile selection when required to be manually set by report type")
	}
}

func TestValidateMetricDefinitionReportWithIncorrectSystemProfileSelectionPolicy(t *testing.T) {
	m := makeSomeValidMetric()
	r := makeValidReportWithType(config.ReportDefinition_FLEETWIDE_OCCURRENCE_COUNTS)
	r.SystemProfileSelection = config.SystemProfileSelectionPolicy_SELECT_LAST
	m.Reports = []*config.ReportDefinition{r}
	if err := validateMetricDefinition(m); err == nil {
		t.Errorf("Accepted metric with incorrectly set system profile seelction policy for report type (should be REPORT_ALL)")
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tests concerning STRUCT metrics.
////////////////////////////////////////////////////////////////////////////////

func TestStructMetric(t *testing.T) {
	m := makeValidStructMetric()
	if err := validateStruct(m); err != nil {
		t.Errorf("Rejected valid STRUCT metric with: %v", err)
	}

	m = makeValidStructMetric()
	m.StructFields[0].Id = 1
	m.StructFields[1].Id = 1
	if err := validateStruct(m); err == nil {
		t.Errorf("Accepted STRUCT metric with duplicate field ids")
	}

	m = makeValidStructMetric()
	m.StructFields[0].Name = "dup"
	m.StructFields[1].Name = "dup"
	if err := validateStruct(m); err == nil {
		t.Errorf("Accepted STRUCT metric with duplicate field names")
	}
}

func TestStructField(t *testing.T) {
	f := makeValidStructFieldAny()
	if err := validateStructField(f); err != nil {
		t.Errorf("Rejected valid STRUCT field with: %v", err)
	}

	f.Type = config.MetricDefinition_StructField_FIELD_TYPE_UNSPECIFIED
	if err := validateStructField(f); err == nil {
		t.Errorf("Accepted STRUCT field with unspecified type")
	}

	f = makeValidStructFieldAny()
	f.Name = ""
	if err := validateStructField(f); err == nil {
		t.Errorf("Accepted STRUCT field with no name specified")
	}

	f = makeValidStructFieldAny()
	f.Id = 0
	if err := validateStructField(f); err == nil {
		t.Errorf("Accepted STRUCT field with no id")
	}
}

func TestStructFieldEnum(t *testing.T) {
	f := makeValidStructFieldEnum()
	if err := validateStructField(f); err != nil {
		t.Errorf("Rejected valid STRUCT ENUM field with: %v", err)
	}

	f = makeValidStructFieldEnum()
	f.Enum = map[uint32]string{
		1: "One",
		2: "Two",
		3: "One",
	}
	if err := validateStructField(f); err == nil {
		t.Errorf("Accepted STRUCT ENUM field with duplicate enum name")
	}
}

func TestStructFieldString(t *testing.T) {
	f := makeValidStructFieldString()
	if err := validateStructField(f); err != nil {
		t.Errorf("Rejected valid STRUCT STRING field with: %v", err)
	}
}

func TestStructFieldBool(t *testing.T) {
	f := makeValidStructFieldBool()
	if err := validateStructField(f); err != nil {
		t.Errorf("Rejected valid STRUCT BOOL field with: %v", err)
	}
}

////////////////////////////////////////////////////////////////////////////////
// Following are utility functions to facilitate testing.
////////////////////////////////////////////////////////////////////////////////

// Allows generating a list of MetricTypes for which we can run tests.
func metricTypesExcept(remove ...config.MetricDefinition_MetricType) []config.MetricDefinition_MetricType {
	return metricTypesExceptList(remove)
}

// Allows generating a list of MetricTypes for which we can run tests.
func metricTypesExceptList(remove []config.MetricDefinition_MetricType) (s []config.MetricDefinition_MetricType) {
	types := map[config.MetricDefinition_MetricType]bool{}
	for t := range config.MetricDefinition_MetricType_name {
		types[config.MetricDefinition_MetricType(t)] = true
	}

	for _, r := range remove {
		delete(types, r)
	}
	delete(types, config.MetricDefinition_UNSET)

	for t, _ := range types {
		s = append(s, t)
	}

	return
}

func makeValidMetadata() *config.MetricDefinition_Metadata {
	return &config.MetricDefinition_Metadata{
		ExpirationDate:  time.Now().AddDate(1, 0, 0).Format(dateFormat),
		MaxReleaseStage: config.ReleaseStage_DEBUG,
	}
}

func addDimensions(m *config.MetricDefinition, numDim int) {
	for i := 0; i < numDim; i++ {
		m.MetricDimensions = append(m.MetricDimensions, &config.MetricDefinition_MetricDimension{
			Dimension:  fmt.Sprintf("Dimension %d", i),
			EventCodes: map[uint32]string{1: "hello_world"},
		})
	}
}

func makeValidBaseMetric(t config.MetricDefinition_MetricType) *config.MetricDefinition {
	metadata := makeValidMetadata()
	return &config.MetricDefinition{
		CustomerId: 0,
		ProjectId:  0,
		Id:         1,
		MetricName: "the_metric_name",
		MetaData:   metadata,
		MetricType: t,
		MetricSemantics: []config.MetricSemantics{
			config.MetricSemantics_METRIC_SEMANTICS_UNSPECIFIED,
		},
	}
}

func makeValidOccurrenceMetric() *config.MetricDefinition {
	return makeValidBaseMetric(config.MetricDefinition_OCCURRENCE)
}

func makeValidIntegerMetric() *config.MetricDefinition {
	m := makeValidBaseMetric(config.MetricDefinition_INTEGER)
	m.MetricUnits = config.MetricUnits_SECONDS
	return m
}

func makeValidIntegerHistogramMetric() *config.MetricDefinition {
	m := makeValidBaseMetric(config.MetricDefinition_INTEGER_HISTOGRAM)
	m.MetricUnits = config.MetricUnits_SECONDS
	m.IntBuckets = &config.IntegerBuckets{}
	return m
}

func makeValidStringMetric() *config.MetricDefinition {
	m := makeValidBaseMetric(config.MetricDefinition_STRING)
	m.StringCandidateFile = "some_file"
	return m
}

func makeValidStructMetric() *config.MetricDefinition {
	m := makeValidBaseMetric(config.MetricDefinition_STRUCT)
	m.StructFields = []*config.MetricDefinition_StructField{
		makeValidStructFieldEnum(),
		makeValidStructFieldString(),
		makeValidStructFieldBool(),
	}

	return m
}

func makeValidStructFieldAny() *config.MetricDefinition_StructField {
	return makeValidStructFieldEnum()
}

func makeValidStructFieldEnum() *config.MetricDefinition_StructField {
	return &config.MetricDefinition_StructField{
		Id:   1,
		Type: config.MetricDefinition_StructField_ENUM,
		Name: "the_enum_field",
		Enum: map[uint32]string{
			1: "First",
			2: "Second",
		},
	}
}

func makeValidStructFieldString() *config.MetricDefinition_StructField {
	return &config.MetricDefinition_StructField{
		Id:                  2,
		Type:                config.MetricDefinition_StructField_STRING,
		Name:                "the_string_field",
		StringCandidateFile: "some_file.txt",
	}
}

func makeValidStructFieldBool() *config.MetricDefinition_StructField {
	return &config.MetricDefinition_StructField{
		Id:   3,
		Type: config.MetricDefinition_StructField_BOOL,
		Name: "the_bool_field",
	}
}

func makeValidMetric(t config.MetricDefinition_MetricType) *config.MetricDefinition {
	switch t {
	case config.MetricDefinition_OCCURRENCE:
		return makeValidOccurrenceMetric()
	case config.MetricDefinition_INTEGER:
		return makeValidIntegerMetric()
	case config.MetricDefinition_INTEGER_HISTOGRAM:
		return makeValidIntegerHistogramMetric()
	case config.MetricDefinition_STRING:
		return makeValidStringMetric()
	}
	return makeValidOccurrenceMetric()
}

func makeSomeValidMetric() *config.MetricDefinition {
	return makeValidOccurrenceMetric()
}

func makeValidMetricWithMultipleValidReports() *config.MetricDefinition {
	m := makeSomeValidMetric()
	r1 := makeValidReportWithNameAndType("the_report_name", config.ReportDefinition_FLEETWIDE_OCCURRENCE_COUNTS)
	r2 := makeValidReportWithNameAndType("the_other_report", config.ReportDefinition_HOURLY_VALUE_NUMERIC_STATS)
	r2.Id = 20
	m.Reports = []*config.ReportDefinition{r1, r2}
	return m
}

// Test the makeValidMetric functions return valid metrics.
func TestMakeValidMetricsValid(t *testing.T) {
	for _, mt := range metricTypesExcept() {
		if err := validateMetricDefinition(makeValidMetric(mt)); err != nil {
			t.Errorf("Rejected valid %v metric: %v", mt.String(), err)
		}
	}
}

// Test the makeValidMetadata does return a valid metadata message.
func TestValidateMakeValidMetadata(t *testing.T) {
	m := makeValidMetadata()
	if err := validateMetadata(m); err != nil {
		t.Errorf("Rejected valid metadata: %v", err)
	}
}
