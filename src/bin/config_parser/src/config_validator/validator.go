// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package config_validator

import (
	"config"
	"config_parser"
	"errors"
	"flag"
	"fmt"
	"sort"
	"strings"
)

const (
	CobaltInternalCustomerId = 2147483647
	OtherInternalCustomerId  = 299999
)

var (
	validateExperimentNamespaces = flag.Bool("validate_experiment_namespaces", false, "Validate that all experiment reports have experiment namespaces set in the project or customer.")
)

// validationErrors is used to package up nested validation errors in a structured way
//
// The method validationErrors::err() converts this into an error type (or nil if no entries have been added to errors)
type validationErrors struct {
	ty     string
	errors map[string][]error
}

func (ve validationErrors) addError(key string, value error) {
	if value == nil {
		return
	}
	ve.errors[key] = append(ve.errors[key], value)
}

func (ve validationErrors) addErrorf(key string, format string, a ...interface{}) {
	ve.addError(key, fmt.Errorf(format, a...))
}

func (ve *validationErrors) Error() string {
	if ve == nil {
		return ""
	}

	projects := []string{}
	for project := range ve.errors {
		projects = append(projects, project)
	}
	sort.Strings(projects)
	result := []string{}
	for _, project := range projects {
		errs := ve.errors[project]
		if len(errs) == 1 {
			var innerErrs *validationErrors
			err := errs[0]
			if errors.As(err, &innerErrs) {
				// Indent the nested validationErrors
				errStr := strings.Join(strings.Split(err.Error(), "\n"), "\n  ")
				result = append(result, fmt.Sprintf("%s `%s`:\n  %v", ve.ty, project, errStr))
			} else {
				result = append(result, fmt.Sprintf("%s `%s`: %v", ve.ty, project, err.Error()))
			}
		} else {
			errStrs := []string{}
			for _, err := range errs {
				errStrs = append(errStrs, err.Error())
			}
			errStr := strings.Join(errStrs, "\n")
			errStr = strings.Join(strings.Split(errStr, "\n"), "\n  ")
			result = append(result, fmt.Sprintf("%s `%s`:\n  %v", ve.ty, project, errStr))
		}
	}
	return strings.Join(result, "\n")
}

func newValidationErrors(ty string) validationErrors {
	return validationErrors{
		ty:     ty,
		errors: map[string][]error{},
	}
}

func (ve validationErrors) err() error {
	if len(ve.errors) > 0 {
		return &ve
	}
	return nil
}

func ValidateProjectConfigDatas(configs []config_parser.ProjectConfigData) error {
	projectErrors := newValidationErrors("customer/project")
	deletedCustomerIds := []uint32{}

	for _, c := range configs {
		if c.IsDeletedCustomer {
			deletedCustomerIds = append(deletedCustomerIds, c.CustomerId)
		}
	}

	for _, c := range configs {
		if err := validateProjectConfigData(&c, deletedCustomerIds); err != nil {
			projectErrors.addError(fmt.Sprintf("%s/%s", c.CustomerName, c.ProjectName), err)
		}
	}

	return projectErrors.err()
}

func validateProjectConfigData(c *config_parser.ProjectConfigData, deletedCustomerIds []uint32) (err error) {
	if c.IsDeletedCustomer {
		// If a ProjectConfigData is for a deleted customer ID, no further
		// validation is necessary.
		return nil
	}

	for _, deletedCustomerId := range deletedCustomerIds {
		if c.CustomerId == deletedCustomerId {
			return fmt.Errorf("IDs from previously deleted customers are being reused, choose a new customer ID for: %v (%v)", c.CustomerName, c.CustomerId)
		}
	}

	hasExperimentNamespaces := len(c.CustomerExperimentsNamespaces) > 0 || len(c.ProjectExperimentsNamespaces) > 0

	if (c.CustomerId == CobaltInternalCustomerId || c.CustomerId == OtherInternalCustomerId) && hasExperimentNamespaces {
		return fmt.Errorf("Internal customer %d can not contain experiments", c.CustomerId)
	}

	if err = validateConfiguredMetricDefinitions(c.ProjectConfigFile.MetricDefinitions); err != nil {
		return err
	}

	if *validateExperimentNamespaces {
		if err = validateReportExperimentNamespaces(c.ProjectConfigFile.MetricDefinitions, hasExperimentNamespaces); err != nil {
			return err
		}
	}

	for _, deletedProjectId := range c.DeletedProjectIds {
		if c.ProjectId == deletedProjectId {
			return fmt.Errorf("IDs from previously deleted projects are being reused, choose a new project ID for: %v", c.ProjectId)
		}
	}

	if len(c.ProjectConfigFile.DeletedMetricIds) > 0 {
		if err = validateDeletedMetricIds(c.ProjectConfigFile.DeletedMetricIds, c.ProjectConfigFile.MetricDefinitions); err != nil {
			return err
		}
	}

	return nil
}

func validateDeletedMetricIds(deletedMetricIds []uint32, metrics []*config.MetricDefinition) (err error) {
	currentMetricIds := make(map[uint32]bool)
	for _, metric := range metrics {
		currentMetricIds[metric.Id] = true
	}

	reusedMetricIds := []uint32{}
	for _, deletedMetricId := range deletedMetricIds {
		if _, ok := currentMetricIds[deletedMetricId]; ok {
			reusedMetricIds = append(reusedMetricIds, deletedMetricId)
		}
	}

	if len(reusedMetricIds) > 0 {
		return fmt.Errorf("IDs from previously deleted metrics are being reused, choose new metric IDs for: %v", reusedMetricIds)
	}

	return nil
}

func validateReportExperimentNamespaces(metrics []*config.MetricDefinition, hasExperimentNamespaces bool) (err error) {
	for _, m := range metrics {
		for _, r := range m.Reports {
			if containsExperimentIds(r.GetSystemProfileField()) && !hasExperimentNamespaces {
				return fmt.Errorf("Experiment reports must have experiment namespaces set in project or customer config")
			}
		}
	}

	return nil
}
