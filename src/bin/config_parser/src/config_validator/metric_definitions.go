// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package config_validator

import (
	"config"
	"fmt"
	"math"
	"regexp"
	"strings"
	"time"
)

// This file contains logic to validate lists of MetricDefinition protos.

// Date format is yyyy/mm/dd. This is done by specifying in the desired format
// Mon Jan 2 15:04:05 MST 2006. See time module documentation.
const dateFormat = "2006/01/02"

// Valid names are 1-64 characters long. They have the same syntactic requirements
// as a C variable name.
var validNameRegexp = regexp.MustCompile("^[a-zA-Z][_a-zA-Z0-9]{1,65}$")

// Validate a list of MetricDefinitions.
func validateConfiguredMetricDefinitions(metrics []*config.MetricDefinition) (err error) {
	metricErrors := newValidationErrors("metric")

	metricIds := map[uint32]int{}
	metricNames := map[string]bool{}
	metricLookup := map[uint32]*config.MetricDefinition{}
	for i, metric := range metrics {
		if _, ok := metricIds[metric.Id]; ok {
			metricErrors.addError(fmt.Sprintf("Metric %d", metric.Id), fmt.Errorf("there are two metrics with id=%v", metric.Id))
			continue
		}
		metricIds[metric.Id] = i

		if _, ok := metricNames[metric.MetricName]; ok {
			metricErrors.addError(metric.MetricName, fmt.Errorf("there are two metrics with name=%v", metric.MetricName))
			continue
		}
		metricNames[metric.MetricName] = true

		metricLookup[metric.Id] = metric

		if err = validateMetricDefinition(metric); err != nil {
			metricErrors.addError(metric.MetricName, err)
		}

	}

	return metricErrors.err()
}

// Validate a single MetricDefinition.
func validateMetricDefinition(m *config.MetricDefinition) (err error) {
	validationErrors := newValidationErrors("field")
	if !validNameRegexp.MatchString(m.MetricName) {
		validationErrors.addError("metric_name", fmt.Errorf("metric names must match the regular expression '%v'", validNameRegexp))
	}

	if m.Id == 0 {
		validationErrors.addError("id", fmt.Errorf("metric ID of zero is not allowed. Please specify a positive metric ID"))
	}

	if m.MetricType == config.MetricDefinition_UNSET {
		validationErrors.addError("metric_type", fmt.Errorf("is not set"))
	}

	if m.MetaData == nil {
		validationErrors.addError("meta_data", fmt.Errorf("meta_data is not set, additionally, in meta_data: %v", validateMetadata(&config.MetricDefinition_Metadata{})))
	} else if err := validateMetadata(m.MetaData); err != nil {
		validationErrors.addError("meta_data", err)
	}

	if err := validateMetricUnits(m); err != nil {
		validationErrors.addError("metric_units", err)
	}

	if err := validateMetricTimeZone(m); err != nil {
		validationErrors.addError("time_zone_policy", err)
	}

	if m.IntBuckets != nil && m.MetricType != config.MetricDefinition_INTEGER_HISTOGRAM {
		validationErrors.addError("int_buckets", fmt.Errorf("int_buckets can only be set for metrics for metric type INTEGER_HISTOGRAM"))
	}

	if m.MetricSemantics == nil || len(m.MetricSemantics) == 0 {
		validationErrors.addError("metric_semantics", fmt.Errorf("metric_semantics is required for this metric"))
	}

	if err := validateMetricDefinitionForType(m); err != nil {
		validationErrors.addError("other", err)
	}

	if err := validateMetricDimensions(m); err != nil {
		validationErrors.addError("metric_dimensions", err)
	}

	err = validationErrors.err()
	if err != nil {
		return err
	}

	return validateReportDefinitions(m)
}

// Validate a single instance of Metadata.
func validateMetadata(m *config.MetricDefinition_Metadata) (err error) {
	errors := []string{}

	if len(m.ExpirationDate) == 0 {
		errors = append(errors, "expiration_date field is required")
	} else {
		var exp time.Time
		exp, err = time.ParseInLocation(dateFormat, m.ExpirationDate, time.UTC)
		if err != nil {
			errors = append(errors, fmt.Sprintf("expiration_date (%v) field is invalid, it must use the yyyy/mm/dd format", m.ExpirationDate))
		} else {
			// Date one year and one day from today.
			maxExp := time.Now().AddDate(1, 0, 0)
			if exp.After(maxExp) {
				errors = append(errors, fmt.Sprintf("expiration_date must be no later than %v", maxExp.Format(dateFormat)))
			}
		}
	}

	if m.MaxReleaseStage == config.ReleaseStage_RELEASE_STAGE_NOT_SET {
		errors = append(errors, "max_release_stage is required")
	}

	if len(errors) > 0 {
		return fmt.Errorf("%v", strings.Join(errors, ", "))
	} else {
		return nil
	}
}

// Validate the event_codes and max_event_code fields.
func validateMetricDimensions(m *config.MetricDefinition) error {
	if len(m.MetricDimensions) == 0 {
		// A metric definition is allowed to not have any metric dimensions.
		return nil
	}

	if len(m.MetricDimensions) > 5 {
		return fmt.Errorf("there can be at most 5 dimensions in metric_dimensions")
	}

	seenNames := make(map[string]bool)
	numOptions := 1
	for i, md := range m.MetricDimensions {
		if md.Dimension == "" {
			return fmt.Errorf("dimension %d has no 'dimension' name", i)
		}

		if seenNames[md.Dimension] {
			return fmt.Errorf("duplicate dimension name in metric_dimensions %s", md.Dimension)
		}
		seenNames[md.Dimension] = true

		name := md.Dimension

		seenCodeNames := make(map[string]bool)
		for _, codeName := range md.EventCodes {
			if seenCodeNames[codeName] {
				return fmt.Errorf("duplicate event_code name in metric_dimensions %s", name)
			}
			seenCodeNames[codeName] = true
		}

		for from, to := range md.EventCodeAliases {
			if !seenCodeNames[from] {
				if seenCodeNames[to] {
					return fmt.Errorf("unknown event_code %s for alias %s: %s. Did you mean %s: %s?", from, from, to, to, from)
				} else {
					return fmt.Errorf("unknown event_code_alias: %s: %s", from, to)
				}
			}
			if seenCodeNames[to] {
				return fmt.Errorf("cannot have event_code_alias %s, when that name already exists in event_codes", to)
			}
		}

		if len(md.EventCodes) == 0 && md.MaxEventCode == 0 {
			return fmt.Errorf("for metric dimension %v, you must either define max_event_code or explicitly define at least one event code", name)
		}
		if md.MaxEventCode > math.MaxInt32 {
			return fmt.Errorf("for metric dimension %v, max_event_code must be smaller than INT32.MAX", name)
		}
		if md.MaxEventCode != 0 {
			for j := range md.EventCodes {
				if j > md.MaxEventCode {
					return fmt.Errorf("event index %v in metric_dimension %v is greater than max_event_code %v", j, name, md.MaxEventCode)
				}
			}
			numOptions *= int(md.MaxEventCode + 1)
		} else {
			for j := range md.EventCodes {
				if j > math.MaxInt32 {
					return fmt.Errorf("event index %v in metric_dimension %v is is too large (greater than INT32.MAX)", j, name)
				}
			}
			numOptions *= len(md.EventCodes)
		}
	}

	return nil
}

func validateMetricUnits(m *config.MetricDefinition) error {
	hasMetricUnits := m.MetricUnits != config.MetricUnits_METRIC_UNITS_OTHER
	hasMetricUnitsOther := len(m.MetricUnitsOther) > 0
	if m.MetricType != config.MetricDefinition_INTEGER && m.MetricType != config.MetricDefinition_INTEGER_HISTOGRAM {
		if hasMetricUnits || hasMetricUnitsOther {
			return fmt.Errorf("metric_units and metric_units_other must be set only for INTEGER and INTEGER_HISTOGRAM metrics")
		}
		return nil
	}

	if hasMetricUnits == hasMetricUnitsOther {
		return fmt.Errorf("metrics of type INTEGER and INTEGER_HISTOGRAM must have exactly one of metric_units or metric_units_other specified")
	}
	return nil
}

func validateMetricTimeZone(m *config.MetricDefinition) error {
	if m.TimeZonePolicy == config.MetricDefinition_OTHER_TIME_ZONE {
		if m.OtherTimeZone == "" {
			return fmt.Errorf("metrics with TimeZonePolicy OTHER_TIME_ZONE must specify other_time_zone")
		}
		// `time.LoadLocation` accepts "Local" as a time zone identifier, but that is not an IANA identifier and may not
		// be accepted by all consumers of the `other_time_zone` field.
		_, err := time.LoadLocation(m.OtherTimeZone)
		if err != nil || m.OtherTimeZone == "Local" {
			return fmt.Errorf("other_time_zone is not a valid IANA time zone identifier (see https://iana.org/time-zones)")
		}
	} else {
		if m.OtherTimeZone != "" {
			return fmt.Errorf("only metrics with TimeZonePolicy OTHER_TIME_ZONE should specify other_time_zone")
		}
	}
	return nil
}

///////////////////////////////////////////////////////////////
// Validation for specific metric types:
///////////////////////////////////////////////////////////////

func validateMetricDefinitionForType(m *config.MetricDefinition) error {
	switch m.MetricType {
	case config.MetricDefinition_OCCURRENCE:
		return nil
	case config.MetricDefinition_INTEGER:
		return nil
	case config.MetricDefinition_INTEGER_HISTOGRAM:
		return validateIntegerHistogram(m)
	case config.MetricDefinition_STRING:
		return validateString(m)
	case config.MetricDefinition_STRUCT:
		return validateStruct(m)
	}

	return fmt.Errorf("Unknown MetricType: %v", m.MetricType)
}

func validateIntegerHistogram(m *config.MetricDefinition) error {
	if m.IntBuckets == nil {
		return fmt.Errorf("no int_buckets specified for metric of type INTEGER_HISTOGRAM")
	}

	// TODO(https://fxbug.dev/278917650): Validate bucket definition.

	for _, r := range m.Reports {
		if m.IntBuckets != nil && r.IntBuckets != nil {
			return fmt.Errorf("for report %q: both report and metric contain int_buckets stanzas, the stanza in %q will be ignored", r.ReportName, r.ReportName)
		}
	}

	return nil
}

func validateString(m *config.MetricDefinition) error {
	if m.StringCandidateFile == "" {
		return fmt.Errorf("no string_candidate_file specified for metric of type STRING")
	}

	return nil
}

func validateStruct(m *config.MetricDefinition) error {
	validationErrors := newValidationErrors(m.MetricName)

	if !isInStructMetricAllowList(m) {
		validationErrors.addErrorf("struct", "Project not in struct metrics allowlist.")
	}

	fieldNames := map[string]bool{}
	fieldIds := map[uint32]bool{}

	for _, f := range m.StructFields {
		validationErrors.addError("struct_fields", validateStructField(f))
		if fieldNames[f.Name] {
			validationErrors.addErrorf("struct_fields", "duplicate field name: %s", f.Name)
		}

		if fieldIds[f.Id] {
			validationErrors.addErrorf("struct_fields", "duplicate field ids for field %s", f.Name)
		}
		fieldNames[f.Name] = true
		fieldIds[f.Id] = true
	}

	return validationErrors.err()
}

var structMetricAllowList = []struct {
	cId uint32
	pId uint32
}{
	{cId: 0, pId: 0}, // Customer and project id unset for testing.
}

func isInStructMetricAllowList(m *config.MetricDefinition) bool {
	for _, allowed := range structMetricAllowList {
		if m.CustomerId == allowed.cId && m.ProjectId == allowed.pId {
			return true
		}
	}
	return false
}

func validateStructField(f *config.MetricDefinition_StructField) error {
	if len(f.Name) == 0 {
		return fmt.Errorf("no name specified for field %v", f.Id)
	}

	validationErrors := newValidationErrors(f.Name)
	if f.Id == 0 {
		validationErrors.addErrorf("id", "field ID must not be 0. Please specifiy a positive field ID.")
	}

	switch f.Type {
	case config.MetricDefinition_StructField_FIELD_TYPE_UNSPECIFIED:
		validationErrors.addErrorf("type", "is not set")
	case config.MetricDefinition_StructField_ENUM:
		validationErrors.addError("enum", validateStructFieldEnum(f))
	case config.MetricDefinition_StructField_STRING:
	case config.MetricDefinition_StructField_BOOL:
		break
	default:
		validationErrors.addErrorf("type", "is not supported")
	}

	return validationErrors.err()
}

func validateStructFieldEnum(f *config.MetricDefinition_StructField) error {
	if len(f.Enum) == 0 {
		return fmt.Errorf("no enum specified for field of type ENUM")
	}

	names := map[string]bool{}
	for _, name := range f.Enum {
		if names[name] {
			return fmt.Errorf("duplicate enum name \"%s\"", name)
		}
		names[name] = true
	}
	return nil
}
