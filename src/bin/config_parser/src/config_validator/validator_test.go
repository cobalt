package config_validator

import (
	"config"
	"config_parser"
	"fmt"
	"github.com/google/go-cmp/cmp"
	"testing"
)

func TestEmptyValidationErrors(t *testing.T) {
	if newValidationErrors("err").err() != nil {
		t.Errorf("Empty validation errors .err() should return nil")
	}
}

func TestValidationErrorsNil(t *testing.T) {
	errs := newValidationErrors("err")
	errs.addError("test", nil)
	if errs.err() != nil {
		t.Errorf("Validation errors to which only nil has been added .err() should return nil")
	}
}

func TestSingleValidationErrors(t *testing.T) {
	errs := newValidationErrors("err")
	errs.addError("an", fmt.Errorf("error"))
	expectedError := "err `an`: error"
	if diff := cmp.Diff(errs.err().Error(), expectedError); diff != "" {
		t.Errorf("Output error does not match expected error: %s", diff)
	}
}

func TestSingleValidationErrorf(t *testing.T) {
	errs := newValidationErrors("err")
	errs.addErrorf("an", "error: %v", 1)
	expectedError := "err `an`: error: 1"
	if diff := cmp.Diff(errs.err().Error(), expectedError); diff != "" {
		t.Errorf("Output error does not match expected error: %s", diff)
	}
}

func TestMultipleValidationErrors(t *testing.T) {
	errs := newValidationErrors("err")
	errs.addError("an", fmt.Errorf("error"))
	errs.addError("another", fmt.Errorf("error"))
	expectedError := "err `an`: error\nerr `another`: error"
	if diff := cmp.Diff(errs.err().Error(), expectedError); diff != "" {
		t.Errorf("Output error does not match expected error: %s", diff)
	}
}

func TestNestedValidationErrors(t *testing.T) {
	innerErr := newValidationErrors("inner")
	innerErr.addError("an", fmt.Errorf("error"))
	errs := newValidationErrors("err")
	errs.addError("nested", innerErr.err())
	expectedError := "err `nested`:\n  inner `an`: error"
	if diff := cmp.Diff(errs.err().Error(), expectedError); diff != "" {
		t.Errorf("Output error does not match expected error: %s", diff)
	}
}

func TestMultipleNestedValidationErrors(t *testing.T) {
	innerErr := newValidationErrors("inner")
	innerErr.addError("an", fmt.Errorf("error"))
	errs := newValidationErrors("err")
	errs.addError("nested2", innerErr.err())
	errs.addError("nested1", innerErr.err())
	expectedError := "err `nested1`:\n  inner `an`: error\nerr `nested2`:\n  inner `an`: error"
	if diff := cmp.Diff(errs.err().Error(), expectedError); diff != "" {
		t.Errorf("Output error does not match expected error: %s", diff)
	}
}

func TestMultipleErrorsForLevel(t *testing.T) {
	errs := newValidationErrors("err")
	errs.addError("an", fmt.Errorf("error"))
	errs.addError("an", fmt.Errorf("error"))
	expectedError := "err `an`:\n  error\n  error"
	if diff := cmp.Diff(errs.err().Error(), expectedError); diff != "" {
		t.Errorf("Output error does not match expected error: %s", diff)
	}
}

func TestValidateProjectConfigDatasWithDeletedCustomerIds(t *testing.T) {
	m := makeValidMetricWithMultipleValidReports()
	projectConfigFile := config.ProjectConfigFile{
		MetricDefinitions: []*config.MetricDefinition{m},
	}
	configs := []config_parser.ProjectConfigData{
		{
			CustomerName:      "customer5",
			CustomerId:        5,
			ProjectName:       "project3",
			ProjectId:         3,
			Contact:           "project3@customer5.com",
			ProjectConfigFile: &projectConfigFile,
		},
		{
			CustomerName:      "customer5",
			CustomerId:        5,
			ProjectName:       "project4",
			ProjectId:         4,
			Contact:           "project4@customer5.com",
			ProjectConfigFile: &projectConfigFile,
		},
		{
			CustomerId:        1,
			IsDeletedCustomer: true,
		},
		{
			CustomerId:        2,
			IsDeletedCustomer: true,
		},
	}
	err := ValidateProjectConfigDatas(configs)
	if err != nil {
		t.Errorf("Validation incorrectly rejected deleted customer IDs: %v", err)
	}
}

func TestValidateProjectConfigDatasWithReusedDeletedCustomerIds(t *testing.T) {
	m := makeValidMetricWithMultipleValidReports()
	projectConfigFile := config.ProjectConfigFile{
		MetricDefinitions: []*config.MetricDefinition{m},
	}
	configs := []config_parser.ProjectConfigData{
		{
			CustomerName:      "customer5",
			CustomerId:        5,
			ProjectName:       "project3",
			ProjectId:         3,
			Contact:           "project3@customer5.com",
			ProjectConfigFile: &projectConfigFile,
		},
		{
			CustomerName:      "customer5",
			CustomerId:        5,
			ProjectName:       "project4",
			ProjectId:         4,
			Contact:           "project4@customer5.com",
			ProjectConfigFile: &projectConfigFile,
		},
		{
			CustomerId:        1,
			IsDeletedCustomer: true,
		},
		{
			CustomerId:        5,
			IsDeletedCustomer: true,
		},
	}
	err := ValidateProjectConfigDatas(configs)
	if err == nil {
		t.Errorf("Validation did not catch expected error for reusing deleted customer IDs")
	}
}

func TestValidateProjectConfigDatasCustomerExperimentNamespaces(t *testing.T) {
	configs := []config_parser.ProjectConfigData{
		{
			CustomerName:                  "cobalt_internal",
			CustomerId:                    2147483647,
			ProjectName:                   "project3",
			ProjectId:                     3,
			Contact:                       "project3@customer5.com",
			CustomerExperimentsNamespaces: []interface{}{"customer_experiment_namespace"},
		},
	}
	err := ValidateProjectConfigDatas(configs)
	if err == nil {
		t.Errorf("Validation did not catch expected error for experiments in internal customers")
	}
}

func TestValidateProjectConfigDatasProjectExperimentNamespaces(t *testing.T) {
	configs := []config_parser.ProjectConfigData{
		{
			CustomerName:                 "cobalt_internal",
			CustomerId:                   2147483647,
			ProjectName:                  "project3",
			ProjectId:                    3,
			Contact:                      "project3@customer5.com",
			ProjectExperimentsNamespaces: []interface{}{"customer_experiment_namespace"},
		},
	}
	err := ValidateProjectConfigDatas(configs)
	if err == nil {
		t.Errorf("Validation did not catch expected error for experiments in internal projects")
	}
}

func TestValidateProjectConfigDatasSuccessWithCustomerExperimentNamespaces(t *testing.T) {
	*validateExperimentNamespaces = true
	m := makeSomeValidMetric()
	r := makeValidReport()
	r.SystemProfileField = []config.SystemProfileField{config.SystemProfileField_EXPERIMENT_IDS}
	r.ExperimentId = []int64{1}
	m.Reports = []*config.ReportDefinition{r}
	projectConfigFile := config.ProjectConfigFile{
		MetricDefinitions: []*config.MetricDefinition{m},
	}
	configs := []config_parser.ProjectConfigData{
		{
			CustomerName:                  "customer5",
			CustomerId:                    5,
			ProjectName:                   "project4",
			ProjectId:                     4,
			Contact:                       "project4@customer5.com",
			ProjectConfigFile:             &projectConfigFile,
			CustomerExperimentsNamespaces: []interface{}{"customer_experiment_namespace"},
		},
	}
	err := ValidateProjectConfigDatas(configs)
	if err != nil {
		t.Errorf("Validation incorrectly rejected report with customer experiment namespaces set")
	}
}

func TestValidateProjectConfigDatasSuccessWithProjectExperimentNamespaces(t *testing.T) {
	*validateExperimentNamespaces = true
	m := makeSomeValidMetric()
	r := makeValidReport()
	r.SystemProfileField = []config.SystemProfileField{config.SystemProfileField_EXPERIMENT_IDS}
	r.ExperimentId = []int64{1}
	m.Reports = []*config.ReportDefinition{r}
	projectConfigFile := config.ProjectConfigFile{
		MetricDefinitions: []*config.MetricDefinition{m},
	}
	configs := []config_parser.ProjectConfigData{
		{
			CustomerName:                 "customer5",
			CustomerId:                   5,
			ProjectName:                  "project4",
			ProjectId:                    4,
			Contact:                      "project4@customer5.com",
			ProjectConfigFile:            &projectConfigFile,
			ProjectExperimentsNamespaces: []interface{}{"project_experiment_namespace"},
		},
	}
	err := ValidateProjectConfigDatas(configs)
	if err != nil {
		t.Errorf("Validation incorrectly rejected report with project experiment namespaces set")
	}
}

func TestValidateProjectConfigDatasMissingExperimentNamespaces(t *testing.T) {
	*validateExperimentNamespaces = true
	m := makeSomeValidMetric()
	r := makeValidReport()
	r.SystemProfileField = []config.SystemProfileField{config.SystemProfileField_EXPERIMENT_IDS}
	r.ExperimentId = []int64{1}
	m.Reports = []*config.ReportDefinition{r}
	projectConfigFile := config.ProjectConfigFile{
		MetricDefinitions: []*config.MetricDefinition{m},
	}
	configs := []config_parser.ProjectConfigData{
		{
			CustomerName:      "customer5",
			CustomerId:        5,
			ProjectName:       "project4",
			ProjectId:         4,
			Contact:           "project4@customer5.com",
			ProjectConfigFile: &projectConfigFile,
		},
	}
	err := ValidateProjectConfigDatas(configs)
	if err == nil {
		t.Errorf("Validation did not catch expected error for experiment report with no experiment namespaces set")
	}
}

func TestValidateProjectConfigDatasDeletedProject(t *testing.T) {
	m := makeValidMetricWithMultipleValidReports()
	projectConfigFile := config.ProjectConfigFile{
		MetricDefinitions: []*config.MetricDefinition{m},
	}
	configs := []config_parser.ProjectConfigData{
		{
			CustomerName:      "customer5",
			CustomerId:        5,
			ProjectName:       "project3",
			ProjectId:         3,
			Contact:           "project3@customer5.com",
			DeletedProjectIds: []uint32{4, 5},
			ProjectConfigFile: &projectConfigFile,
		},
		{
			CustomerName:      "customer5",
			CustomerId:        5,
			ProjectName:       "project4",
			ProjectId:         4,
			Contact:           "project4@customer5.com",
			DeletedProjectIds: []uint32{4, 5},
			ProjectConfigFile: &projectConfigFile,
		},
	}
	err := ValidateProjectConfigDatas(configs)
	if err == nil {
		t.Errorf("Validation did not catch expected error for reusing deleted project ID")
	}
}

func TestValidateProjectConfigDatasDeletedMetric(t *testing.T) {
	m1 := makeValidMetricWithMultipleValidReports()
	m2 := makeSomeValidMetric()
	m2.Id = 2
	projectConfigFile := config.ProjectConfigFile{
		MetricDefinitions: []*config.MetricDefinition{m1, m2},
		DeletedMetricIds:  []uint32{2},
	}
	configs := []config_parser.ProjectConfigData{
		{
			CustomerName:      "customer5",
			CustomerId:        5,
			ProjectName:       "project3",
			ProjectId:         3,
			Contact:           "project3@customer5.com",
			ProjectConfigFile: &projectConfigFile,
		},
	}
	err := ValidateProjectConfigDatas(configs)
	if err == nil {
		t.Errorf("Validation did not catch expected error for reusing deleted metric ID")
	}
}
