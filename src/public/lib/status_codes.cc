// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/public/lib/status_codes.h"

namespace cobalt {

std::string StatusCodeToString(StatusCode code) {
  switch (code) {
    case StatusCode::OK:
      return "OK";
    case StatusCode::CANCELLED:
      return "CANCELLED";
    case StatusCode::UNKNOWN:
      return "UNKNOWN";
    case StatusCode::INVALID_ARGUMENT:
      return "INVALID_ARGUMENT";
    case StatusCode::DEADLINE_EXCEEDED:
      return "DEADLINE_EXCEEDED";
    case StatusCode::NOT_FOUND:
      return "NOT_FOUND";
    case StatusCode::ALREADY_EXISTS:
      return "ALREADY_EXISTS";
    case StatusCode::PERMISSION_DENIED:
      return "PERMISSION_DENIED";
    case StatusCode::RESOURCE_EXHAUSTED:
      return "RESOURCE_EXHAUSTED";
    case StatusCode::FAILED_PRECONDITION:
      return "FAILED_PRECONDITION";
    case StatusCode::ABORTED:
      return "ABORTED";
    case StatusCode::OUT_OF_RANGE:
      return "OUT_OF_RANGE";
    case StatusCode::UNIMPLEMENTED:
      return "UNIMPLEMENTED";
    case StatusCode::INTERNAL:
      return "INTERNAL";
    case StatusCode::UNAVAILABLE:
      return "UNAVAILABLE";
    case StatusCode::DATA_LOSS:
      return "DATA_LOSS";
    case StatusCode::UNAUTHENTICATED:
      return "UNAUTHENTICATED";
    default:
      return "UNKNOWN_ERROR";
  }
}

std::ostream& operator<<(std::ostream& os, StatusCode code) {
  return os << StatusCodeToString(code);
}

std::optional<StatusCode> ErrnoToStatusCode(int error_number) {
  switch (error_number) {
    case 0:
      return StatusCode::OK;

    case EINVAL:
      return StatusCode::INVALID_ARGUMENT;

    case ETIMEDOUT:
    case ETIME:
      return StatusCode::DEADLINE_EXCEEDED;

    case ENODEV:
    case ENOENT:
    case ENXIO:
    case ESRCH:
      return StatusCode::NOT_FOUND;

    case EEXIST:
    case EADDRNOTAVAIL:
    case EALREADY:
      return StatusCode::ALREADY_EXISTS;

    case EPERM:
    case EACCES:
    case EROFS:
      return StatusCode::PERMISSION_DENIED;

    case EMFILE:
    case ENFILE:
    case EMLINK:
      return StatusCode::RESOURCE_EXHAUSTED;

    case EFBIG:
    case EOVERFLOW:
    case ERANGE:
      return StatusCode::OUT_OF_RANGE;

    case ENOSYS:
    case ENOTSUP:
      return StatusCode::UNIMPLEMENTED;

    case EAGAIN:
      return StatusCode::UNAVAILABLE;

    case ECONNABORTED:
      return StatusCode::ABORTED;

    case ECANCELED:
      return StatusCode::CANCELLED;

    default:
      return std::nullopt;
  }
}

}  // namespace cobalt
