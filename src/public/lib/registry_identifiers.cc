// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/public/lib/registry_identifiers.h"

namespace cobalt::lib {

ProjectIdentifier CustomerIdentifier::ForProject(uint32_t project_id) const& {
  return ProjectIdentifier(*this, project_id);
}

ProjectIdentifier CustomerIdentifier::ForProject(uint32_t project_id) && {
  return ProjectIdentifier(*this, project_id);
}

MetricIdentifier ProjectIdentifier::ForMetric(uint32_t metric_id) const& {
  return MetricIdentifier(*this, metric_id);
}

MetricIdentifier ProjectIdentifier::ForMetric(uint32_t metric_id) && {
  return MetricIdentifier(*this, metric_id);
}

ReportIdentifier MetricIdentifier::ForReport(uint32_t report_id) const& {
  return ReportIdentifier(*this, report_id);
}

ReportIdentifier MetricIdentifier::ForReport(uint32_t report_id) && {
  return ReportIdentifier(*this, report_id);
}

}  // namespace cobalt::lib
