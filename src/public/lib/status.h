// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_PUBLIC_LIB_STATUS_H_
#define COBALT_SRC_PUBLIC_LIB_STATUS_H_

#include <ostream>
#include <string>

#include "src/public/lib/status_codes.h"

namespace cobalt {

class Status {
 public:
  Status() = default;  // NOLINT bugprone-exception-escape
                       // (https://github.com/llvm/llvm-project/issues/54668)

  Status(StatusCode code, std::string error_message)
      : code_(code), error_message_(std::move(error_message)) {}

  Status(StatusCode code, std::string error_message, std::string error_details)
      : code_(code),
        error_message_(std::move(error_message)),
        error_details_(std::move(error_details)) {}

  // ToString converts the Status to a human-readable string.
  [[nodiscard]] inline std::string ToString() const;

  [[nodiscard]] StatusCode error_code() const { return code_; }
  [[nodiscard]] std::string error_message() const { return error_message_; }
  [[nodiscard]] std::string error_details() const { return error_details_; }

  [[nodiscard]] bool ok() const { return code_ == StatusCode::OK; }
  [[nodiscard]] bool operator==(const Status &other) const {
    return code_ == other.code_ && error_message_ == other.error_message_ &&
           error_details_ == other.error_details_;
  }

  // Ignores any errors. This method does nothing except potentially suppress
  // complaints from any tools that are checking that errors are not dropped on
  // the floor.
  void IgnoreError() {}

  static Status OkStatus() { return Status(); }

 private:
  StatusCode code_ = StatusCode::OK;
  std::string error_message_;
  std::string error_details_;
};

// Implementation of the steam operator for printing StatusCodes.
std::ostream &operator<<(std::ostream &os, const Status &status);

}  // namespace cobalt

#endif  // COBALT_SRC_PUBLIC_LIB_STATUS_H_
