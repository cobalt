// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_PUBLIC_LIB_REGISTRY_IDENTIFIERS_H_
#define COBALT_SRC_PUBLIC_LIB_REGISTRY_IDENTIFIERS_H_

#include <stdint.h>

#include <string>
#include <tuple>

#include "src/lib/util/status_builder.h"
#include "src/pb/observation_batch.pb.h"
#include "src/registry/metric_definition.pb.h"
#include "src/registry/project.pb.h"

namespace cobalt::lib {

class ProjectIdentifier;
class MetricIdentifier;
class ReportIdentifier;

class CustomerIdentifier {
 public:
  explicit CustomerIdentifier(uint32_t customer_id) : customer_id_(customer_id) {}

  // Conversion constructors
  explicit CustomerIdentifier(const Project& project) : customer_id_(project.customer_id()) {}
  explicit CustomerIdentifier(const ObservationMetadata& metadata)
      : customer_id_(metadata.customer_id()) {}
  explicit CustomerIdentifier(const MetricDefinition& metric)
      : customer_id_(metric.customer_id()) {}

  // Builder-style ProjectIdentifier constructor.
  [[nodiscard]] ProjectIdentifier ForProject(uint32_t project_id) const&;
  [[nodiscard]] ProjectIdentifier ForProject(uint32_t project_id) &&;

  [[nodiscard]] uint32_t customer_id() const { return customer_id_; }

  // Utility methods
  bool operator<(const CustomerIdentifier& o) const { return customer_id_ < o.customer_id_; }
  [[nodiscard]] std::string ToString() const { return std::to_string(customer_id_); }
  friend std::ostream& operator<<(std::ostream& os, const CustomerIdentifier& cust) {
    os << cust.ToString();
    return os;
  }

 private:
  uint32_t customer_id_;
};

class ProjectIdentifier {
 public:
  ProjectIdentifier(CustomerIdentifier customer, uint32_t project_id)
      : customer_(customer), project_id_(project_id) {}

  // Conversion contstructors
  explicit ProjectIdentifier(const Project& project)
      : customer_(project), project_id_(project.project_id()) {}
  explicit ProjectIdentifier(const ObservationMetadata& metadata)
      : customer_(metadata), project_id_(metadata.project_id()) {}
  explicit ProjectIdentifier(const MetricDefinition& metric)
      : customer_(metric), project_id_(metric.project_id()) {}

  // Builder-style MetricIdentifier constructor.
  [[nodiscard]] MetricIdentifier ForMetric(uint32_t metric_id) const&;
  [[nodiscard]] MetricIdentifier ForMetric(uint32_t metric_id) &&;

  [[nodiscard]] uint32_t customer_id() const { return customer_.customer_id(); }
  [[nodiscard]] const CustomerIdentifier& customer() const { return customer_; }
  [[nodiscard]] uint32_t project_id() const { return project_id_; }

  // Utility methods
  bool operator<(const ProjectIdentifier& o) const {
    return std::tuple(customer_id(), project_id_) < std::tuple(o.customer_id(), o.project_id_);
  }
  [[nodiscard]] std::string ToString() const {
    return customer_.ToString() + "-" + std::to_string(project_id_);
  }
  friend std::ostream& operator<<(std::ostream& os, const ProjectIdentifier& proj) {
    os << proj.ToString();
    return os;
  }

 private:
  CustomerIdentifier customer_;
  uint32_t project_id_;
};

class MetricIdentifier {
 public:
  MetricIdentifier(ProjectIdentifier project, uint32_t metric_id)
      : project_(project), metric_id_(metric_id) {}

  // Conversion constructors
  explicit MetricIdentifier(const ObservationMetadata& metadata)
      : project_(metadata), metric_id_(metadata.metric_id()) {}
  explicit MetricIdentifier(const MetricDefinition& metric)
      : project_(metric), metric_id_(metric.id()) {}

  // Builder-style ReportIdentifier constructor.
  [[nodiscard]] ReportIdentifier ForReport(uint32_t report_id) const&;
  [[nodiscard]] ReportIdentifier ForReport(uint32_t report_id) &&;

  [[nodiscard]] uint32_t customer_id() const { return project_.customer_id(); }
  [[nodiscard]] uint32_t project_id() const { return project_.project_id(); }
  [[nodiscard]] const ProjectIdentifier& project() const { return project_; }
  [[nodiscard]] uint32_t metric_id() const { return metric_id_; }

  // Utility methods.
  bool operator<(const MetricIdentifier& o) const {
    return std::tuple(customer_id(), project_id(), metric_id_) <
           std::tuple(o.customer_id(), o.project_id(), o.metric_id_);
  }
  [[nodiscard]] std::string ToString() const {
    return project_.ToString() + "-" + std::to_string(metric_id_);
  }
  friend std::ostream& operator<<(std::ostream& os, const MetricIdentifier& metric) {
    os << metric.ToString();
    return os;
  }

 private:
  ProjectIdentifier project_;
  uint32_t metric_id_;
};

class ReportIdentifier {
 public:
  ReportIdentifier(MetricIdentifier metric, uint32_t report_id)
      : metric_(metric), report_id_(report_id) {}

  // Conversion constructors.
  explicit ReportIdentifier(const ObservationMetadata& metadata)
      : metric_(metadata), report_id_(metadata.report_id()) {}

  [[nodiscard]] uint32_t customer_id() const { return metric_.customer_id(); }
  [[nodiscard]] uint32_t project_id() const { return metric_.project_id(); }
  [[nodiscard]] uint32_t metric_id() const { return metric_.metric_id(); }
  [[nodiscard]] const MetricIdentifier& metric() const { return metric_; }
  [[nodiscard]] uint32_t report_id() const { return report_id_; }

  // Utility methods.
  bool operator<(const ReportIdentifier& o) const {
    return std::tuple(customer_id(), project_id(), metric_id(), report_id_) <
           std::tuple(o.customer_id(), o.project_id(), o.metric_id(), o.report_id_);
  }
  [[nodiscard]] std::string ToString() const {
    return metric_.ToString() + "-" + std::to_string(report_id_);
  }
  friend std::ostream& operator<<(std::ostream& os, const ReportIdentifier& report) {
    os << report.ToString();
    return os;
  }

 private:
  MetricIdentifier metric_;
  uint32_t report_id_;
};

}  // namespace cobalt::lib

namespace cobalt::util {

#define ID_FORMATTER(ty, name)                                      \
  template <>                                                       \
  class ContextFormatter<ty> {                                      \
   public:                                                          \
    explicit ContextFormatter(const ty& v) : v_(v) {}               \
    static inline std::string default_key() { return (name); }      \
    inline void write_value(std::ostream& stream) { stream << v_; } \
                                                                    \
   private:                                                         \
    const ty& v_;                                                   \
  }

ID_FORMATTER(lib::CustomerIdentifier, "Customer");
ID_FORMATTER(lib::ProjectIdentifier, "Project");
ID_FORMATTER(lib::MetricIdentifier, "Metric");
ID_FORMATTER(lib::ReportIdentifier, "Report");

}  // namespace cobalt::util

#endif  // COBALT_SRC_PUBLIC_LIB_REGISTRY_IDENTIFIERS_H_
