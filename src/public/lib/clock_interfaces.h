// Copyright 2017 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_PUBLIC_LIB_CLOCK_INTERFACES_H_
#define COBALT_SRC_PUBLIC_LIB_CLOCK_INTERFACES_H_

#include <chrono>
#include <optional>
#include <string>

#include "src/public/lib/compat/clock.h"
#include "src/public/lib/statusor/statusor.h"

namespace cobalt::util {

// Allows us to mock out a clock for tests.
class SystemClockInterface {
 public:
  virtual ~SystemClockInterface() = default;

  virtual std::chrono::system_clock::time_point now() = 0;
};

// Allows us to mock out a clock for tests.
class SteadyClockInterface {
 public:
  virtual ~SteadyClockInterface() = default;

  virtual std::chrono::steady_clock::time_point now() = 0;
};

// A monotonically increasing clock that doesn't pause during suspension. For Fuchsia, see RFC-0259:
// https://fuchsia.dev/fuchsia-src/contribute/governance/rfcs/0259_monotonic_clock_suspension_and_the_boot_timeline.
class BootClockInterface {
 public:
  virtual ~BootClockInterface() = default;

  virtual compat::boot_clock::time_point now() = 0;
};

// An abstract interface to a clock that refuses to provide the time if a quality condition is not
// satisfied; for example, if the clock has not been initialized from a trustworthy source.
class ValidatedClockInterface {
 public:
  virtual ~ValidatedClockInterface() = default;

  // Returns the current time if an accurate clock is avaliable or nullopt otherwise.
  virtual std::optional<std::chrono::system_clock::time_point> now() = 0;
};

// An abstract interface to a class that has access to a time zone database, and that converts
// time points to civil time in a specified time zone.
class CivilTimeConverterInterface {
 public:
  virtual ~CivilTimeConverterInterface() = default;

  // Returns the time struct corresponding to a system time `time` in the time zone identified by
  // `time_zone`, which must be a valid IANA time zone identifier. May return an error status; for
  // example, returns an error if `time_zone` is not a valid time zone identifier.
  [[nodiscard]] virtual lib::statusor::StatusOr<std::tm> civil_time(
      std::chrono::system_clock::time_point time, const std::string& time_zone) const = 0;
};

}  // namespace cobalt::util

#endif  // COBALT_SRC_PUBLIC_LIB_CLOCK_INTERFACES_H_
