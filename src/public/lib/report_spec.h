// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_PUBLIC_LIB_REPORT_SPEC_H_
#define COBALT_SRC_PUBLIC_LIB_REPORT_SPEC_H_

#include <stdint.h>

#include <tuple>

namespace cobalt::lib {

// A specification to identify a single Cobalt report.
// DEPRECATED: Use ReportIdentifier.
struct ReportSpec {
  uint32_t customer_id;
  uint32_t project_id;
  uint32_t metric_id;
  uint32_t report_id;

  bool operator<(const ReportSpec& o) const {
    return std::tie(customer_id, project_id, metric_id, report_id) <
           std::tie(o.customer_id, o.project_id, o.metric_id, o.report_id);
  }
};

}  // namespace cobalt::lib

#endif  // COBALT_SRC_PUBLIC_LIB_REPORT_SPEC_H_
