// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/public/lib/status.h"

#include <sstream>

namespace cobalt {

[[nodiscard]] inline std::string Status::ToString() const {
  if (ok()) {
    return "OK";
  }
  std::stringstream err;
  err << code_ << ": " << error_message_;
  if (!error_details_.empty()) {
    err << " (" << error_details_ << ")";
  }
  return err.str();
}

std::ostream &operator<<(std::ostream &os, const Status &status) { return os << status.ToString(); }

}  // namespace cobalt
