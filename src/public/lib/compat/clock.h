// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_PUBLIC_LIB_COMPAT_CLOCK_H_
#define COBALT_SRC_PUBLIC_LIB_COMPAT_CLOCK_H_

#include <chrono>
#include <ctime>

#include "src/logging.h"

namespace cobalt::util::compat {

class boot_clock {
 public:
  using duration = std::chrono::nanoseconds;
  using time_point = std::chrono::time_point<boot_clock, duration>;

  static time_point now() {
    std::timespec ts;
    if (clock_gettime(CLOCK_BOOTTIME, &ts) != 0) {
      LOG(ERROR) << "Failed to get boot time";
      return compat::boot_clock::time_point();
    }

    const compat::boot_clock::duration duration =
        std::chrono::seconds(ts.tv_sec) + std::chrono::nanoseconds(ts.tv_nsec);

    return compat::boot_clock::time_point(duration);
  }
};

}  // namespace cobalt::util::compat

#endif  // COBALT_SRC_PUBLIC_LIB_COMPAT_CLOCK_H_
