// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/public/lib/registry_identifiers.h"

#include <type_traits>

#include <gtest/gtest.h>

namespace cobalt::lib {

// All RegistryIdentifiers are used by-value, so they should always be trivially_copyable.
// This test makes sure that doesn't change.
TEST(RegistryIdentifiersTest, IdentifiersAreTriviallyCopyable) {
  EXPECT_TRUE(std::is_trivially_copyable_v<CustomerIdentifier>);
  EXPECT_TRUE(std::is_trivially_copyable_v<ProjectIdentifier>);
  EXPECT_TRUE(std::is_trivially_copyable_v<MetricIdentifier>);
  EXPECT_TRUE(std::is_trivially_copyable_v<ReportIdentifier>);
}

// All RegistryIdentifiers must be a well-known size (4 bytes per id).
// This test makes sure that doesn't change.
TEST(RegistryIdentifiersTest, IdentifiersAreExpectedSizes) {
  EXPECT_EQ(sizeof(CustomerIdentifier(UINT_MAX)), 4u);
  EXPECT_EQ(sizeof(ProjectIdentifier(CustomerIdentifier(UINT_MAX), UINT_MAX)), 8u);
  EXPECT_EQ(
      sizeof(MetricIdentifier(ProjectIdentifier(CustomerIdentifier(UINT_MAX), UINT_MAX), UINT_MAX)),
      12u);
  EXPECT_EQ(
      sizeof(ReportIdentifier(
          MetricIdentifier(ProjectIdentifier(CustomerIdentifier(UINT_MAX), UINT_MAX), UINT_MAX),
          UINT_MAX)),
      16u);
}

}  // namespace cobalt::lib
