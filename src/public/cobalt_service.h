// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_PUBLIC_COBALT_SERVICE_H_
#define COBALT_SRC_PUBLIC_COBALT_SERVICE_H_

#include <chrono>
#include <cstddef>
#include <memory>
#include <string>

#include "src/lib/util/encrypted_message_util.h"
#include "src/lib/util/not_null.h"
#include "src/local_aggregation/local_aggregation.h"
#include "src/logger/internal_metrics.h"
#include "src/logger/logger.h"
#include "src/logger/observation_writer.h"
#include "src/logger/project_context.h"
#include "src/logger/project_context_factory.h"
#include "src/logger/undated_event_manager.h"
#include "src/observation_store/observation_store.h"
#include "src/public/clockless_cobalt_service_interface.h"
#include "src/public/cobalt_config.h"
#include "src/public/cobalt_service_interface.h"
#include "src/public/diagnostics_interface.h"
#include "src/public/lib/clock_interfaces.h"
#include "src/public/lib/registry_identifiers.h"
#include "src/public/lib/statusor/statusor.h"
#include "src/system_data/system_data.h"
#include "src/uploader/shipping_manager.h"

namespace cobalt {

// CobaltService is the primary public interface for Cobalt on client platforms.
//
// It is constructed using a CobaltConfig struct, which provides all available customization options
// for the behavior of Cobalt.
//
// Example:
//
// CobaltConfig cfg;
// cfg.product_name = "product";
// cfg.version = "version";
// ... continue setting config values ...
//
// CobaltService service(std::move(cfg));
//
// // Get a logger:
//
// auto logger = service.NewLogger(project_context);
// logger.LogEvent(Event);
//
class CobaltService : public CobaltServiceInterface, public ClocklessCobaltServiceInterface {
 public:
  static lib::statusor::StatusOr<std::unique_ptr<CobaltService>> Create(CobaltConfig cfg);
  static lib::statusor::StatusOr<std::unique_ptr<ClocklessCobaltServiceInterface>> CreateClockless(
      CobaltConfig cfg);

  CobaltService(const CobaltService &) = delete;
  CobaltService(CobaltService &&) = delete;
  CobaltService &operator=(const CobaltService &) = delete;
  CobaltService &operator=(CobaltService &&) = delete;

  // NewLogger returns a new instance of a Logger object based on the provided |project_context|.
  std::unique_ptr<logger::LoggerInterface> NewLogger(
      std::unique_ptr<logger::ProjectContext> project_context) override;

  std::unique_ptr<logger::LoggerInterface> NewLogger(uint32_t customer_id,
                                                     uint32_t project_id) override;

  std::unique_ptr<logger::LoggerInterface> NewLogger(uint32_t customer_id, uint32_t project_id,
                                                     std::vector<uint32_t> experiment_ids) override;

  std::unique_ptr<logger::AtTimePointLoggerInterface> NewAtTimePointLogger(
      uint32_t customer_id, uint32_t project_id) override;

 private:
  explicit CobaltService(CobaltConfig cfg,
                         logger::ProjectContextFactory &&global_project_context_factory,
                         const uploader::UploadScheduler &upload_scheduler);

  std::unique_ptr<logger::Logger> NewLogger(std::unique_ptr<logger::ProjectContext> project_context,
                                            bool include_internal_logger,
                                            std::vector<uint32_t> experiment_ids);

  std::unique_ptr<logger::Logger> NewLogger(uint32_t customer_id, uint32_t project_id,
                                            bool include_internal_logger,
                                            std::vector<uint32_t> experiment_ids);

  void CleanUpLegacyFileOrDirectory(const std::string &file_or_directory);

 public:
  // SystemClockIsAccurate lets CobaltService know that it no longer needs to maintain an
  // UndatedEventManager, and can flush the data from it into the observation store.
  //
  // This method should be used at most once in the lifetime of a CobaltService object.
  //
  // |system_clock|: An instance of SystemClockInterface that is used to add a timestamp to all of
  // the events that were received before the system clock was made accurate. It is then given to
  // the EventAggregator if |start_event_aggregator_worker| is true.
  void SystemClockIsAccurate(std::unique_ptr<util::SystemClockInterface> system_clock,
                             bool start_event_aggregator_worker) override;

  // system_data returns a pointer to the internal SystemData object. This should only be used for
  // updating the Expirement state or channel in SystemData.
  system_data::SystemDataInterface *system_data() override { return &system_data_; }
  system_data::SystemDataInterface &system_data_ref() override { return system_data_; }

  // Sets the data collection policy.
  void SetDataCollectionPolicy(DataCollectionPolicy policy) override;

  // Generates aggregated observations for aggregation periods that have ended in the time zone
  // associated with the metric.
  Status GenerateAggregatedObservations() override;
  // Test-only method that generates aggregated observations for aggregation periods with day index
  // less than or equal to final_day_index_utc (including hourly observations).
  Status GenerateAggregatedObservations(uint32_t final_day_index_utc) override;

  // Generates aggregated observations for aggregation periods that have ended
  // before |timestamp| in the time zone associated with the metric.
  Status GenerateAggregatedObservations(
      const std::chrono::system_clock::time_point &timestamp) override;

  [[nodiscard]] uint64_t num_observations_added() const override {
    return observation_store_->num_observations_added();
  }

  [[nodiscard]] std::vector<uint64_t> num_observations_added_for_reports(
      const std::vector<lib::ReportIdentifier> &report_specs) const override {
    return observation_store_->num_observations_added_for_reports(report_specs);
  }

  void ShippingRequestSendSoon(const SendCallback &send_callback) override {
    shipping_manager_->RequestSendSoon(send_callback);
  }

  void WaitUntilShippingIdle(std::chrono::seconds max_wait) override {
    shipping_manager_->WaitUntilIdle(max_wait);
  }

  void ShutDown() override { local_aggregation_.ShutDown(); }

  [[nodiscard]] size_t num_shipping_send_attempts() const override {
    return shipping_manager_->num_send_attempts();
  }
  [[nodiscard]] size_t num_shipping_failed_attempts() const override {
    return shipping_manager_->num_failed_attempts();
  }

  bool has_internal_logger() const { return internal_logger_ != nullptr; }

 private:
  friend class CobaltServicePeer;

  observation_store::ObservationStore &observation_store() { return *observation_store_; }
  uploader::ShippingManager *shipping_manager() { return shipping_manager_.get(); }

  util::PinnedUniquePtr<util::FileSystem> fs_;
  system_data::SystemData system_data_;
  logger::ProjectContextFactory global_project_context_factory_;
  std::unique_ptr<DiagnosticsInterface>
      diagnostics_;  // Null if no DiagnosticsInterface is provided.
  util::PinnedUniquePtr<observation_store::ObservationStore> observation_store_;
  util::PinnedUniquePtr<util::EncryptedMessageMaker> encrypt_to_analyzer_;
  util::PinnedUniquePtr<util::EncryptedMessageMaker> encrypt_to_shuffler_;
  util::PinnedUniquePtr<uploader::ShippingManager> shipping_manager_;
  logger::ObservationWriter observation_writer_;
  util::PinnedUniquePtr<util::CivilTimeConverterInterface> civil_time_converter_;
  local_aggregation::LocalAggregation local_aggregation_;
  std::shared_ptr<logger::UndatedEventManager>
      undated_event_manager_;  // Null after the system clock becomes accurate.
  util::ValidatedClockInterface *validated_clock_;
  std::unique_ptr<logger::LoggerInterface>
      internal_logger_;  // Null if the internal metrics are not in the config.
  util::PinnedUniquePtr<logger::InternalMetrics> internal_metrics_;
  std::unique_ptr<util::SystemClockInterface>
      system_clock_;  // Null until system clock is accurate.
  bool start_worker_threads_;
};

}  // namespace cobalt

#endif  // COBALT_SRC_PUBLIC_COBALT_SERVICE_H_
