// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_PUBLIC_DIAGNOSTICS_INTERFACE_H_
#define COBALT_SRC_PUBLIC_DIAGNOSTICS_INTERFACE_H_

#include <map>

#include "src/public/lib/registry_identifiers.h"
#include "src/public/lib/report_spec.h"
#include "src/public/lib/status.h"

namespace cobalt {

// Interface for receiving diagnostic information about the functioning of the Cobalt Core library.
class DiagnosticsInterface {
 public:
  DiagnosticsInterface() = default;
  virtual ~DiagnosticsInterface() = default;

  // Called when Cobalt finishes an attempt to send observations to the server.
  //
  // |status| is the result of the attempt.
  virtual void SentObservationResult(const Status& status) = 0;

  // Called when the Cobalt local Observation Store is updated.
  //
  // |status| is the result of the attempt.
  virtual void ObservationStoreUpdated(
      const std::map<lib::ReportSpec, uint64_t>& num_obs_per_report, int64_t store_byte_count,
      int64_t max_store_bytes) = 0;

  void ObservationStoreUpdated(const std::map<lib::ReportIdentifier, uint64_t>& num_obs_per_report,
                               int64_t store_byte_count, int64_t max_store_bytes) {
    std::map<lib::ReportSpec, uint64_t> map;
    for (const auto& [key, value] : num_obs_per_report) {
      map[{
          .customer_id = key.customer_id(),
          .project_id = key.project_id(),
          .metric_id = key.metric_id(),
          .report_id = key.report_id(),
      }] = value;
    }
    ObservationStoreUpdated(map, store_byte_count, max_store_bytes);
  }

  // Logged for every call to Logger along with which method was called and the project
  // that called it.
  virtual void LoggerCalled(int PerProjectLoggerCallsMadeMigratedMetricDimensionLoggerMethod,
                            const std::string& project) = 0;

  // Used to track how much data is stored per-class on disk.
  // TODO(https://fxbug.dev/278930401): use named types when InternalMetrics::TrackDiskUsage is
  // fixed. NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
  virtual void TrackDiskUsage(int storageClass, int64_t bytes, int64_t byte_limit) = 0;

  // Used to track what the quota state of each project on the system is.
  //
  // |event_type|: tracks what the state is (1: Below quota, 2: Above quota, 3: Above quota and log
  //               rejected).
  //
  // TODO(https://fxbug.dev/42178669): Remove default implementation once this is implemented in
  // Fuchsia.
  virtual void LocalAggregationQuotaEvent(const lib::ProjectIdentifier& project, int event_type) {}
};

}  // namespace cobalt

#endif  // COBALT_SRC_PUBLIC_DIAGNOSTICS_INTERFACE_H_
