// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_PUBLIC_CLOCKLESS_COBALT_SERVICE_INTERFACE_H_
#define COBALT_SRC_PUBLIC_CLOCKLESS_COBALT_SERVICE_INTERFACE_H_

#include "src/logger/at_time_point_logger_interface.h"
#include "src/system_data/system_data.h"
namespace cobalt {

class ClocklessCobaltServiceInterface {
 public:
  virtual ~ClocklessCobaltServiceInterface() = default;

  // NewAtTimePointLogger returns a new instance of a Logger object using the
  // customer_id and project_id based
  // on the registry provided in the config. If the project is not found, this
  // will return nullptr. If no registry was provided, this will crash.
  virtual std::unique_ptr<logger::AtTimePointLoggerInterface> NewAtTimePointLogger(
      uint32_t customer_id, uint32_t project_id) = 0;

  // system_data returns a pointer to the internal SystemDataInterface object.
  // This should only be used for updating the Experiment state or channel in the system data.
  virtual system_data::SystemDataInterface* system_data() = 0;
  // system_data_ref returns a reference to the internal SystemDataInterface
  // object. This should be used instead of system_data().
  virtual system_data::SystemDataInterface& system_data_ref() = 0;

  // Generates aggregated observations for aggregation periods that have ended
  // before |timestamp| in the time zone associated with the metric.
  virtual Status GenerateAggregatedObservations(
      const std::chrono::system_clock::time_point& timestamp) = 0;

  // Courtesy signal for Cobalt to begin a graceful shutdown.
  //
  // Cobalt may be terminated before the shut down process is complete. The
  // amount of time before this occurs depends on caller. No metrics should be
  // logged after the shutdown signal is sent.
  virtual void ShutDown() = 0;
};

}  // namespace cobalt

#endif  // COBALT_SRC_PUBLIC_CLOCKLESS_COBALT_SERVICE_INTERFACE_H_
