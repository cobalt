// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/public/cobalt_service.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "src/lib/util/posix_file_system.h"
#include "src/uploader/shipping_manager.h"
#include "third_party/abseil-cpp/absl/strings/escaping.h"

namespace cobalt {

using uploader::ClearcutV1ShippingManager;
using uploader::ShippingManager;

namespace {

CobaltConfig MinConfigForTesting() {
  CobaltConfig cfg = {.client_secret = system_data::ClientSecret::GenerateNewSecret()};

  cfg.file_system = std::make_unique<util::PosixFileSystem>();
  cfg.target_pipeline = std::make_unique<LocalPipeline>();

  cfg.observation_store_directory = "/tmp/a";
  cfg.local_aggregate_store_dir = "/tmp/d";
  cfg.system_data_cache_path = "/tmp/e";

  return cfg;
}

std::unique_ptr<TargetPipeline> MinClearcutTargetPipeline() {
  return std::make_unique<TargetPipeline>(system_data::Environment::DEVEL,
                                          /*shuffler_encryption_key=*/"",
                                          /*analyzer_encryption_key=*/"", /*http_client=*/nullptr);
}

}  // namespace

class CobaltServicePeer {
 public:
  static bool HasShippingManager(const CobaltService& service, ShippingManager::Impl impl) {
    return service.shipping_manager_->impl() == impl;
  }

  static bool HasRealInternalMetrics(const CobaltService& service) {
    if (service.shipping_manager_->impl() == ShippingManager::Impl::CLEARCUTV1) {
      const uploader::ClearcutV1ShippingManager* clearcut_shipping =
          static_cast<const ClearcutV1ShippingManager*>(service.shipping_manager_.get());
      if (!clearcut_shipping->internal_metrics()->IsRealImpl()) {
        return false;
      }
      if (!clearcut_shipping->clearcut_uploader()->internal_metrics()->IsRealImpl()) {
        return false;
      }
    }

    return service.observation_store_->internal_metrics()->IsRealImpl();
  }

  static bool HasCivilTimeConverterInterface(const CobaltService& service) {
    return service.civil_time_converter_.get() != nullptr;
  }
};

TEST(CobaltService, FailsConstructionWithNoGlobalRegistry) {
  CobaltConfig cfg = MinConfigForTesting();
  cfg.global_registry = nullptr;
  lib::statusor::StatusOr<std::unique_ptr<CobaltService>> service =
      CobaltService::Create(std::move(cfg));
  ASSERT_EQ(service.status().error_code(), StatusCode::INVALID_ARGUMENT);
  ASSERT_THAT(service.status().error_message(),
              testing::HasSubstr("CobaltService requires a non-null global_registry"));
}

TEST(CobaltService, DoesNotCreateInternalLoggerWithEmptyGlobalRegistry) {
  CobaltConfig cfg = MinConfigForTesting();
  cfg.global_registry = std::make_unique<CobaltRegistry>();
  std::unique_ptr<CobaltService> service = CobaltService::Create(std::move(cfg)).value();
  EXPECT_FALSE(service->has_internal_logger());
  EXPECT_TRUE(CobaltServicePeer::HasShippingManager(*service, ShippingManager::Impl::OTHER));
  EXPECT_FALSE(CobaltServicePeer::HasRealInternalMetrics(*service));
}

TEST(CobaltService, CreatesInternalLoggerWithValidRegistry) {
  CobaltConfig cfg = MinConfigForTesting();
  cfg.global_registry = std::make_unique<CobaltRegistry>();
  std::string registry_bytes;
  ASSERT_TRUE(absl::Base64Unescape(cobalt::logger::kConfig, &registry_bytes));
  ASSERT_TRUE(cfg.global_registry->ParseFromString(registry_bytes));
  std::unique_ptr<CobaltService> service = CobaltService::Create(std::move(cfg)).value();
  EXPECT_TRUE(service->has_internal_logger());
  EXPECT_TRUE(CobaltServicePeer::HasShippingManager(*service, ShippingManager::Impl::OTHER));
  EXPECT_TRUE(CobaltServicePeer::HasRealInternalMetrics(*service));
}

// Tests that when a real ClearcutV1ShippingManager is used that it
// gets a real internal logger to use.
TEST(CobaltService, CreatesClearcutV1ShippingManager) {
  CobaltConfig cfg = MinConfigForTesting();
  cfg.global_registry = std::make_unique<CobaltRegistry>();
  cfg.target_pipeline = MinClearcutTargetPipeline();
  std::string registry_bytes;
  ASSERT_TRUE(absl::Base64Unescape(cobalt::logger::kConfig, &registry_bytes));
  ASSERT_TRUE(cfg.global_registry->ParseFromString(registry_bytes));
  std::unique_ptr<CobaltService> service = CobaltService::Create(std::move(cfg)).value();
  EXPECT_TRUE(service->has_internal_logger());
  ASSERT_TRUE(CobaltServicePeer::HasShippingManager(*service, ShippingManager::Impl::CLEARCUTV1));
  EXPECT_TRUE(CobaltServicePeer::HasRealInternalMetrics(*service));
}

TEST(CobaltServiceTest, CreatesDefaultCivilTimeConverter) {
  CobaltConfig cfg = MinConfigForTesting();
  EXPECT_EQ(cfg.civil_time_converter, nullptr);
  cfg.global_registry = std::make_unique<CobaltRegistry>();
  std::unique_ptr<CobaltService> service = CobaltService::Create(std::move(cfg)).value();
  EXPECT_TRUE(CobaltServicePeer::HasCivilTimeConverterInterface(*service));
}

}  // namespace cobalt
