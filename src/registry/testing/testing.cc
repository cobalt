// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/registry/testing/testing.h"

namespace cobalt::testing {

std::unique_ptr<CobaltRegistry> MutateProject(
    std::unique_ptr<CobaltRegistry> registry,
    // TODO(https://fxbug.dev/278930401): NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
    uint32_t customer_id, uint32_t project_id,
    const std::function<void(ProjectConfig* project)>& mutate_project) {
  for (auto& customer : *registry->mutable_customers()) {
    if (customer.customer_id() != customer_id) {
      continue;
    }
    for (auto& project : *customer.mutable_projects()) {
      if (project.project_id() != project_id) {
        continue;
      }
      mutate_project(&project);
    }
  }

  return registry;
}

}  // namespace cobalt::testing
