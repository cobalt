// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_REGISTRY_TESTING_TESTING_H_
#define COBALT_SRC_REGISTRY_TESTING_TESTING_H_

#include "src/registry/cobalt_registry.pb.h"

namespace cobalt::testing {

std::unique_ptr<CobaltRegistry> MutateProject(
    std::unique_ptr<CobaltRegistry> registry, uint32_t customer_id, uint32_t project_id,
    const std::function<void(ProjectConfig* project)>& mutate_project);

}  // namespace cobalt::testing

#endif  // COBALT_SRC_REGISTRY_TESTING_TESTING_H_
