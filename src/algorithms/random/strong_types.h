// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_ALGORITHMS_RANDOM_STRONG_TYPES_H_
#define COBALT_SRC_ALGORITHMS_RANDOM_STRONG_TYPES_H_

#include "src/lib/util/named_type.h"

namespace cobalt {

// A probability value used in various algorithms.
using Probability = util::NamedType<double, struct ProbabilityTag>;

// The mean of a Poisson distribution.
using PoissonParameter = util::NamedType<double, struct PoissonParameterTag>;

}  // namespace cobalt

#endif  // COBALT_SRC_ALGORITHMS_RANDOM_STRONG_TYPES_H_
