#include "src/algorithms/random/random.h"

#include <gtest/gtest.h>

namespace cobalt {

TEST(RandomNumberGeneratorTest, Generate) {
  auto gen = RandomNumberGenerator();
  uint32_t val = gen();
  EXPECT_GE(val, 0u);
  EXPECT_LE(val, UINT32_MAX);
}

TEST(RandomNumberGeneratorTest, MinMax) {
  auto gen = RandomNumberGenerator();
  EXPECT_EQ(gen.min(), 0u);
  EXPECT_EQ(gen.max(), UINT32_MAX);
}

TEST(FakeRandomNumberGeneratorTest, Generate) {
  uint32_t val = 100;
  auto fake_gen = FakeRandomNumberGenerator(val);
  EXPECT_EQ(fake_gen(), val);
}

TEST(FakeRandomNumberGenerator, MinMax) {
  uint32_t val = 100;
  auto fake_gen = FakeRandomNumberGenerator(val);
  EXPECT_EQ(fake_gen.min(), 0u);
  EXPECT_EQ(fake_gen.max(), UINT32_MAX);
}

TEST(SecureRandomNumberGeneratorTest, GenerateMany) {
  auto gen = SecureRandomNumberGenerator();
  for (int i = 0; i < 256; i++) {
    uint32_t val = gen();
    EXPECT_GE(val, 0u);
    EXPECT_LE(val, UINT32_MAX);
  }
}

}  // namespace cobalt
