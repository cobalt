#include "src/algorithms/random/distributions.h"

#include <random>

#include "src/algorithms/random/random.h"

namespace cobalt {

BernoulliDistribution::BernoulliDistribution(BitGeneratorInterface<uint32_t>* gen, Probability p)
    : gen_(gen) {
  dist_ = std::bernoulli_distribution(p.get());
}

bool BernoulliDistribution::Sample() { return dist_(*gen_); }

BinomialDistribution::BinomialDistribution(BitGeneratorInterface<uint32_t>* gen,
                                           uint64_t num_trials, Probability p)
    : gen_(gen) {
  dist_ = std::binomial_distribution<uint64_t>(num_trials, p.get());
}

uint64_t BinomialDistribution::Sample() { return dist_(*gen_); }

DiscreteUniformDistribution::DiscreteUniformDistribution(BitGeneratorInterface<uint32_t>* gen,
                                                         uint64_t min, uint64_t max)
    : gen_(gen) {
  dist_ = std::uniform_int_distribution<uint64_t>(min, max);
}

uint64_t DiscreteUniformDistribution::Sample() { return dist_(*gen_); }

GeometricDistribution::GeometricDistribution(BitGeneratorInterface<uint32_t>* gen, Probability p)
    : gen_(gen) {
  dist_ = std::geometric_distribution<uint64_t>(p.get());
}

uint64_t GeometricDistribution::Sample() { return dist_(*gen_); }

PoissonDistribution::PoissonDistribution(BitGeneratorInterface<uint32_t>* gen,
                                         PoissonParameter mean)
    : gen_(gen) {
  dist_ = std::poisson_distribution<>(mean.get());
}

int PoissonDistribution::Sample() { return dist_(*gen_); }

}  // namespace cobalt
