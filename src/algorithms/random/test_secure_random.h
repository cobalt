// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
#ifndef COBALT_SRC_ALGORITHMS_RANDOM_TEST_SECURE_RANDOM_H_
#define COBALT_SRC_ALGORITHMS_RANDOM_TEST_SECURE_RANDOM_H_

#include <random>

#include "src/algorithms/random/random.h"

namespace cobalt {
// An insecure implementation of SecureBitGeneratorInterface to be used for tests only.
class TestSecureRandomNumberGenerator : public SecureBitGeneratorInterface<uint32_t> {
 public:
  TestSecureRandomNumberGenerator() { engine_ = std::mt19937(rd_()); }
  explicit TestSecureRandomNumberGenerator(uint32_t seed) { engine_ = std::mt19937(seed); }
  result_type operator()() override { return static_cast<uint32_t>(engine_()); }

 private:
  std::random_device rd_;
  std::mt19937 engine_;
};

}  // namespace cobalt

#endif  // COBALT_SRC_ALGORITHMS_RANDOM_TEST_SECURE_RANDOM_H_
