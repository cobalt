#include "src/algorithms/random/random.h"

#include <openssl/rand.h>

namespace cobalt {

namespace {
inline void ConfigureBoringSSLRandom() {
// When running on Fuchsia, the fork protection offered by BoringSSL is not needed and adds
// undesirable overhead.
#if defined(__Fuchsia__)
  RAND_enable_fork_unsafe_buffering(-1);
#endif
}
}  // namespace

SecureRandomNumberGenerator::SecureRandomNumberGenerator() { ConfigureBoringSSLRandom(); }

SecureRandomNumberGenerator::result_type SecureRandomNumberGenerator::operator()() {
  result_type ret;
  RAND_bytes(reinterpret_cast<uint8_t*>(&ret), sizeof(result_type));
  return ret;
}

}  // namespace cobalt
