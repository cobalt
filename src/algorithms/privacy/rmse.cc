// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/algorithms/privacy/rmse.h"

#include <cmath>

namespace cobalt {

namespace {

inline double square(const double v) { return v * v; }

}  // namespace

// Assumptions:
// - min_value < max_value
// - num_index_points > 1
// NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
double PoissonSumRmse(const double lambda, const double min_value, const double max_value,
                      const uint64_t num_index_points) {
  double n = static_cast<double>(num_index_points);
  double interval = (max_value - min_value) / (n - 1);
  // NOLINTBEGIN(readability-magic-numbers)
  double sum_of_nums = ((n - 1) * n) / 2.0;
  double sum_of_squares = ((n - 1) * (2 * n - 1) * n) / 6.0;
  double sum_of_xi_squared = n * square(min_value) + (2.0 * min_value * interval * sum_of_nums) +
                             square(interval) * sum_of_squares;
  // NOLINTEND(readability-magic-numbers)
  return std::sqrt(lambda * sum_of_xi_squared);
}

double PoissonUnaryRmse(const double lambda) { return std::sqrt(lambda); }

}  // namespace cobalt
