// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_ALGORITHMS_PRIVACY_RMSE_H_
#define COBALT_SRC_ALGORITHMS_PRIVACY_RMSE_H_

#include <cstdint>

namespace cobalt {

// Computes the RMSE for a report row in which the true value for each device is summed and
// is in the |min_value| - |max_value| range which is discretized in |num_index_points| indices.
// NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
double PoissonSumRmse(double lambda, double min_value, double max_value, uint64_t num_index_points);

// Computes the RMSE for a report row in which the true value for each device can be 1 or 0.
double PoissonUnaryRmse(double lambda);

}  // namespace cobalt

#endif  //  COBALT_SRC_ALGORITHMS_PRIVACY_RMSE_H_
