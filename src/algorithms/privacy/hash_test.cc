// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "src/algorithms/privacy/hash.h"

#include <gtest/gtest.h>

namespace cobalt {

TEST(HashTest, Hash64WithSeedStableHash) {
  EXPECT_EQ(0x6817bff2c1a85fb3u, Hash64WithSeed("hello world", 1));
  EXPECT_EQ(0x7304bac4b22251f5u, Hash64WithSeed("hi there earth", 1));
  EXPECT_EQ(0x9b9bbae50eb096d4u, Hash64WithSeed("bonjour monde", 1));
  EXPECT_EQ(0x61cb7a20b37ba347u, Hash64WithSeed("hallo Welt", 1));
}

TEST(HashTest, Hash64WithSeedDifferentSeeds) {
  auto hash0 = Hash64WithSeed("hello world", 0);
  auto hash1 = Hash64WithSeed("hello world", 1);
  EXPECT_NE(hash0, hash1);
}

TEST(HashTest, Hash64WithSeedDifferentFirstLetter) {
  auto hash0 = Hash64WithSeed("hello world", 1);
  auto hash1 = Hash64WithSeed("iello world", 1);
  EXPECT_NE(hash0, hash1);
}

TEST(HashTest, Hash64WithSeedDifferentLastLetter) {
  auto hash0 = Hash64WithSeed("hello world", 1);
  auto hash1 = Hash64WithSeed("hello worle", 1);
  EXPECT_NE(hash0, hash1);
}

TEST(HashTest, TruncatedDigestStableDigest) {
  EXPECT_EQ(15u, TruncatedDigest("hello world", 1, 100));
  EXPECT_EQ(69u, TruncatedDigest("hi there earth", 1, 100));
  EXPECT_EQ(52u, TruncatedDigest("bonjour monde", 1, 100));
  EXPECT_EQ(95u, TruncatedDigest("hallo Welt", 1, 100));
}

TEST(HashTest, TruncatedDigestDifferentSeeds) {
  auto digest0 = TruncatedDigest("hello world", 0, 100);
  auto digest1 = TruncatedDigest("hello world", 1, 100);
  EXPECT_NE(digest0, digest1);
}

TEST(HashTest, TruncatedDigestDifferentFirstLetter) {
  auto digest0 = TruncatedDigest("hello world", 1, 100);
  auto digest1 = TruncatedDigest("iello world", 1, 100);
  EXPECT_NE(digest0, digest1);
}

TEST(HashTest, TruncatedDigestDifferentLastLetter) {
  auto digest0 = TruncatedDigest("hello world", 1, 100);
  auto digest1 = TruncatedDigest("hello worle", 1, 100);
  EXPECT_NE(digest0, digest1);
}

}  // namespace cobalt
