// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COBALT_SRC_ALGORITHMS_PRIVACY_NUMERIC_ENCODING_H_
#define COBALT_SRC_ALGORITHMS_PRIVACY_NUMERIC_ENCODING_H_

#include "src/algorithms/random/random.h"

namespace cobalt {

// Given a |value|, the DoubleToIndex() function returns an index in the range
// [0, |num_index_points| - 1] using randomized fixed-point encoding.
//
// The |value| is snapped to one of the |num_index_points| boundary points of the equal subsegments
// of the range [|min_value|, |max_value|]. If |value| is not equal to a boundary point, then
// |value| is snapped to one of the two nearest boundary points, with probability proportional to
// its distance from each of those points.
//
// Value must be in the range [|min_value|, |max_value|].
uint64_t DoubleToIndex(double value, double min_value, double max_value, uint64_t num_index_points,
                       BitGeneratorInterface<uint32_t>* gen);

// See DoubleToIndex.
uint64_t IntegerToIndex(int64_t value, int64_t min_value, int64_t max_value,
                        uint64_t num_index_points, BitGeneratorInterface<uint32_t>* gen);

// Encodes a |count| to the range [|num_index_points|, 2 * |num_index_points| - 1]. See
// DoubleToIndex for a description of the encoding scheme.
uint64_t CountToIndex(uint64_t count, uint64_t max_count, uint64_t num_index_points,
                      BitGeneratorInterface<uint32_t>* gen);

// Encodes a |bucket_index|, |bucket_count| pair by encoding the |bucket_count| as per the
// DoubleToIndex encoding scheme plus an offset equal to |bucket_index| * |num_index_points|.
uint64_t HistogramBucketAndCountToIndex(uint32_t bucket_index, uint64_t bucket_count,
                                        uint64_t max_count, uint64_t num_index_points,
                                        BitGeneratorInterface<uint32_t>* gen);

// DoubleFromIndex maps |index| to the numeric value corresponding to the boundary point with index
// |index|.
double DoubleFromIndex(uint64_t index, double min_value, double max_value,
                       uint64_t num_index_points);

// Decodes an |index| that was encoded with CountToIndex, as a double representing an approximate
// count.
double CountFromIndex(uint64_t index, uint64_t max_count, uint64_t num_index_points);

// Decodes an |index| that was encoded with HistogramBucketAndCountToIndex, decoding the count value
// as a double.
void HistogramBucketAndCountFromIndex(uint64_t index, uint64_t max_count, uint64_t num_index_points,
                                      uint32_t* bucket_index, double* bucket_count);

// ValueAndEventVectorIndicesToIndex uniquely maps a |value_index|, |event_vector_index| pair to a
// single index.
//
// If |value_index| is the result of a call to DoubleToIndex() with num_index_points =
// |num_index_points|, then the returned index is in the range
// [0, ((|max_event_vector_index| + 1) * |num_index_points|) - 1].
//
// If |value_index| is an index into a vector of |num_buckets| histogram buckets, then the returned
// index is in the range
// [0, ((|max_event_vector_index| + 1) * |num_buckets|) - 1].
//
// If |value_index| is the result of a call to HistogramBucketAndCountToIndex() with
// num_index_points = |num_index_points|, and if the histogram has |num_buckets| buckets, then the
// returned index is in the range
// [0, (|max_event_vector_index| + 1) * |num_buckets| * |num_index_points| - 1].
uint64_t ValueAndEventVectorIndicesToIndex(uint64_t value_index, uint64_t event_vector_index,
                                           uint64_t max_event_vector_index);

// ValueAndEventVectorIndicesFromIndex is the inverse of ValueAndEventVectorIndicesToIndex.
//
// After a call to this function, depending on the context, |value_index| is an index corresponding
// to a signed numeric value, a count, a histogram bucket index, or a histogram bucket index
// together with a count.
void ValueAndEventVectorIndicesFromIndex(uint64_t index, uint64_t max_event_vector_index,
                                         uint64_t* value_index, uint64_t* event_vector_index);

// Returns true if |index| was encoded using CountToIndex.
bool IsCountIndex(uint64_t index, uint64_t num_index_points);

}  // namespace cobalt

#endif  //  COBALT_SRC_ALGORITHMS_PRIVACY_NUMERIC_ENCODING_H_
