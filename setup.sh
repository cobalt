#!/bin/bash
#
# Copyright 2016 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Installs cobalt dependencies.

set -e

readonly SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly PREFIX="${SCRIPT_DIR}/sysroot"

export PATH="${PREFIX}/bin:${PATH}"

echo Ensuring cipd
${SCRIPT_DIR}/cipd ensure -ensure-file cobalt.ensure -root sysroot ||
  echo -e "\033[0;31m\n\nTry deleting .cipd_client and running ./setup.sh again.\033[0m\n\n"
